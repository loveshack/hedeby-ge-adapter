/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.impl.ResourcePropertyUpdateOperation;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * JUnit test for <code>HostImpl</code>.
 * 
 * <p>This junit test test currently onlny the <code>setStatic</code> method of
 *    <code>HostImpl</code>.
 * 
 */
public class HostImplTest extends TestCase {
    
    private final static Logger log = Logger.getLogger(HostImplTest.class.getName());

    private ExecutionEnv env;
    
    /**
     * Create a new instance of <code>HostImplTest</code>
     * @param testName the name of the test
     */
    public HostImplTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        env = DummyExecutionEnvFactory.newInstance();
    }



    /**
     * Tests that <code>setStatic</code> sets and clears the static flag.
     */
    public void testSetStatic() throws Exception {

        Resource res = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost");
        HostImpl hi = new HostImpl(res);
        
        hi.setStatic(true);

        assertTrue("Static flag must be set", hi.isStatic());
        assertTrue("Resource property static must be true", hi.getResource().isStatic());
        
        hi.setStatic(false);
        assertFalse("Static flag must be cleared", hi.isStatic());
        assertFalse("Resource property static must be false", hi.getResource().isStatic());
    }
    
    /**
     * This test checks regression of issue 651
     * 
     * If the resource has the static flag set it indicates that the resource provider
     * assigned a resource to the service with the 'sdmadm mvr -static' operation.
     * In this case HostImpl must set the userSetStaticFlag to true to prevent that the
     * automatic static check overrides the static flag.
     * 
     * @throws java.lang.Exception
     */
    public void test_issue651() throws Exception {

        Field field = HostImpl.class.getDeclaredField("userSetStaticFlag");
        field.setAccessible(true);
        try  {
            // For a default resource the 'user set static' flag must not be set
            HostImpl hi = new HostImpl(DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost"));
            boolean userSetStaticFlag = (Boolean)field.get(hi);
            assertFalse("host must not be 'user set static'", userSetStaticFlag);

            // For a static resource the flag must be set
            Resource res = hi.getResource();
            res.setStatic(true);
            hi = new HostImpl(res);
            userSetStaticFlag = (Boolean)field.get(hi);
            assertTrue("host must be 'user set static'", userSetStaticFlag);
        } finally {
            field.setAccessible(false);
        }
    }
    /**
     * Tests that <code>HostImpl#setStatic</code> can not clear the static flag
     * if it has been set with the <code>HostImpl#modify</code> (test for issue 586).
     * 
     */
    public void testNotClearManualStatic() throws Exception {
        HostImpl hi = new HostImpl(DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), "localhost"));

        ResourceChangeOperation op = new ResourcePropertyUpdateOperation(HostResourceType.STATIC.getName(), true);
        try {
            // The parameter can be null because it this special case HostImpl does
            // not use this parameter. On a long term view we need a mockup for this parameter
            // This mockup will be implemented with issue 575
            hi.modify(null, Collections.singleton(op));
        } catch (InvalidResourcePropertiesException ex) {
            log.throwing(HostImplTest.class.getName(), "testNotClearManualStatic", ex);
            fail("Setting the resource property static did not work");
        }
        
        assertTrue("Static flag must be set", hi.isStatic());
        assertTrue("Resource property static must be true", hi.getResource().isStatic());
        
        hi.setStatic(false);

        assertTrue("Static flag must not be cleared if it has been set with modify method", hi.isStatic());
        assertTrue("Resource property static must be true", hi.getResource().isStatic());
    }
}
