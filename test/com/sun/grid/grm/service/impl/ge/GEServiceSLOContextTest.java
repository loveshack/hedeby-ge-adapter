/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.ConstantFilter;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterHelper;
import com.sun.grid.jgdi.monitoring.JobSummary;
import com.sun.grid.jgdi.monitoring.JobSummaryImpl;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryResult;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 * Tests for GEServiceSLOContext class.
 */
public class GEServiceSLOContextTest extends TestCase {

    private final static Logger log = Logger.getLogger(GEServiceSLOContextTest.class.getName());

    public GEServiceSLOContextTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of invalidate method, of class GEServiceSLOContext.
     */
    public void testConstructorNullQueueInstanceSummaryResult() {
        log.info("constructor");
        QueueInstanceSummaryResult result = null;
        List<Resource> resourceList = Collections.<Resource>emptyList();
        try {
            new GEServiceSLOContext(resourceList, result);
        } catch (Exception e) {
            fail("Instance should be created.");
        }
    }

    /**
     * Test of invalidate method, of class GEServiceSLOContext.
     */
    public void testInvalidateNullQueueInstanceSummaryResult() {
        log.info("invalidate");
        QueueInstanceSummaryResult result = null;
        List<Resource> resourceList = Collections.<Resource>emptyList();
        GEServiceSLOContext instance = new GEServiceSLOContext(resourceList, result);
        try {
            instance.invalidate();
        } catch (Exception e) {
            fail("Invalidation should pass.");
        }
    }  
    
    public void testGetPendingJobs() throws Exception {
        log.fine("testGetPendingJobs");
        
        QstatMockup qstat = new QstatMockup();
        qstat.addPendingJobs(1);
        
        List<Resource> resourceList = Collections.<Resource>emptyList();
        GEServiceSLOContext ctx = new GEServiceSLOContext(resourceList, qstat.getResult());
        assertFalse("Pending job list must not be empty", ctx.getPendingJobs().isEmpty());
    }
    
    public void testGetRunningJobsPerHost() throws Exception {
        log.fine("testGetRunningJobsPerHost");
        
        QstatMockup qstat = new QstatMockup();
        List<JobSummary> jobs = new LinkedList<JobSummary>();
        for(int i = 0; i < 10; i++) {
            JobSummaryImpl job = qstat.createJob();
            job.addHardRequest("num_proc", Integer.toString(i), 0);
            jobs.add(job);
        }
        qstat.addRunningJobs(Hostname.getLocalHost().getHostname(), jobs);
        
        List<Resource> resourceList = Collections.<Resource>emptyList();
         
        GEServiceSLOContext ctx = new GEServiceSLOContext(resourceList, qstat.getResult());

        Filter<JobSummary> filter = ConstantFilter.<JobSummary>alwaysMatching();
        Map<Hostname,GEServiceSLOContext.HostStatistic> hostStatMap = ctx.getHostStatistic(filter);
        
        GEServiceSLOContext.HostStatistic hostStat = hostStatMap.get(Hostname.getLocalHost());
        assertNotNull("Did not find the static of the localhost in the qstat result", hostStat);
        assertEquals("expect 10 running jobs on localhost", 10, hostStat.getRunningJobsCount());
    }

    public void testGetRunningJobsPerHostFiltered() throws Exception {
        log.fine("testGetRunningJobsPerHostFiltered");
        
        QstatMockup qstat = new QstatMockup();
        List<JobSummary> jobs = new LinkedList<JobSummary>();
        for(int i = 0; i < 10; i++) {
            JobSummaryImpl job = qstat.createJob();
            job.addHardRequest("num_proc", Integer.toString(i), 0);
            jobs.add(job);
        }
        qstat.addRunningJobs(Hostname.getLocalHost().getHostname(), jobs);
        
        List<Resource> resourceList = Collections.<Resource>emptyList();
         
        GEServiceSLOContext ctx = new GEServiceSLOContext(resourceList, qstat.getResult());
        Filter<JobSummary> filter = FilterHelper.parse("num_proc <= 5");
        
        Map<Hostname,GEServiceSLOContext.HostStatistic> hostStatMap = ctx.getHostStatistic(filter);
        
        GEServiceSLOContext.HostStatistic hostStat = hostStatMap.get(Hostname.getLocalHost());
        assertNotNull("Did not find the static of the localhost in the qstat result", hostStat);
        assertEquals("expect 6 running jobs on localhost", 6, hostStat.getRunningJobsCount());
        
    }
    
    
}
