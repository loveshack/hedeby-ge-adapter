/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.jgdi.monitoring.JobSummary;
import com.sun.grid.jgdi.monitoring.JobSummaryImpl;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryImpl;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryResult;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryResultImpl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * This class can be used as mockup for the qstat -f over jgdi
 */
public class QstatMockup {

    private final AtomicReference<QueueInstanceSummaryResultImpl> qstatRes = new AtomicReference<QueueInstanceSummaryResultImpl>();
    private static AtomicInteger jobSequence = new AtomicInteger(1);

    public QstatMockup() {
        qstatRes.set(new QueueInstanceSummaryResultImpl());
    }

    public void addPendingJobs(int count) {
        List<JobSummary> pendingJobs = new ArrayList<JobSummary>(count);
        for (int i = 0; i < count; i++) {
            pendingJobs.add(createJob());
        }
        qstatRes.get().addPendingJobs(pendingJobs);
    }

    /**
     * A method to generate Pendingjobs that are on hold 
     * These jobs must not be counted for the MaxPendingJobSLO
     * @param count amount of jobs
     */
    public void addPendigJobOnHold(int count) {
        List<JobSummary> pendingJobs = new ArrayList<JobSummary>(count);
        for (int i = 0; i < count; i++) {
            JobSummaryImpl job = createJob();
            job.setState("wh");
            pendingJobs.add(job);
        }
        qstatRes.get().addPendingJobs(pendingJobs);
    }

    public JobSummaryImpl addPendingJob() {
        JobSummaryImpl job = createJob();
        qstatRes.get().addPendingJobs(Collections.singletonList((JobSummary) job));
        return job;
    }
    
    /**
     * same as addPendingJob it just sets the amount of needed slots 
     * as a real parallel job would do
     * @param amountSlots needed for parallel execution
     * @return a JobSummaryImpl object that represents a parallel job
     */
    public JobSummaryImpl addPendingParallelJob(int amountSlots) {
        JobSummaryImpl job = createJob();
        job.setSlots(amountSlots);
        qstatRes.get().addPendingJobs(Collections.singletonList((JobSummary) job));
        return job;
    }

    public void addRunningJobs(String host, List<JobSummary> jobs) {
        addRunningJobs(host, "all.q", jobs);
    }

    public void clearQstatRes() {
        qstatRes.set(new QueueInstanceSummaryResultImpl());
    }

    public void addRunningJobs(String host, String queue, List<JobSummary> jobs) {
        QueueInstanceSummaryImpl qi = new QueueInstanceSummaryImpl();
        qi.setName(queue + "@" + host);
        qi.addJobs(jobs);
        qstatRes.get().addQueueInstanceSummary(qi);
    }

    public JobSummaryImpl createJob() {
        JobSummaryImpl job = new JobSummaryImpl();
        job.setName("dummy");
        //state should not be null as it is tested by the MaxPendingJobSLO
        job.setState("");
        job.setId(jobSequence.getAndIncrement());
        //every job should have per default 1 slot that it requires
        job.setSlots(1);
        return job;
    }

    public void addRunningJobs(String host, int count) {
        for (int i = 0; i < count; i++) {
            QueueInstanceSummaryImpl qi = new QueueInstanceSummaryImpl();
            qi.setName("all.q@" + host);
            qstatRes.get().addQueueInstanceSummary(qi);
        }
    }

    public QueueInstanceSummaryResult getResult() {
        return qstatRes.get();
    }
}
