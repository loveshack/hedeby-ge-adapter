/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge.slo;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.common.TimeInterval;
import com.sun.grid.grm.config.gridengine.GEServiceConfig;
import com.sun.grid.grm.config.gridengine.MaxPendingJobsSLOConfig;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.DefaultResourceFactory;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.impl.ge.GEServiceSLOContext;
import com.sun.grid.grm.service.impl.ge.QstatMockup;
import com.sun.grid.grm.service.impl.ge.cli.AddGEServiceCliCommand;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.TimeIntervalHelper;
import com.sun.grid.grm.validate.GrmValidationException;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;

/**
 *
 */
public class MaxPendingJobsSLOTest extends TestCase {

    private final static Logger log = Logger.getLogger(MaxPendingJobsSLOTest.class.getName());

    private ExecutionEnv env;

    public MaxPendingJobsSLOTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        env = DummyExecutionEnvFactory.newInstance();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * This test addresses Issue 606. If a job is on hold (qsub -h) the max pending
     * job slo should not count it, although it is pending
     * The test works in x steps
     * 1) setup managed gridengine with a MaxPendingJobSLO and one Res (two slots)
     * 2) submit two pending jobs on hold ==> slo is fullfilled
     * 3) submit two more pending jobs on hold ==> slo is fullfilled
     * 4) submit two pending jobs *NOT* on hold ==> slo is fullfilled 
     * 5) expire setMaxWaitTimeForJobs ==> slo is not fullfilled any more!
     * @throws java.lang.Exception
     */
    public void testJobsOnHoldIssue606() throws Exception {
        log.log(Level.FINE, "testJobsOnHold606");

        // The configurator represents a Grid engine instance that is managed by 
        // an GEAdapter with a MaxPendingJobSLO with a max pending job threshold of 1
        Configurator conf = new Configurator();

        // The MaxPendingJobSLO has an urgency of 20  => every resource that is 
        // needed to fullfill the SLO will have the usage 20
        conf.getConf().setUrgency(20);


        // We will let the SLO wait 2 sec until a not used resource is considered as useless
        TimeInterval ti = new TimeInterval();
        ti.setValue(2);
        ti.setUnit(TimeIntervalHelper.SECONDS);
        conf.getConf().setMaxWaitTimeForJobs(ti);

        // Setup a resource with two slots that is assumed be used by any incoming job
        // this is the only resource of the GridEngine cluster
        Resource res = conf.addResource(Hostname.getLocalHost().getHostname());
        conf.getConf().setAverageSlotsPerHost(2);


        // we start to schedule 2 jobs with qsub -h
        conf.addPendigJobOnHold(2);

        // The slo calculation
        // As the two jobs that are pending are on hold they are not considered
        // No resource is needed to fullfill the SLO
        SLOState state = conf.newRun();

        // There is no usage on the resource as it is not needed to fullfill the slo
        Usage usage = state.getUsageMap().get(res);
        assertNull("Must not have a usage for the resource", usage);

        // MaxPendingJobsSLO is is fullfilled ==> no need 
        assertFalse("Must not have a need, MaxPendingJobsSLO should assume that" + " the two pending jobs are scheduled on the new resource", state.hasNeeds());


        // we start to schedule 2 more jobs with qsub -h
        conf.addPendigJobOnHold(2);

        // The slo calculation
        // As the 4 jobs that are pending are on hold they are not considered
        // No resource is needed to fullfill the SLO
        state = conf.run();

        // There is still no usage on the resource as it is not needed to fullfill the slo
        usage = state.getUsageMap().get(res);
        assertNull("Must not have a usage for the resource", usage);

        // MaxPendingJobsSLO is is fullfilled ==> no need 
        assertFalse("Must not have a need, MaxPendingJobsSLO should assume that" + " the two pending jobs are scheduled on the new resource", state.hasNeeds());


        // Now we add two really pending jobs!
        conf.addPendingJobs(2);



        // The slo calculation
        // From the 6 jobs that are pending 2 are not on hold they are considered!
        // The resource is not timed out! 
        // The usage of the resource should be now 20
        // Thus it should not produce any needs as the resource is not timed out 
        // and may take the two jobs
        state = conf.run();

        assertFalse("Must not have a need", state.hasNeeds());
        usage = state.getUsageMap().get(res);
        assertEquals("Usage must be 20", usage.getLevel(), 20);




        // Now wait until the waitForJobsTime has expired
        Thread.sleep(2000);
        // the resource is not in the waitForJobsTime window anymore

        // The slo calculation
        // From the 6 jobs that are pending 2 are not on hold they are considered!
        // The resource is not timed out! 
        // No resource is can fullfill the SLO 
        // The need will be now 1 (for two slots)
        // The usage of the resouce will be now 0 (as it is not used any more to
        // fullfill the SLO)
        state = conf.run();

        // There is still no usage on the resource as it is not needed to fullfill the slo
        usage = state.getUsageMap().get(res);
        assertNull("Must not have a usage for the resource", usage);

        // MaxPendingJobsSLO is not is fullfilled ==> need of one resource! 
        assertTrue("Must have a need, MaxPendingJobsSLO should see that a new Resource is required!", state.hasNeeds());

    }
    
     /**
     * This test addresses Issue 612.
     * If a parallel job is submitted it should generate not a single need but
     * a need according to the amount of slots it requires!
     * @throws java.lang.Exception
     */
    public void testParallelJobIssue612() throws Exception {
        log.log(Level.FINE, "testParallelJobIssue612");


        // The configurator represents a Grid engine instance that is managed by 
        // an GEAdapter with a MaxPendingJobSLO with a max pending job threshold of 1
        Configurator conf = new Configurator();

        // The MaxPendingJobSLO has an urgency of 20  => every resource that is 
        // needed to fullfill the SLO will have the usage 20
        conf.getConf().setUrgency(20);


        // We will let the SLO wait 2 sec until a not used resource is considered as useless
        TimeInterval ti = new TimeInterval();
        ti.setValue(2);
        ti.setUnit(TimeIntervalHelper.SECONDS);
        conf.getConf().setMaxWaitTimeForJobs(ti);

        // Setup a resource with two slots that is assumed be used by any incoming job
        // this is the only resource of the GridEngine cluster
        Resource res = conf.addResource(Hostname.getLocalHost().getHostname());
        conf.getConf().setAverageSlotsPerHost(2);


        // we schedule a parallel job that needs 50 slots => 25 resources a 2 slots
        conf.addPendingParallelJob(50);

        
        // The slo calculation
        SLOState state = conf.newRun();

        // There is a usage on the resource as it is considered by the MPJ slo
        Usage usage = state.getUsageMap().get(res);
        assertEquals("Usage must be 20", usage.getLevel(), 20);

        
        // MaxPendingJobsSLO is not is fullfilled ==> need of resources! 
        assertTrue("Must have a need, MaxPendingJobsSLO should see that new Resources are required!", state.hasNeeds());

        // There are 50 slots needed ==> 25 resources (a two slots) are required. 
        // There is allready one==>makes a need of 24 resources
        assertTrue("There should be a need of 24, but it was "+ state.getNeeds().get(0).getQuantity(), state.getNeeds().get(0).getQuantity()==24);
    

    }

    public void testNoPendingJobNoNeedNoUsage() throws Exception {
        log.log(Level.FINE, "testNoPendingJobNoNeedNoUsage");

        Configurator conf = new Configurator();

        conf.addResource("localhost");

        SLOState state = conf.newRun();

        assertFalse("Must not have a need without pending jobs", state.hasNeeds());
        assertTrue("Usage must be empty", state.getUsageMap().isEmpty());
    }

    public void testMaxWaitTimeForJobs() throws Exception {
        log.log(Level.FINE, "testMaxWaitTimeForJobs");
        Configurator conf = new Configurator();

        // New constructr the following configuration
        // averageSlotsPerHost = 2
        // # pending jobs = 2
        // urgency = 2
        // MaxWaitTimeForJobs = 2 seconds
        conf.addPendingJobs(2);

        Resource res = conf.addResource(Hostname.getLocalHost().getHostname());

        conf.getConf().setAverageSlotsPerHost(2);
        conf.getConf().setUrgency(2);

        TimeInterval ti = new TimeInterval();
        ti.setValue(2);
        ti.setUnit(TimeIntervalHelper.SECONDS);
        conf.getConf().setMaxWaitTimeForJobs(ti);

        SLOState state = conf.newRun();
        // MaxPendingJobsSLO must assume that the new resource will
        // run the two pending jobs
        // the slo must produces no need
        assertFalse("Must not have a need, MaxPendingJobsSLO should assume that" + " the two pending jobs are scheduled on the new resource",
                state.hasNeeds());
        Usage usage = state.getUsageMap().get(res);

        assertNotNull("Must have a usage for the resource", usage);
        assertEquals("Usage must be 2", usage.getLevel(), 2);

        // Increase the number of pending jobs
        conf.addPendingJobs(2);

        // Now we have 4 pending jobs, MaxPendingJobsSLO assume that two
        // jobs will be scheduled on the pending resource. The remaining
        // not scheduleable 2 jobs will produce a need of one additional
        // resource
        state = conf.run();
        assertTrue("Must have a need", state.hasNeeds());

        assertEquals("The quantity of the need must be 1", 1, state.getNeeds().get(0).getQuantity());

        assertNotNull("Must have a usage for the resource", usage);
        assertEquals("Usage must be 2", usage.getLevel(), 2);

        // Now wait until the waitForJobsTime has expired
        Thread.sleep(2000);

        // Now we have still 4 pending jobs, MaxPendingJobsSLO detects that the
        // pending resource did not get a jobs scheduled within the maxWaitTimeForJobs
        // It assumes that it will never get a jobs
        // => the resource must not have a usage
        // => there must be a need for 2 additional resource (2 jobs per resource)
        state = conf.run();

        assertTrue("Must have a need", state.hasNeeds());
        assertEquals("The quantity of the need must be 2", 2, state.getNeeds().get(0).getQuantity());
        usage = state.getUsageMap().get(res);
        assertNull("maxWaitTimeForJob expired, expect no usage", usage);


        // Increase the number of jobs to 5
        conf.addPendingJobs(1);

        // Now we have 5 pending jobs, but no resource
        // the timedout resource must not count
        // the requested need must be 3
        state = conf.run();

        assertTrue("Must have a need", state.hasNeeds());
        assertEquals("The quantity of the need must be 3", 3, state.getNeeds().get(0).getQuantity());
        usage = state.getUsageMap().get(res);
        assertNull("maxWaitTimeForJob expired, expect no usage", usage);


        // remove the resource 
        conf.removeResource(res);

        // We have still the same situation
        // 5 pending jobs produces a need with quantity 3
        state = conf.run();

        assertTrue("Must have a need", state.hasNeeds());
        assertEquals("The quantity of the need must be 3", 3, state.getNeeds().get(0).getQuantity());
        usage = state.getUsageMap().get(res);
        assertNull("no usage expected", usage);

        // Readd the resource => it must count now
        res = conf.addResource(Hostname.getLocalHost().getHostname());

        // We have 5 pending jobs
        // MaxPendingJobsSLO assumes that two jobs will be scheduled on the new
        // resource
        // => 3 not schedulable jobs will produces a need of two additonal resource
        // The resource gets a usage
        state = conf.run();

        assertTrue("Must have a need", state.hasNeeds());
        assertEquals("The quantity of the need must be 2", 2, state.getNeeds().get(0).getQuantity());
        usage = state.getUsageMap().get(res);
        assertNotNull("Must have a usage for the resource", usage);
        assertEquals("Usage must be 2", usage.getLevel(), 2);
    }

    public void testZeroAverageSlotsPerHost() throws Exception {
        log.log(Level.FINE, "testZeroAverageSlotsPerHost");

        MaxPendingJobsSLOConfig conf = new MaxPendingJobsSLOConfig();
        conf.setAverageSlotsPerHost(0);
        conf.setMax(1);
        conf.setName("max_pend");
        try {
            MaxPendingsJobsSLO slo = new MaxPendingsJobsSLO(conf);
            fail("MaxPendingsJobsSLO must not accept averageSlotsPerHost=0");
        } catch (GrmException ex) {
            // Expected error
            log.log(Level.FINE, "Expected error ", ex);
        }
    }

    public void testNegativeAverageSlotsPerHost() throws Exception {
        log.log(Level.FINE, "testNegativeAverageSlotsPerHost");

        MaxPendingJobsSLOConfig conf = new MaxPendingJobsSLOConfig();
        conf.setAverageSlotsPerHost(-1);
        conf.setMax(1);
        conf.setName("max_pend");
        try {
            MaxPendingsJobsSLO slo = new MaxPendingsJobsSLO(conf);
            fail("MaxPendingsJobsSLO must not accept a negative value for averageSlotsPerHost");
        } catch (GrmException ex) {
            // Expected error
            log.log(Level.FINE, "Expected error ", ex);
        }
    }

    public void testQuantityAverageSlotsPerHost() throws Exception {

        log.log(Level.FINE, "testQuantityAverageSlotsPerHost");

        int[] pendingJobs = {1, 1, 10, 20};
        int[] averageSlotsPerHost = {1, 10, 10, 10};
        int[] expectedQuantity = {1, 1, 1, 2};
        for (int i = 0; i < pendingJobs.length; i++) {
            Configurator conf = new Configurator();

            conf.addPendingJobs(pendingJobs[i]);

            conf.getConf().setAverageSlotsPerHost(averageSlotsPerHost[i]);
            SLOState state = conf.newRun();

            assertEquals("number of needs must be 1", 1, state.getNeeds().size());
            assertEquals("invalid quantity in need", state.getNeeds().get(0).getQuantity(), expectedQuantity[i]);
        }
    }

    public void testInvalidAverageSlotsPerHost() throws Exception {

        log.log(Level.FINE, "testInvalidAverageSlotsPerHost");

        GEServiceConfig serviceConf = AddGEServiceCliCommand.createDefaultConfig();

        // Validate the default configuration, must not produce an error
        validate(serviceConf);

        MaxPendingJobsSLOConfig conf = new MaxPendingJobsSLOConfig();
        conf.setAverageSlotsPerHost(0);
        conf.setMax(1);
        conf.setName("max_pend");
        serviceConf.getSlos().getSlo().add(conf);
        try {
            validate(serviceConf);
            fail("xml parser did not detect that 0 is not a valid value for the averageSlotsPerHost parameter");
        } catch (GrmValidationException ex) {
            // Expected error
            for (String msg : ex.getErrorMessages()) {
                log.log(Level.FINE, "Expected error: " + msg);
            }
        }

    }
    
    /**
     * This test makes sure that it is possible to set a usage on the MaxPendingJobsSLO
     * that is separate from the set urgency.
     * 
     * @throws java.lang.Exception
     */
    public void testSettingSeparateUsage() throws Exception {
        log.log(Level.FINE, "testSettingSeparateUsage");
        
        // setup configuration with one resource and urgency 1 (==usage)
        Configurator conf = new Configurator();
        conf.getConf().setUrgency(1);
        conf.addPendingJobs(10);
        conf.getConf().setAverageSlotsPerHost(1);
        Resource res = conf.addResource(Hostname.getLocalHost().getHostname());
        
        SLOState state = conf.newRun();
        List<Need> needList = state.getNeeds();
        assertTrue("There are some needs", !needList.isEmpty());
        assertEquals("urgency is equal to 1", 1, needList.get(0).getUrgency().getLevel());
        Usage usage = state.getUsageMap().get(res);
        assertNotNull("usage is not null", usage);
        assertEquals("usage is equal to urgency", 1, usage.getLevel());
        
        // now change the usage to 10
        conf.getConf().setUsage(10);
        
        state = conf.newRun();
        needList = state.getNeeds();
        assertTrue("There are some needs", !needList.isEmpty());
        assertEquals("urgency is equal to 1", 1, needList.get(0).getUrgency().getLevel());
        usage = state.getUsageMap().get(res);
        assertNotNull("usage is not null", usage);
        assertEquals("usage is now 10", 10, usage.getLevel());
    }

    private class Configurator extends QstatMockup {

        private MaxPendingsJobsSLO slo;
        private final MaxPendingJobsSLOConfig conf;
        private final List<Resource> resources = new LinkedList<Resource>();
        public Configurator() {
            conf = new MaxPendingJobsSLOConfig();
            conf.setMax(1);
            conf.setAverageSlotsPerHost(10);
            conf.setName("max_pend");
        }

        public Resource addResource(String hostname) throws Exception {
            Resource ret = DefaultResourceFactory.createResourceByName(env, HostResourceType.getInstance(), hostname);
            resources.add(ret);
            return ret;
        }

        public boolean removeResource(Resource res) {
            return resources.remove(res);
        }

        public SLOState newRun() throws Exception {
            slo = new MaxPendingsJobsSLO(getConf());
            return run();
        }

        public SLOState run() throws Exception {
            GEServiceSLOContext ctx = new GEServiceSLOContext(resources, getResult());
            return slo.update(ctx);
        }

        public MaxPendingJobsSLOConfig getConf() {
            return conf;
        }
    }

    private static void validate(GEServiceConfig conf) throws Exception {
        File tmpFile = File.createTempFile("MaxPendingJobsSLOTest", ",xml");
        try {
            XMLUtil.store(conf, tmpFile);
            XMLUtil.loadAndValidate(tmpFile);
        } finally {
            tmpFile.delete();
        }
    }
}
