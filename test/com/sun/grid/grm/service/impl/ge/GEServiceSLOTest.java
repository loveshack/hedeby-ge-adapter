/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.DummyExecutionEnvFactory;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.common.FixedUsageSLOConfig;
import com.sun.grid.grm.config.common.MinResourceSLOConfig;
import com.sun.grid.grm.config.common.PermanentRequestSLOConfig;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.config.common.SLOSet;
import com.sun.grid.grm.config.gridengine.GEServiceConfig;
import com.sun.grid.grm.config.gridengine.MappingConfig;
import com.sun.grid.grm.config.gridengine.MaxPendingJobsSLOConfig;
import com.sun.grid.grm.config.naming.DummyConfigurationServiceContext;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.impl.ge.cli.AddGEServiceCliCommand;
import com.sun.grid.grm.service.impl.ge.ui.MappingUtil;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.service.slo.event.SLOErrorEvent;
import com.sun.grid.grm.service.slo.event.SLOManagerEventListener;
import com.sun.grid.grm.service.slo.event.SLOUpdatedEvent;
import com.sun.grid.grm.service.slo.event.UsageMapUpdatedEvent;
import com.sun.grid.grm.util.Platform;
import java.io.File;
import java.util.Collections;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import javax.naming.Context;
import junit.framework.TestCase;

/**
 * This test checks the all possible SLO can be configured for GEService.
 * It does not check that the SLOs works correctly.
 */
public class GEServiceSLOTest extends TestCase {
    
    private final static Logger log = Logger.getLogger(GEServiceSLOTest.class.getName());
    
    private final static String CELL_NAME = "default";
    private ExecutionEnv env;
    private TestGEServiceAdapterImpl service;
    private MappingConfig mapping;
    private File tmpSgeRoot;
    public GEServiceSLOTest(String testName) {
        super(testName);
    }            

    @Override
    protected void setUp() throws Exception {
        log.entering(GEServiceSLOTest.class.getName(), "setUp");

        env = DummyExecutionEnvFactory.newInstance();
        
        // GEServiceConfigHelper#setConfig is very picky, it checks
        // the the common directory of the GE cluster exists.
        // => create a dummy common directory
        File td = new File(System.getProperty("java.io.tmpdir"));
        File userTmpDir = new File(td, System.getProperty("user.name"));
        this.tmpSgeRoot = new File(userTmpDir, getName() + System.currentTimeMillis());
        File commonDir = new File(tmpSgeRoot, CELL_NAME + File.separatorChar + "common");
        commonDir.mkdirs();
        
        // GEServiceConfigHelper#setConfig that the complex mapping is stored
        // in the configuration
        // => Create it
        mapping = MappingUtil.createDefaultMapping();
        env.getContext().bind("module.ge-service-adapter.complex_mapping.default", mapping);
        
        service = new TestGEServiceAdapterImpl(new TestGESerivceContainer(env, "ge"));
        
        log.exiting(GEServiceSLOTest.class.getName(), "setUp");
    }

    @Override
    protected void tearDown() throws Exception {
        log.entering(GEServiceSLOTest.class.getName(), "tearDown");
        Context ctx = env.getContext();
        if (ctx instanceof DummyConfigurationServiceContext) {
            ((DummyConfigurationServiceContext)ctx).clear();
        }
        Platform.getPlatform().removeDir(tmpSgeRoot, true);
        log.exiting(GEServiceSLOTest.class.getName(), "tearDown");
    }
    
    /**
     * Reconfigures the SLOs of a GE service and runs an SLO update.
     * Assert the the SLOManager fires and sloUpdate event for the slo.
     * 
     * @param slo  the slo configuration
     * @throws java.lang.Exception
     */
    private void assertSLOUpdate(SLOConfig slo) throws Exception {
        log.entering(GEServiceSLOTest.class.getName(), "assertSLOUpdate");
        
        SLOManagerEventHistory lis = new SLOManagerEventHistory();
        
        service.getSLOManager().addSLOManagerListener(lis);
        
        // The complete reconfiguration of the GE service can be done over
        // the config helper
        service.getConfigHelper().setConfig(createConfig(slo), mapping);
        
        // Reconfigure the SLO Manager
        service.getSLOManager().setSLOs(service.getConfigHelper().getSLOs());
        
        // Do an SLO run
        service.getSLOManager().update();
        
        lis.assertUpdateForSLO(slo, 100);
        
        log.exiting(GEServiceSLOTest.class.getName(), "assertSLOUpdate");
    }
    
    /**
     * Check that a PermanentRequestSLO can be handled by an instance of
     * <code>SparePoolServiceImpl</code>.
     * @throws java.lang.Exception
     */
    public void testPermanentRequestSLO() throws Exception {
        PermanentRequestSLOConfig slo = new PermanentRequestSLOConfig();
        slo.setUrgency(99);
        slo.setName("permanentRequest");
        slo.setQuantity(1);
        
        assertSLOUpdate(slo);
    }
    
    /**
     * Check that a MinResourceSLO can be handled by an instance of
     * <code>SparePoolServiceImpl</code>.
     * @throws java.lang.Exception
     */
    public void testMinResourceSLO() throws Exception {
        MinResourceSLOConfig slo = new MinResourceSLOConfig();
        slo.setMin(1);
        slo.setName("minRes");
        slo.setUrgency(99);
        
        assertSLOUpdate(slo);
    }
    
    /**
     * Check that a FixedUsageSLO can be handled by an instance of
     * <code>SparePoolServiceImpl</code>.
     * @throws java.lang.Exception
     */
    public void testFixedUsageSLO() throws Exception {
        FixedUsageSLOConfig slo = new FixedUsageSLOConfig();
        slo.setName("fixedUsage");
        slo.setUrgency(99);
        
        assertSLOUpdate(slo);
    }
    
    
    public void testMaxPendingJobsSLO() throws Exception {
        MaxPendingJobsSLOConfig slo = new MaxPendingJobsSLOConfig();
        slo.setMax(1);
        slo.setName("maxPend");
        assertSLOUpdate(slo);
    }
    
    private GEServiceConfig createConfig(SLOConfig sloConfig) {
        
        GEServiceConfig ret = AddGEServiceCliCommand.createDefaultConfig();
        
        ret.getConnection().setRoot(tmpSgeRoot.getAbsolutePath());
        ret.getConnection().setKeystore(null);
        ret.getConnection().setUsername(System.getProperty("user.name"));
        ret.getConnection().setPassword("secret");
        
        SLOSet slos = new SLOSet();
        slos.getSlo().add(sloConfig);
        ret.setSlos(slos);
        
        return ret;
    }
    
    private static class SLOManagerEventHistory implements SLOManagerEventListener {

        private final BlockingQueue<SLOUpdatedEvent> updateEvents = new LinkedBlockingQueue<SLOUpdatedEvent>();
        
        public void sloUpdated(SLOUpdatedEvent event) {
            updateEvents.add(event);
        }

        public void usageMapUpdated(UsageMapUpdatedEvent event) {
            // Ignore
        }

        public void sloUpdateError(SLOErrorEvent event) {
            // Ignore
        }
        
        public void assertUpdateForSLO(SLOConfig slo, long timeout) throws InterruptedException {
            SLOUpdatedEvent evt = updateEvents.poll(timeout, TimeUnit.MILLISECONDS);
            
            if(evt == null) {
                fail("Timeout while waiting for update event of slo " + slo.getName());
            }
            assertEquals("SLOUpdate event contains wrong slo name", slo.getName(), evt.getSLOName());
        }
    }
    
    /**
     * This class overrides <code>getResources</code> to avoid the ServiceNotActiveException,
     * hence it is not necessary to start the GEService.
     * 
     * In addition the createSLOContext method must be mockuped, it runs normally
     * an qstat of jgdi, this is not possible within the test.
     */
    private static class TestGEServiceAdapterImpl extends GEServiceAdapterImpl {
        
        private final  static QstatMockup qstat = new QstatMockup();
        
        public TestGEServiceAdapterImpl(TestGESerivceContainer component) throws GrmException {
            super(component);
        }

        @Override
        public SLOContext createSLOContext() throws GrmException {
            return new GEServiceSLOContext(Collections.<Resource>emptyList(), qstat.getResult()); 
        }
        
    }
    
    
    private static class TestGESerivceContainer extends GEServiceContainer {
        
        public TestGESerivceContainer(ExecutionEnv env, String name) throws GrmException {
            super(env, name);
        }
        
    }
    
}
