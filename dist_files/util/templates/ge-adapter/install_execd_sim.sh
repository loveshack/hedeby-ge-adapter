#!/bin/sh
#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2008 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__
##########################################################################
# Scriptname: install_execd_sim.sh
#
# This script adds only the exec host to the exec host list. It can be used
# for a grid engine service running in simulation mode.
#
# set -x

BASEDIR=`pwd`
SGE_ROOT="@@@SGE_ROOT@@@"
SGE_CELL="@@@CELL_NAME@@@"
EXEC_HOST="@@@EXEC_HOST@@@"

log() {
  echo "$@"
}

if [ ! -d "$SGE_ROOT" ]; then
    log "ERROR: SGE_ROOT directory $SGE_ROOT does not exists"
    exit 2
fi
if [ ! -f "$SGE_ROOT/$SGE_CELL/common/settings.sh" ]; then
    log "ERROR: Settings file ${SGE_ROOT}/${SGE_CELL}/common/settings.sh not found"
    exit 2
fi

. $SGE_ROOT/$SGE_CELL/common/settings.sh
if [ $? -ne 0 ]; then
   log "ERROR: Cannot source settings file"
   exit 2
fi

##### check that the simulation mod is enabled

qconf -sconf > sconf.$$
if [ $? -ne 0 ]; then
   log "ERROR: qconf -sconf failed"
   exit $ERROR_EXIT_STATE
fi

QMASTER_PARAMS=""
ERROR_EXIT_STATE=2
grep qmaster_params sconf.$$ | grep SIMULATE_EXECDS
if [ $? -ne 0 ]; then
   log "SIMULATE_EXECDS not set"
   QMASTER_PARAMS="SIMULATE_EXECDS=true"
fi

if [ "$QMASTER_PARAMS" != "" ]; then
   log "Changing qmaster_params"
   echo "#!/bin/sh" > sconf.$$.sh
   #echo "set -x" >> sconf.$$.sh
   echo "sed 's/qmaster_params.*/qmaster_params ${QMASTER_PARAMS}/' \$1 > /tmp/sconf.\$\$"  >> sconf.$$.sh
   echo "mv /tmp/sconf.\$\$ \$1"  >> sconf.$$.sh
   echo "exit 0"  >> sconf.$$.sh
   chmod 700  sconf.$$.sh
   EDITOR=./sconf.$$.sh
   export EDITOR
   output=`qconf -mconf 2>&1`
   res=$?
   rm -f sconf.$$.sh
   if [ $res -ne 0 ]; then
      log "ERROR: qconf -mconf exited with status $res:\n$output"
      exit $ERROR_EXIT_STATE
   fi
   ERROR_EXIT_STATE=1
fi

# ensure that the simhost complex exits   
qconf -sc > complexes.$$
if [ $? -ne 0 ]; then
  log "ERROR: qconf -sc exited with statis $res"
  exit $ERROR_EXIT_STATE
fi
grep load_report_host complexes.$$
if [ $? -ne 0 ]; then
  log "Creating complex load_report_host"
  echo "load_report_host load_report_host RESTRING == NO NO NONE 0" >> complexes.$$
  output=`qconf -Mc complexes.$$ 2>&1`
  res=$?
  if [ $res -ne 0 ]; then
     log "ERROR: qconf -Mc exited with statis $res\noutput:\n$output"
     exit $ERROR_EXIT_STATE
  fi
fi

output=`qconf -aattr hostgroup hostlist $EXEC_HOST @allhosts 2>&1`
res=$?
if [ $res -ne 0 ]; then
   log "ERROR: qconf -aattr hostgroup exited with status $res:\n$output"
   exit $ERROR_EXIT_STATE
fi
ERROR_EXIT_STATE=1

# Add the complex value load_report_host (the new execd will get the load values
# from the execd running on the master host
master_host=`cat $SGE_ROOT/$SGE_CELL/common/act_qmaster`
output=`qconf -mattr exechost complex_values load_report_host=$master_host $EXEC_HOST`
res=$?
if [ $res -ne 0 ]; then
   log "ERROR: qconf -aattr exechost exited with status $res:\n$output"
   exit $ERROR_EXIT_STATE
fi

exit 0
