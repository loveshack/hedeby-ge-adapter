#!/bin/sh
#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__
##########################################################################
# Scriptname: copy_sge_root_to_cloud.sh
#
# copies the sge_root/cell_name/common directory to the cloud host
#
# set -x

SGE_ROOT="@@@SGE_ROOT@@@"
CELL_NAME="@@@CELL_NAME@@@"

if [ ! -d "$SGE_ROOT" ]; then
    echo "SGE_ROOT directory $SGE_ROOT does not exists"
    exit 2
fi

USER_KEY_PAIR_FILE="@@@RESOURCE:userKeyPairFile@@@"
CLOUD_HOST="@@@EXEC_HOST@@@"
CLOUD_SGE_ROOT=/opt/sge

LOCAL_PATH=$SGE_ROOT/$CELL_NAME/common 
CLOUD_PATH=$CLOUD_SGE_ROOT/$CELL_NAME

# general parameters for ssh/scp invocation
SSH_PARAMS="-q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i $USER_KEY_PAIR_FILE"

# make sure that destination directory exists
ssh $SSH_PARAMS root@$CLOUD_HOST "mkdir -p $CLOUD_PATH" 2> /dev/null
rc=$?

if [ $rc -ne 0 ] ; then
   echo "Could not create directory '$CLOUD_PATH' on cloud host '$CLOUD_HOST', RC=$rc"
   exit 2
fi

scp $SSH_PARAMS -r $LOCAL_PATH root@$CLOUD_HOST:$CLOUD_PATH 2> /dev/null
rc=$?
if [ $rc -ne 0 ] ; then
   echo "Could not copy from local '$LOCAL_PATH' to directory '$CLOUD_PATH' on cloud host '$CLOUD_HOST', RC=$rc"
   exit 2
fi

exit $rc
