#!/bin/sh
#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__
##########################################################################
# Scriptname: patch_bootstrap_files_on_cloud.sh
#
#  Patches the bootstrap files for the cloud host
#
# set -x

CLOUD_SGE_ROOT=/opt/sge
CELL_NAME=@@@CELL_NAME@@@


patch_file() {
   file=$1
   mode=$2
   pattern="$3"

   mv $file $file.org
   sed -e "$pattern" $file.org > $file

   chmod $mode $file 
}

cd /opt/sge/$CELL_NAME/common

patch_file bootstrap    444 "s#^admin_user.*#admin_user root#;s#^binary_path.*#binary_path $CLOUD_SGE_ROOT/bin#"
patch_file settings.sh  644 "s#^SGE_ROOT=.*;#SGE_ROOT=$CLOUD_SGE_ROOT ; #"
patch_file settings.csh 644 "s#^setenv SGE_ROOT.*#setenv SGE_ROOT $CLOUD_SGE_ROOT#"
patch_file sgeexecd     755 "s#^SGE_ROOT=.*;#SGE_ROOT=$CLOUD_SGE_ROOT ; #"
patch_file sgemaster    755 "s#^SGE_ROOT=.*;#SGE_ROOT=$CLOUD_SGE_ROOT ; #"

exit 0

