#!/bin/sh
#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2009 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__
##########################################################################
# Scriptname: install_execd_cloud.sh
#
# set -x

BASEDIR=`pwd`
SGE_ROOT=/opt/sge
CONF_FILE="$BASEDIR/install_execd_cloud.conf"
pid=""

cleanup() {
   if [ "$pid" != "" ]; then
        mypid=$pid
        pid=""
        # If execd installation has been interrupted it did not do the
        # move log
        # The log file is still in /tmp/install.$pid
        # Copy it to the basedir

        if [ -f /tmp/install.$mypid ]; then
           mv /tmp/install.$mypid $BASEDIR
        fi

        if [ -f $BASEDIR/install_execd.stdout ]; then
           log_line=`grep "Install log can be found in:" $BASEDIR/install_execd.stdout`
           if [ $? -eq 0 ]; then
              install_log_file=`echo "$line "| sed 's/^.*Install log can be found in: //'`
              if [ -f "$install_log_file" ]; then
                 mv "$install_log_file" $BASEDIR
              fi
           fi
        fi
   fi
}

# setup the interupt handler
trap "cleanup" TERM INT

if [ ! -d "$SGE_ROOT" ]; then
    echo "SGE_ROOT directory $SGE_ROOT does not exists"
    exit 2
fi
if [ ! -f "$SGE_ROOT/inst_sge" ]; then
    echo "inst_sge script in directory $SGE_ROOT not found"
    exit 2
fi

if [ ! -x "$SGE_ROOT/inst_sge" ]; then
    echo "inst_sge script in directory $SGE_ROOT is not executable"
    exit 2
fi

if [ ! -f "$CONF_FILE" ]; then
    echo "auto config file $CONF_FILE not found"
    exit 2
fi

cd "$SGE_ROOT"

./inst_sge -x -noremote -auto $CONF_FILE \
        2> $BASEDIR/install_execd.stderr > $BASEDIR/install_execd.stdout &
pid=$!
wait $pid
res=$?
cleanup
exit $res
