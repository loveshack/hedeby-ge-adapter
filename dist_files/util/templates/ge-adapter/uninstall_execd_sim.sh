#!/bin/sh
#___INFO__MARK_BEGIN__
##########################################################################
#
#  The Contents of this file are made available subject to the terms of
#  the Sun Industry Standards Source License Version 1.2
#
#  Sun Microsystems Inc., March, 2001
#
#
#  Sun Industry Standards Source License Version 1.2
#  =================================================
#  The contents of this file are subject to the Sun Industry Standards
#  Source License Version 1.2 (the "License"); You may not use this file
#  except in compliance with the License. You may obtain a copy of the
#  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
#
#  Software provided under this License is provided on an "AS IS" basis,
#  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
#  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
#  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
#  See the License for the specific provisions governing your rights and
#  obligations concerning the Software.
#
#  The Initial Developer of the Original Code is: Sun Microsystems, Inc.
#
#  Copyright: 2006 by Sun Microsystems, Inc.
#
#  All Rights Reserved.
#
##########################################################################
#___INFO__MARK_END__
##########################################################################
# Scriptname: install_execd_sim.sh
#
# This script removes only the exec host to the exec host list. It can be used
# for a grid engine service running in simulation mode.
#
# set -x

BASEDIR=`pwd`
SGE_ROOT="@@@SGE_ROOT@@@"
SGE_CELL="@@@CELL_NAME@@@"
EXEC_HOST="@@@EXEC_HOST@@@"

log() {
  echo "$@"
}

if [ ! -d "$SGE_ROOT" ]; then
    log "ERROR: SGE_ROOT directory $SGE_ROOT does not exists"
    exit 2
fi
if [ ! -f "$SGE_ROOT/$SGE_CELL/common/settings.sh" ]; then
   log "ERROR: settings file ${SGE_ROOT}/${SGE_CELL}/common/settings.sh not found"
    exit 2
fi

. $SGE_ROOT/$SGE_CELL/common/settings.sh
if [ $? -ne 0 ]; then
   log "ERROR: Cannot source settings file"
   exit 2
fi

output=`qconf -dattr hostgroup hostlist $EXEC_HOST @allhosts 2>&1`
res=$?
if [ $res -ne 0 ]; then
    log "ERROR: qconf -dattr hostgroup exited with status $res:\n$output"
    exit 1 
fi
log "exec host $EXEC_HOST removed from host group @allhosts"

output=`qconf -de $EXEC_HOST 2>&1`
res=$?
if [ $res -ne 0 ]; then
    log "ERROR: qconf -de exited with status $res:\n$output"
    exit 2 
fi
log "exec host $EXEC_HOST deleted"

exit 0
