/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;

/**
 * This exception is thrown if a transition id of a host is expired
 */
public class HostTransitionExpiredException extends GrmException {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    private final static long serialVersionUID = -2008091801L;
    
    /**
     * Create a new HostTransitionExpiredException
     * @param host  the host 
     * @param transitionId the transition id
     * @param currentTransitionId the current transition id of the host
     */
    public HostTransitionExpiredException(Host host, long transitionId, long currentTransitionId) {
        super("transition.expired", BUNDLE, host.getName().toString(), transitionId, currentTransitionId);
    }
}
