/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.gridengine.MappingConfig;
import com.sun.grid.grm.resource.AbstractResourceType;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.impl.ge.ui.MappingUtil;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.adapter.InvalidResourceStateException;
import com.sun.grid.grm.resource.adapter.impl.AbstractResourceAdapter;
import com.sun.grid.grm.resource.impl.AbstractResourceChanged;
import com.sun.grid.grm.resource.impl.ResourceAmbiguousStateChanged;
import com.sun.grid.grm.resource.impl.ResourceAnnotationChanged;
import com.sun.grid.grm.resource.impl.ResourceAnnotationUpdateOperation;
import com.sun.grid.grm.resource.impl.ResourceUsageChanged;
import com.sun.grid.grm.resource.impl.ResourcePropertyUpdateOperation;
import com.sun.grid.grm.resource.impl.ResourcePropertyUpdated;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.descriptor.ResourcePurgeDescriptor;
import com.sun.grid.grm.util.EventListenerSupport;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.jgdi.configuration.AdminHost;
import com.sun.grid.jgdi.configuration.ClusterQueue;
import com.sun.grid.jgdi.configuration.ComplexEntry;
import com.sun.grid.jgdi.configuration.ExecHost;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummary;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryOptions;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryResult;
import com.sun.grid.jgdi.monitoring.filter.JobStateFilter;
import com.sun.grid.jgdi.monitoring.filter.QueueFilter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Contains all the functionality to work with one Grid Engine host.
 *
 */
public class HostImpl implements  Serializable, Host {
    
    private final static long serialVersionUID = -2008040101L;

    /**
     * These resource properties will be never modified by a jgdi update event
     * @see #update
     */
    private final static List<String> UNMODIFIABLE_KEYS_FOR_UPDATE_METHOD = Arrays.<String>asList(new String []{
        HostResourceType.HOSTNAME.getName(),
        AbstractResourceType.STATIC.getName()
    });
    
    /**
     * These resource properties will be never modified by the modify method
     * @see #modify 
     */
    private final static List<String> UNMODIFIABLE_KEYS_FOR_MODIFY_METHOD = Collections.<String>emptyList();;
    
    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    private final static Logger log = Logger.getLogger(HostImpl.class.getName(), BUNDLE);

    private final Hostname hostName;

    private Map<String, Complex> complexes = Collections.<String, Complex>emptyMap();
    private Resource resource;
    private HostState state = HostState.HOST_NONE;
    
    /**
     * This flag is set to true if the user marked the resource
     * manually as static.
     * This information must be stored persistent in the spool
     * @see #update
     * @see #modify
     */
    private boolean userSetStaticFlag;
    
    /**
     * Store state from prepareForUninstall() to uninstall().
     * Stores the transitionId if the resource is purged.
     */
    private transient long transIdOfResourcePurge = -1;
    
    private transient Lock lock = new ReentrantLock();
    private transient Condition stateChangedCondition = lock.newCondition();
    private transient EventListenerSupport<HostListener> evtSupport = EventListenerSupport.<HostListener>newSynchronInstance(HostListener.class);
    
    /** contains the id of the current transition */
    private transient AtomicLong transitionId = new AtomicLong();
    
    /**
     * Reference to the current install sequence. The reset method
     * will abort this sequence before reseting the resource
     * 
     * @see #reset
     * @see #install
     * @see #uninstall
     */
    private transient AtomicReference<InstallationSequence> installer = new AtomicReference<InstallationSequence>();
    
    private transient volatile String toStringResult = null;
    
    /**
     * Creates a new instance of HostImpl based on a given resource.
     * 
     * @param resource the resource describing the host to be added
     * @throws com.sun.grid.grm.resource.InvalidResourceException thrown, when
     * the resource is of wrong type (not a host), or the hostname cannot be
     * resolved.
     */
    public HostImpl(Resource resource)  throws InvalidResourceException {
        try {
            this.hostName = (Hostname) HostResourceType.HOSTNAME.getValue(resource.getProperties());
            if (this.hostName == null) {
                throw new InvalidResourceException("h.ex.noHostname", BUNDLE, HostResourceType.HOSTNAME.getName());
            }
            if (!this.hostName.isResolved()) {
                throw new InvalidResourceException("h.ex.unresolvedHostname", BUNDLE, hostName.getHostname());
            }
            this.resource = resource;
            this.resource.setUsage(Usage.MAX_VALUE);
            this.resource.setState(GEResourceAdapter.mapHostStateToResourceState(state));
            // issue 651: 
            // If the resource has the static flag set it indicates that the resource provider
            // assigned a resource to the service with the 'sdmadm mvr -static' operation.
            // In this case HostImpl must set the userSetStaticFlag to true to prevent that the
            // automatic static check overrides the static flag
            if (this.resource.isStatic()) {
                userSetStaticFlag = true;
            }
            updateToStringResult();
        } catch (InvalidResourcePropertiesException ex) {
            throw new InvalidResourceException(ex.getLocalizedMessage(), ex);
        }
    }
    
    
    /**
     * Store the host
     * @param out  the output stream
     * @throws java.io.IOException on any IO error
     */
    public void store(OutputStream out) throws IOException {
        lock.lock();
        try {
            ObjectOutputStream oout = new ObjectOutputStream(out);
            oout.writeObject(this);
        } finally {
            lock.unlock();
        }        
    }

    
    /**
     * Add a new host listener
     * @param lis the host listener
     */
    public void addHostListener(HostListener lis) {
        evtSupport.addListener(lis);
    }
    
    /**
     * Remove a host listener
     * @param lis the host listener
     */
    public void removeHostListener(HostListener lis) {
        evtSupport.removeListener(lis);
    }

    

    /**
     * Returns a map of load values for the given host. The map as the
     * attribute name as a key and the load as a value.
     *
     * @return eHost  the exec host
     */
    private static Map<String, String> getLoad(ExecHost eHost) {
        log.entering(HostImpl.class.getName(), "getLoad", eHost.getName());
        Map<String, String> loadValues = new HashMap<String, String>();

        Set loadKeys = eHost.getLoadKeys();
        for (Object obj : loadKeys) {
            String key = (String) obj;
            
            loadValues.put(key, eHost.getLoad(key));
        }
        
        log.exiting(HostImpl.class.getName(), "getLoad", loadValues);

        return loadValues;
    }
    
    /**
     * This method is call from the java serialization after a host object
     * has been deserialized.
     * 
     * @param in  the input stream
     * @throws java.io.IOException
     * @throws java.lang.ClassNotFoundException
     */
    private void readObject(java.io.ObjectInputStream in)
            throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        lock = new ReentrantLock();
        stateChangedCondition = lock.newCondition();
        evtSupport = EventListenerSupport.<HostListener>newSynchronInstance(HostListener.class);
        transitionId = new AtomicLong();
        transIdOfResourcePurge = -1;
        installer = new AtomicReference<InstallationSequence>();
        updateToStringResult();
    }
    
    /**
     * get the next transition id
     * @return the next transition id
     */
    private long getNextTransitionId() {
        return transitionId.incrementAndGet();
    }
    
    /**
     * Check that the <code>transitionId</code> is the current transition id
     * @return <code>true</code> if <code>transitionId</code> is the current transition id
     */
    private boolean isCurrentTransition(long transitionId) {
        return transitionId == this.transitionId.get();
    }
    
    /**
     * check that the transitionId is the current transition id.
     * 
     * @param transitionId the transition id
     * @throws com.sun.grid.grm.service.impl.ge.HostTransitionExpiredException if the transitionId is not the current transition id
     */
    private void checkTransitionId(long transitionId) throws HostTransitionExpiredException {
        long cid = this.transitionId.get();
        if (transitionId != cid) {
            throw new HostTransitionExpiredException(this, transitionId, cid);
        }
    }
    
    
    /**
     * Set the install sequence
     * @param installer the installer
     */
    private void setInstaller(InstallationSequence installer) {
        this.installer.set(installer);
    }
    
    /**
     * Get the current installer
     * @return
     */
    private InstallationSequence getInstaller() {
        return installer.get();
    }
    
    /**
     * Set the current installer to <code>null</code> if the <code>tmpInstaller</code> is the current installer
     * @param tmpIinstaller the assumed current installer
     */
    private void resetInstaller(InstallationSequence tmpInstaller) {
        this.installer.compareAndSet(tmpInstaller, null);
    }
    
    
    /**
     * removes this host from the @allhost group and
     * resets the slots setting in the all.q. These settings
     * are created after the install. There is currently no
     * flag to disable the creation of default configurations.
     * 
     * @param jgdi connection to qmaster
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     */
    public void cleanupDefault(GEConnection jgdi) throws GrmException {
        log.entering(HostImpl.class.getName(), "cleanupDefault");

        if (!jgdi.removeHostfromHostgroup("@allhosts", hostName)) {
            throw new GrmException("h.ex.rmAllHosts", BUNDLE, hostName);
        }

        ClusterQueue cqueue = jgdi.getClusterQueue("all.q");
        Integer slots = cqueue.removeJobSlots(hostName.getHostname());
        jgdi.updateClusterQueue(cqueue);
        if(log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "h.rmSlots", new Object[] { "all.q", slots });
        }
        
        log.exiting(HostImpl.class.getName(), "cleanupDefault");
    }
    
    /**
     * Reports the number of running jobs on the host
     * 
     * @return the number of jobs
     * @param jgdi connection to qmaster
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     */
    public int getNumberOfJobsOnThisHost(GEConnection jgdi) throws GrmException {
        log.entering(HostImpl.class.getName(), "getNumberOfJobsOnThisHost");

        int jobCount = 0;
        
        QueueInstanceSummaryOptions options = new QueueInstanceSummaryOptions();

        options.setShowFullOutput(true);
        JobStateFilter.State states[] = new JobStateFilter.State[]{JobStateFilter.RUNNING};
        options.setJobStateFilter(new JobStateFilter(states));
        QueueFilter qFilter = new QueueFilter();
        qFilter.addQueue("*@" + hostName.getHostname());
        options.setQueueFilter(qFilter);
        
        QueueInstanceSummaryResult result = jgdi.getQueueInstanceSummary(options);

        Iterator iter = result.getQueueInstanceSummary().iterator();
        while(iter.hasNext()) {
           QueueInstanceSummary qis = (QueueInstanceSummary)iter.next();
           jobCount += qis.getJobList().size();
        }

        log.exiting(HostImpl.class.getName(), "getNumberOfJobsOnThisHost", jobCount);
        return jobCount;
    }

    
    /**
     * Update the information about this host
     *
     * @param service  the server to which this host belongs to
     * @param eHost    the exec host object for this host
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     * @return <code>true</code> if the resource properties has changed
     */
    public boolean update(GEServiceAdapterImpl service, ExecHost eHost) throws GrmException {
        
        log.entering(HostImpl.class.getName(), "update", eHost);
        
        boolean hasLoadValue = false;
        Set<String> changedProperties = Collections.<String>emptySet();
        Resource old = null;
        
        boolean isStaticHost = service.getConfigHelper().isStaticHost(service.getExecutionEnv(), this);
        
        lock.lock();
        try {
            HostState currentState = getHostState();
            // We only process the jgdi event if the exec host is not uninstalled
            // Otherwise we have a raise condition in the event protocol
            if (!HostState.HOST_REMOVED.equals(currentState)) {
                old = resource.clone();

                changedProperties = updateComplexes(service, eHost, old.getProperties());
                // Update the static flag
                // The change the static flag automatically only if the user have
                // not marked the resource manually as static
                if(!userSetStaticFlag && isStaticHost != resource.isStatic()) {
                    resource.setStatic(isStaticHost);
                    changedProperties.add(HostResourceType.STATIC.getName());
                }

                // If the load value np_load_avg we can assume that the execd
                // has been started
                Complex np_load_avg = complexes.get("np_load_avg");
                if (np_load_avg == null) {
                    if (log.isLoggable(Level.FINE) && isRunning()) {
                        log.log(Level.FINE, "h.no_load_complex", new Object [] { getName().toString(), "np_load_avg" });
                    }
                } else if (np_load_avg.getValue() == null || np_load_avg.getValue().length() == 0) {
                    if (log.isLoggable(Level.FINE) && isRunning()) {
                        log.log(Level.FINE, "h.empty_load_complex", new Object [] { getName().toString(), "np_load_avg" });
                    }
                } else {
                    if (log.isLoggable(Level.FINE) && !isRunning()) {
                        log.log(Level.FINE, "h.found_load_complex", new Object [] { getName().toString(), "np_load_avg" });
                    }
                    hasLoadValue = true;
                }

                // Only if the host is in a active state we set the host state to RUNNING
                // Otherwise it could happen that UNASSIGNING resource goes into ASSIGNED state
                switch (currentState) {
                    case HOST_UNINSTALLING:
                        if(!hasLoadValue) {
                            // issue #571
                            // If the uninstallation is in progress we do not set the
                            // host into HOST_REMOVED state.
                            // If we do it the RESOURCE_REMOVED event will be sent before
                            // the uninstallation is finished
                            InstallationSequence inst = getInstaller();
                            if (inst instanceof ExecdUninstaller) {
                                // Uninstaller is running
                                log.log(Level.FINE, "h.update.uninstallInProgress", this.getName());
                                ((ExecdUninstaller)inst).setExecdStopped(true);
                            } else {
                                setHostState(HostState.HOST_REMOVED, I18NManager.formatMessage("h.notRunning", BUNDLE));
                            }
                        }
                        break;
                    case HOST_REMOVED:
                        break;
                    case HOST_INSTALLING:
                    case HOST_STARTING:
                    case HOST_INSTALLED:
                        // We set the set of the host to RUNNING only if an exec event with load
                        // values is received
                        if (hasLoadValue) {
                            // issue #571
                            // If the installation is in progress we do not set the
                            // host into HOST_RUNNING state.
                            // If we do it the RESOURCE_ADDED event will be sent before
                            // the installation is finished
                            InstallationSequence inst = getInstaller();
                            if (inst instanceof ExecdInstaller) {
                                // Installation is still in progress
                                log.log(Level.FINE, "h.update.installInProgress", getName());
                                ((ExecdInstaller)inst).setExecdStarted(true);
                            } else {
                                setHostState(HostState.HOST_RUNNING, I18NManager.formatMessage("h.updated", BUNDLE));
                            }
                        }
                        break;
                    case HOST_ERROR:
                        if (hasLoadValue) {
                            // HostImpl was in error state
                            // however we got an event for this host => set the state to running
                            setHostState(HostState.HOST_RUNNING, I18NManager.formatMessage("h.updated", BUNDLE));
                        }
                        break;
                    default:
                        if (hasLoadValue) {
                            setHostState(HostState.HOST_RUNNING, I18NManager.formatMessage("h.updated", BUNDLE));
                        } else {
                            setHostState(HostState.HOST_ERROR, I18NManager.formatMessage("h.notRunning", BUNDLE));
                        }
                }
            }
        } finally {
            lock.unlock();
        }
        boolean ret = !changedProperties.isEmpty();
        if (ret) {
            if (log.isLoggable(Level.FINE)) {
                Map<String, Object> newProps = resource.getProperties();
                for (String prop : changedProperties) {
                    log.log(Level.FINE, "h.propChanged", new Object[]{hostName, prop, old.getProperties().get(prop), newProps.get(prop)});
                }
            }
            evtSupport.getProxy().resourceChanged(this, 
                    AbstractResourceChanged.getChanges(old, resource), I18NManager.formatMessage("h.updated", BUNDLE));
        }
        
        log.exiting(HostImpl.class.getName(), "update", ret);
        return ret;
    }
    
    private Set<String> updateComplexes(GEServiceAdapterImpl service, ExecHost eHost, Map<String, Object> orgProperties) 
       throws GrmException {
        
        MappingConfig mapping = service.getConfigHelper().getMapping();


        Map<String, Complex> oldComplexes = complexes;

        complexes = new HashMap<String, Complex>();

        Set<String> changedProperties = new HashSet<String>();

        List<String> changedLoadValues = addLoadValues(eHost, mapping, resource, complexes);
        changedProperties.addAll(changedLoadValues);

        List<String> changedConsumables = addConsumables(eHost, mapping, resource, complexes);
        changedProperties.addAll(changedConsumables);

        // Remove the used complexes from the oldComplex map
        for (String complex : complexes.keySet()) {
            oldComplexes.remove(complex);
        }

        // Remove all resource properties which has been added by a 
        // complex which is not longer available
        for (Complex complex : oldComplexes.values()) {
            for (String prop : complex.getPropertyNames()) {
                boolean notFound = true;
                for (Complex c1 : complexes.values()) {
                    if (c1.getPropertyNames().contains(prop)) {
                        notFound = false;
                        break;
                    }
                }
                if (notFound) {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "h.removeComplexProperty", new Object[]{prop, complex.name, hostName});
                    }
                    resource.removeProperty(prop);
                    changedProperties.add(prop);
                }
            }
        }

        // We do not allow the update of unmodifyable properties
        for (String prop : UNMODIFIABLE_KEYS_FOR_UPDATE_METHOD) {
            if (changedProperties.remove(prop)) {
                resource.setProperty(prop, orgProperties.get(prop));
            }
        }

        return changedProperties;
    }
    
    /**
     * Update the usage of this host
     * @param usage the new usage of this host
     */
    public void updateUsage(Usage usage) {
        boolean changed = false;
        Usage oldUsage = null;
        lock.lock();
        try {
            oldUsage = resource.getUsage();
            if(!usage.equals(oldUsage)) {
                resource.setUsage(usage);
                updateToStringResult();
                // We do not send the properties changed event if uninstall
                // is in progress or if it is not installed
                if(!(state.equals(HostState.HOST_UNINSTALLING) || state.equals(HostState.HOST_REMOVED))) {
                    changed = true;
                }
            }
        } finally {
            lock.unlock();
        }
        if (changed) {
            ResourceUsageChanged rpu = new ResourceUsageChanged(oldUsage, usage);
            evtSupport.getProxy().resourceChanged(this, Collections.<ResourceChanged>singleton(rpu), I18NManager.formatMessage("h.usageChanged", BUNDLE, oldUsage, usage));
        }
    }

    /**
     * Modify the properties of this host
     * @param operations to be done on this resource
     * @param service that owns the resource
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the new properties contains errors
     */
    public Collection<ResourceChanged> modify(GEServiceAdapterImpl service, Collection<ResourceChangeOperation> operations) throws InvalidResourcePropertiesException {

        ArrayList<ResourceChangeOperation> allowedOps = new ArrayList<ResourceChangeOperation>(operations.size());
        for(ResourceChangeOperation op: operations) {
                if (UNMODIFIABLE_KEYS_FOR_MODIFY_METHOD.contains(op.getKey())) {
                    continue;
                }
                if(HostResourceType.STATIC.getName().equals(op.getKey())) {
                    // We do not allow reseting the static flag of static host resources
                    // (qmaster or static host defined in the configuration of GEAdapter)
                    if (Boolean.FALSE.equals(op.getValue()) &&
                        service.getConfigHelper().isStaticHost(service.getExecutionEnv(), this)) {
                        throw new InvalidResourcePropertiesException(I18NManager.formatMessage("h.ex.invalidStatic", BUNDLE));
                    }
                }
                allowedOps.add(op);
        }

        Collection<ResourceChanged> changes = Collections.<ResourceChanged>emptyList();
        lock.lock();
        try {
            Resource tmpResource = resource.clone();

            boolean tmpUserSetStaticFlag = userSetStaticFlag;
            changes = tmpResource.modify(allowedOps);
            for (ResourceChanged rc : changes) {
                if(rc.getName().equals(HostResourceType.HOSTNAME.getName())) {
                    throw new InvalidResourcePropertiesException(I18NManager.formatMessage("h.ex.HostnameImmutable", BUNDLE));
                }
                if (rc.getName().equals(HostResourceType.STATIC.getName())) {
                    // The static flag has been changed
                    tmpUserSetStaticFlag = tmpResource.isStatic();
                }
            }
            // All changes are correct, commit the changes
            this.resource = tmpResource;
            userSetStaticFlag = tmpUserSetStaticFlag;
        } finally {
            lock.unlock();
        }
        if (changes.size() > 0) {
            evtSupport.getProxy().resourceChanged(this, changes, I18NManager.formatMessage("h.modified", BUNDLE));
        }
        return changes;
    }
    
    /**
     * Set the ambiguous flag for this host
     * @param ambiguous the ambiguous flag
     * @param annotation the annotation for the resource
     */
    public void setAmbiguous(boolean ambiguous, String annotation) {
        
        boolean changed = false;
        String oldAnnotation = null;
        lock.lock();
        try {
            if(resource.isAmbiguous() != ambiguous) {
                resource.setAmbiguous(ambiguous);
                oldAnnotation = resource.getAnnotation();
                resource.setAnnotation(annotation);
                changed = true;
            }
        } finally {
            lock.unlock();
        }
        if (changed) {
            Collection<ResourceChanged> changes = new ArrayList<ResourceChanged>(2);
            changes.add(new ResourceAmbiguousStateChanged(resource.isAmbiguous()));
            changes.add(new ResourceAnnotationChanged(oldAnnotation, annotation));
            evtSupport.getProxy().resourceChanged(this, changes, annotation);
        }
    }

    
    private static List<String> addLoadValues(ExecHost eHost, MappingConfig mapping, Resource resource, 
                                         Map<String,Complex> complexes) throws InvalidResourcePropertiesException {
        log.entering(HostImpl.class.getName(), "addLoadValues",
                     new Object[] { eHost, mapping });

        List<String> ret = new LinkedList<String>();
        
        Collection<ResourceChangeOperation> batch = new LinkedList<ResourceChangeOperation>();
        for (Map.Entry<String,String> entry: getLoad(eHost).entrySet()) {
            Map<String,Object> props = MappingUtil.mapComplex(mapping, entry.getKey(), entry.getValue());
            Complex complex = new Complex(entry.getKey(), entry.getValue(), props.keySet());
            for (Map.Entry<String, Object> entry2 : props.entrySet()) {
                ResourceChangeOperation update = new ResourcePropertyUpdateOperation(entry2.getKey(), entry2.getValue());
                batch.add(update);
            }
            complexes.put(complex.getName(), complex);
        }
        for (ResourceChanged rpc : resource.modify(batch)) {
            ret.add(rpc.getName());
        }
        log.exiting(HostImpl.class.getName(), "addLoadValues", ret);
        return ret;
    }
    
    private static List<String> addConsumables(ExecHost eHost, MappingConfig mapping, 
                                               Resource resource, Map<String,Complex> complexes) throws InvalidResourcePropertiesException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(HostImpl.class.getName(), "addConsumables", new Object[] { eHost, mapping, resource, complexes });
        }

        List<String> ret = new LinkedList<String>();
        List complexList = eHost.getConsumableConfigList();
        Collection<ResourceChangeOperation> batch = new LinkedList<ResourceChangeOperation>();
        for (Object obj : complexList) {
            ComplexEntry entry = (ComplexEntry) obj;
            String name = entry.getName();
            String value = entry.getStringval();

            Map<String,Object> props = MappingUtil.mapComplex(mapping, name, value);
            
            Complex complex = new Complex(name,value, props.keySet());
            for (Map.Entry<String, Object> entry2 : props.entrySet()) {
                ResourceChangeOperation update = new ResourcePropertyUpdateOperation(entry2.getKey(), entry2.getValue());
                batch.add(update);
            }
            complexes.put(complex.getName(), complex);
        }
        for (ResourceChanged rpc : resource.modify(batch)) {
            ret.add(rpc.getName());
        }
        log.exiting(HostImpl.class.getName(), "addConsumables", ret);
        return ret;
    }

    /**
     * Checks whether an execd is running or not.
     * @return if true, than the execd is up and running
     */
    public boolean isRunning() {
        log.entering(HostImpl.class.getName(), "isRunning");
        boolean ret;
        lock.lock();
        try {
            ret = getHostState().equals(HostState.HOST_RUNNING);
        } finally {
            lock.unlock();
        }
        log.exiting(HostImpl.class.getName(), "isRunning", ret);
        return ret;
    }
    
    /**
     * Gets the current state of the host. This can be installing,
     * uninstalling, starting up and others. 
     * thread save
     * @return the host state
     */
    public HostState getHostState() {
        log.entering(HostImpl.class.getName(), "getHostState", hostName.getHostname());
        HostState ret;
        lock.lock();
        try {
            ret = state;
        } finally {
            lock.unlock();
        }
        log.exiting(HostImpl.class.getName(), "getHostState", ret);
        return ret;
    }

    /**
     * Sets the host state and returns the previous state. 
     * Overrides any existing state. Is thread safe.
     *
     * @param newState the new state to set.
     * @param message the reason why the host state has changed
     * @return the previous state.
     */
    public HostState setHostState(HostState newState, String message) {
        log.entering(HostImpl.class.getName(), "setHostState", newState);
        
        HostState oldHostState = null;
        boolean changed = false;
        ResourceAnnotationUpdateOperation op = new ResourceAnnotationUpdateOperation(message);
        ResourceChanged annoChanged;
        lock.lock();
        try {
            oldHostState = this.state;
            if(!newState.equals(this.state))  {
                changed = true;
                this.state = newState;
                resource.setState(GEResourceAdapter.mapHostStateToResourceState(newState));
                updateToStringResult();
                stateChangedCondition.signalAll();
            }
            try {
                annoChanged = resource.modify(op);
            } catch (InvalidResourcePropertiesException ex) {
                IllegalStateException ex1 = new IllegalStateException("Illegal state, changing the annotation of the resource must be possible", ex);
                log.throwing(HostImpl.class.getName(), "setHostState", ex1);
                throw ex1;
            }
        } finally {
            lock.unlock();
        }
        if(changed) {
            evtSupport.getProxy().hostStateChanged(this, oldHostState, newState, message);
        } else if (annoChanged.hasChangedResource()) {
            evtSupport.getProxy().resourceChanged(this, Collections.<ResourceChanged>singleton(annoChanged), message);
        }

        log.exiting(HostImpl.class.getName(), "setHostState", oldHostState);

        return oldHostState;
    }
    
    /**
     * Finalize the uninstall process
     * @param transitionId the transition id
     */
    public void finalizeUninstall(long transitionId) {
        log.entering(HostImpl.class.getName(), "finalizeUninstall");
        lock.lock();
        try {
            if(isCurrentTransition(transitionId)) {
                switch(getHostState()) {
                    case HOST_UNINSTALLING: {
                        setHostState(HostState.HOST_ERROR, I18NManager.formatMessage("h.waitForShutdownTimeout", BUNDLE));
                    }
                }
            }
        } finally {
            lock.unlock();
        }
        // Do nothing, because another action has modified the host
        log.exiting(HostImpl.class.getName(), "finalizeUninstall");
    }
    
    /**
     * Checks weather the host is static
     *
     * @return true, it is static
     */
    public boolean isStatic() {
        log.entering(HostImpl.class.getName(), "isStatic");
        boolean ret;
        lock.lock();
        try {
            ret = resource.isStatic();
        } finally {
            lock.unlock();
        }
        log.exiting(HostImpl.class.getName(), "isStatic", ret);
        return ret;
    }
    
    /**
     * Change the static flag of the resource of this host.
     * 
     * <p>If the <code>isStatic</code> parameter is false and the static flag has
     * been set with the <code>modify</code> method the static flag will be not
     * cleared.</p>
     * 
     * @param isStatic the new value of the static flag
     */
    public void setStatic(boolean isStatic) {
        log.entering(HostImpl.class.getName(), "setStatic", isStatic);
        boolean changed = false;
        boolean clearIgnored = false;
        lock.lock();
        try {
            if(resource.isStatic() != isStatic) {
                if (isStatic) {
                    resource.setStatic(true);
                    changed = true;
                } else {
                     // Issue 586: If the static flag has been set with the
                     //            modify method clearing it with the setStatic 
                     //            method is not possible.
                     if (userSetStaticFlag) {
                         clearIgnored = true;
                     } else {
                         resource.setStatic(false);
                         changed = true;
                     }
                }
            }
        } finally {
            lock.unlock();
        }
        if (clearIgnored) {
            log.log(Level.FINE, "h.userstatic.ignore_clear", getName().toString());
        }
        
        if (changed) {
            ResourceChanged updated = new ResourcePropertyUpdated(AbstractResourceType.STATIC.getName(), isStatic, !isStatic);
            String message;
            if (isStatic) {
                message = "h.static";                        
            } else {
                message = "h.nonstatic";
            }
            evtSupport.getProxy().resourceChanged(this, 
                    Collections.<ResourceChanged>singletonList(updated), I18NManager.formatMessage(message, BUNDLE));
        }
        log.exiting(HostImpl.class.getName(), "setStatic");
    }
    
    /**
     * Get a copy of the resource
     * @return the copy of the resource 
     */
    public Resource getResource() {
        log.entering(HostImpl.class.getName(), "getResource");
        Resource ret = null;
        lock.lock();
        try {
            ret = resource.clone();
        } finally {
            lock.unlock();
        }
        log.exiting(HostImpl.class.getName(), "getResource", ret);
        return ret;
    }

    /**
     * Get the id of the resource
     * @return the resource id
     */
    public ResourceId getResourceId() {
        lock.lock();
        try {
            return resource.getId();
        } finally {
            lock.unlock();
        }
    }

    
    /**
     * Gets the fully qualified host name (resolved)
     *
     * @return the <code>Hostname</code> object
     */
    public Hostname getName() {
        log.entering(HostImpl.class.getName(), "getName");
        log.exiting(HostImpl.class.getName(), "getName", this.hostName);
        return this.hostName;
    }
    
    /**
     * Is this host in the admin host list of qmaster.
     * 
     * @param jgdi  connection to qmaster
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     * @return <code>true</code> if this host is an admin host
     */
    public boolean isAdminHost(GEConnection jgdi) throws GrmException {
        List hosts = jgdi.getAdminHostList();
        for (Object obj : hosts) {
            AdminHost host = (AdminHost) obj;
            if (hostName.equals(Hostname.getInstance(host.getName()))) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Makes this host to an adminHost, if it is not yet one.
     * 
     * @return true - the HostImpl was added as an AdminHost, 
     *          false - the host was already an AdminHost
     * @param jgdi  connection to qmaster
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     */
    public boolean makeAdminHost(GEConnection jgdi) throws GrmException {
        log.entering(HostImpl.class.getName(), "makeAdminHost");
        boolean ret = false;
        if(!isAdminHost(jgdi)) {
            log.log(Level.FINER, "h.add.adminHost", hostName);
            jgdi.addAdminHost(this.hostName.getHostname());
            ret = true;
        }
        return ret;
    }
    
    /**
     * Changes this host into a none Admin host.
     *
     * @param jgdi  connection to qmaster
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     */
    public void removeAdminHost(GEConnection jgdi) throws GrmException {
        log.entering(HostImpl.class.getName(), "removeAdminHost");

        if (isAdminHost(jgdi)) {
            log.log(Level.FINER, "h.rm.adminHost", hostName);
            jgdi.deleteAdminHost(hostName.getHostname());
        }
        log.exiting(HostImpl.class.getName(), "removeAdminHost");
    }
    
    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     * 
     * @return a negative integer, zero, or a positive integer as this object
     * 		is less than, equal to, or greater than the specified object.
     * @param o the HostImpl to be compared.
     */
    public int compareTo(Host o) {
        log.entering(HostImpl.class.getName(), "compareTo", o);
        int ret = this.hostName.compareTo(o.getName());
        log.exiting(HostImpl.class.getName(), "compareTo", ret);
        return ret;
    }
    
    /**
     * Compares this HostImpl to another object.
     * @param object another object
     * @return true if this Resource is the same as the other object
     */
    @Override 
    public boolean equals(Object object) {
        return object instanceof HostImpl
            && this.hostName.equals(((HostImpl)object).getName());
    }

    /**
     *  Get the hashcode of this object.
     * 
     *  @return the hashcode of this object
     */
    @Override
    public int hashCode() {
        return hostName.hashCode();
    }
   
    private void updateToStringResult() {
        log.entering(HostImpl.class.getName(), "toString");
        StringBuilder ret = new StringBuilder();
        ret.append(hostName.getHostname());
        ret.append('[');
        ret.append(getHostState());
        ret.append(',');
        ret.append(resource.getUsage());
        ret.append(']');
        log.exiting(HostImpl.class.getName(), "toString", ret);
        toStringResult = ret.toString();
    }
    
    /**
     * Returns a string representation of the object.
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return toStringResult;
    }
    
    /**
     * Prepare the host for a reset
     * @return the transition id
     */
    public long prepareForReset() {
        long ret = -1;
        InstallationSequence orgInstaller = null;
        lock.lock();
        try {
            setHostState(HostState.HOST_RESETING, I18NManager.formatMessage("h.reseting", BUNDLE));
            ret = getNextTransitionId();
            orgInstaller = getInstaller();
        } finally {
            lock.unlock();
        }
        if (orgInstaller != null) {
            orgInstaller.abort();
        }
        return ret;
    }
    
    /**
     * Reset the host.
     * 
     * <p>If a host is reseted it first aborts running install or uninstall.
     * If no installation is in progress the reset starts to reinstall
     * the execd.</p>
     * 
     * <p>If no executor is running no execd can be installed. In this case the 
     * reset method reads over jgdi the load value of the execd. If a this host 
     * reports a load it is assumed that the execd is still running, the resource 
     * goes into ASSIGNED state.If the execd does not report a load the resource 
     * goes into ERROR state</p>
     * @param service the owning service
     * @param transitionId  the transition id
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if the reinstall of the execd failed
     * @throws HostTransitionExpiredException if the transition is expired
     */
    public HostState reset(GEServiceAdapterImpl service, long transitionId) throws InstallException, HostTransitionExpiredException {
        log.entering(HostImpl.class.getName(), "reset", transitionId);
        
        HostState ret = HostState.HOST_NONE;
        
        InstallationSequence orgInstaller = null;
        lock.lock();
        try {
            checkTransitionId(transitionId);
            orgInstaller = getInstaller();
        } finally {
            lock.unlock();
        }
        if(orgInstaller != null) {
            // Install or Uninstall is in progress => abort it
            orgInstaller.abort();
        }
        ExecdInstaller myInstaller = null;
        lock.lock();
        try {
            setHostState(HostState.HOST_INSTALLING, I18NManager.formatMessage("h.installing", BUNDLE));
            myInstaller = new ExecdInstaller(service, this);
            setInstaller(myInstaller);
        } finally {
            lock.unlock();
        }
        try {
            ret = HostState.HOST_INSTALLING;
            // Reinstall the execd
            myInstaller.execute();
            lock.lock();
            try {
                resetInstaller(myInstaller);
                // Under some rare circumstances in can happen that before the EXECD_ADD event
                // the EXECD_MOD event is received. In this case HostImpl.update will set
                // the host state to RUNNING
                // In this case we must not wait for the EXECD_ADD_EVENT
                //
                // => Check the the host state is still HOST_INSTALLING
                if(getHostState().equals(HostState.HOST_INSTALLING)) {
                    if(myInstaller.isExecdStarted()) {
                        log.log(Level.FINE, "h.install_finished.execd_started", getName().toString()); 
                        setHostState(HostState.HOST_RUNNING, I18NManager.formatMessage("h.updated", BUNDLE));
                        ret = HostState.HOST_RUNNING;
                    } else {
                        log.log(Level.FINE, "h.install_finished.execd_not_started", getName().toString()); 
                        setHostState(HostState.HOST_INSTALLED, I18NManager.formatMessage("h.waitForExecd", BUNDLE));
                        ret = HostState.HOST_INSTALLED;
                    }
                }
            } finally {
                lock.unlock();
            }
        } catch(InstallNotPossibleException ex) {
            // It can be the the executor on this host is not active
            // If the execd reports load we assume that it is working
            try {
                ExecHost eh = service.getJgdi().getExecHost(getName().getHostname());
                if(eh == null) {
                    // Execd host object does not exists, this means that
                    // execd for the host resource is not installed
                    setHostState(HostState.HOST_ERROR, ex.getLocalizedMessage());
                } else {
                    // qmaster known the execd. Update the information
                    update(service, eh);
                }
            } catch (GrmException ex1) {
                // Must be a communiation error. Assume that the execd is not running
                throw new InstallException(ex1, "h.ex.jgdierror", getName().getHostname(), ex1.getLocalizedMessage());
            }
        } catch(InstallException ex) {
            setHostState(HostState.HOST_ERROR, ex.getLocalizedMessage());
            throw ex;
        } finally {
            resetInstaller(myInstaller);
        }
        log.exiting(HostImpl.class.getName(), "reset", ret);
        return ret;
    }
    
    /**
     * Finalize the reset of the host.
     * 
     * @param transitionId the id of the reset transition
     */
    public void finalizeReset(long transitionId) {
        finalizeInstall(transitionId);
    }
    
    /**
     * Prepare this host for installation.
     * 
     * <p>This method checks that the host is in valid state allowing that this
     *    host is installed.</p>
     * 
     * <p>Uninstall is not possible if the host is in HOST_INSTALLING, HOST_RUNNING,
     *    HOST_STARTING, HOST_UNINSTALLING, HOST_RESETING or HOST_ERROR state.</p>
     * 
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if the host is not
     *           in a valid state which allows uninstallation. 
     * 
     * @return the id of the transition
     */
    public long prepareForInstall() throws InstallException {
        lock.lock();
        try {
            switch(getHostState()) {
                case HOST_INSTALLING:
                    throw new InstallException("h.ex.install.installing", getName());
                case HOST_RUNNING:
                case HOST_STARTING:
                    throw new InstallException("h.ex.install.installed", getName());
                case HOST_UNINSTALLING:
                    throw new InstallException("h.ex.install.uninstalling", getName());
                case HOST_RESETING:
                    throw new InstallException("h.ex.install.reseting", getName());
                case HOST_ERROR:
                {
                    String oldAnnotation = resource.getAnnotation();
                    // Issue 437: If resource is in ERROR state we can not install
                    //            however RP is waiting for an answer
                    //   => We change for a short time the state
                    setHostState(HostState.HOST_INSTALLING, I18NManager.formatMessage("h.installing", BUNDLE));
                    setHostState(HostState.HOST_ERROR, oldAnnotation);
                    throw new InstallException("h.ex.install.error", getName());
                }
                case HOST_REMOVED:
                case HOST_NONE:
                    break;
                default:
                    throw new InstallException("h.ex.install.ustate", getName(), getHostState());
            }
            setHostState(HostState.HOST_INSTALLING, I18NManager.formatMessage("h.installing", BUNDLE));
            return getNextTransitionId();
        } finally {
            lock.unlock();
        }        
    }
    
    /**
     * Install a execd daemon on the host.
     * 
     * <p>The installation can only be executed if the host is prepared for installation.
     *    This means that the host state is HOST_INSTALLING.</p>
     *
     * <p>If the host is in HOST_INSTALLING state the execd installation is started.</p>
     * 
     * @param  service the service of the host
     * @param  transitionId the transition id
     * @see    #prepareForInstall
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if the installation has been failed
     *            the host state has been set to HOST_ERROR
     * @throws InstallNotPossibleException if the installation was not possible. In this case the host
     *            has been set to HOST_NONE
     * @return HOST_RUNNING if qmaster reported during the installation that the execd has been started
     *         HOST_INSTALLED if the EXECD_ADD event is outstanding
     * @throws HostTransitionExpiredException if the transition is expired
     */
    public Host.HostState install(GEServiceAdapterImpl service, long transitionId) throws InstallException, InstallNotPossibleException, HostTransitionExpiredException {
        log.entering(HostImpl.class.getName(), "install", transitionId);
        Host.HostState ret = HostState.HOST_NONE;
        ExecdInstaller myInstaller = null;
        lock.lock();
        try {
            checkTransitionId(transitionId);
            if(!getHostState().equals(HostState.HOST_INSTALLING)) {
                throw new InstallException("h.ex.install.ustate", getName(), getHostState());
            }
            myInstaller = new ExecdInstaller(service, this);
            setInstaller(myInstaller);
        } finally {
            lock.unlock();
        }        
        
        try {
            ret = HostState.HOST_INSTALLING;
            myInstaller.execute();
            lock.lock();
            try {
                resetInstaller(myInstaller);
                // Under some rare circumstances in can happen that before the EXECD_ADD event
                // the EXECD_MOD event is received. In this case HostImpl.update will set
                // the host state to RUNNING
                // In this case we must not wait for the EXECD_ADD_EVENT
                //
                // => Check the the host state is still HOST_INSTALLING
                if(getHostState().equals(HostState.HOST_INSTALLING)) {
                    if(myInstaller.isExecdStarted()) {
                        log.log(Level.FINE, "h.install_finished.execd_started", getName().toString()); 
                        setHostState(HostState.HOST_RUNNING, I18NManager.formatMessage("h.updated", BUNDLE));
                        ret = HostState.HOST_RUNNING;
                    } else {
                        log.log(Level.FINE, "h.install_finished.execd_not_started", getName().toString()); 
                        setHostState(HostState.HOST_INSTALLED, I18NManager.formatMessage("h.waitForExecd", BUNDLE));
                        ret = HostState.HOST_INSTALLED;
                    }
                }
            } finally {
                lock.unlock();
            }
           
        } catch(InstallNotPossibleException ex) {
            lock.lock();
            try {
                // It can happen that meanwhile a EXECD_MOD event has changed
                // the host state.
                if(getHostState().equals(HostState.HOST_INSTALLING)) {
                    setHostState(HostState.HOST_NONE, ex.getLocalizedMessage());
                }
            } finally {
                lock.unlock();
            }
            throw ex;
        } catch(InstallException ex) {
            lock.lock();
            try {
                // It can happen that meanwhile a EXECD_MOD event has changed
                // the host state.
                if(getHostState().equals(HostState.HOST_INSTALLING)) {
                    setHostState(HostState.HOST_ERROR, ex.getLocalizedMessage());
                }
            } finally {
                lock.unlock();
            }
            throw ex;
        } finally {
            resetInstaller(myInstaller);
        }
        log.exiting(HostImpl.class.getName(), "install", ret);
        return ret;
    }
    
    /**
     * Finalizes the installation of an execd.
     * 
     * <p>If this host object did not receive load values from qmaster
     *    this method is set to HOST_ERROR.</p>
     * 
     * @param transitionId the transitionId
     */
    public void finalizeInstall(long transitionId) {
        log.entering(HostImpl.class.getName(), "finalizeInstall", transitionId);
        lock.lock();
        try {
            if(isCurrentTransition(transitionId)) {
                switch(getHostState()) {
                    case HOST_INSTALLING:
                    case HOST_STARTING:
                        setHostState(HostState.HOST_ERROR, I18NManager.formatMessage("h.waitForStartTimeout", BUNDLE));
                        break;
                    default:
                        // Ignore
                }
            }
        } finally {
            lock.unlock();
        }
        // Do nothing, because another action has modified the host
        log.exiting(HostImpl.class.getName(), "finalizeInstall");
    }
    
    /**
     * Prepare the uninstallation of the execd in the host.
     * 
     * <p>This method checks whether the host is in a valid state which allows the
     *    uninstallation. It is not possible if the host state is
     *    HOST_INSTALLING, HOST_RESETING or HOST_ERROR.</p>
     * 
     * @param descr the resource removal descriptor
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if the host is not in the state
     *           that a uninstall is possible.
     * @throws InvalidResourceStateException  if the resource is static or not in ASSIGNED state
     * @return the id of the host transition
     */
    public long prepareForUninstall(ResourceRemovalDescriptor descr) throws InstallException, InvalidResourceStateException {
        lock.lock();
        try {
            if (descr instanceof ResourcePurgeDescriptor) {
                // allow purge of resource in any state
                long ret = getNextTransitionId();
                transIdOfResourcePurge = ret;
                setHostState(HostState.HOST_UNINSTALLING, I18NManager.formatMessage("h.purging", BUNDLE));
                return ret;
            } else {
                AbstractResourceAdapter.canResourceBeRemoved(resource, descr);
                switch (getHostState()) {
                    case HOST_INSTALLING:
                        throw new InstallException("h.ex.uninstall.installing", getName());
                    case HOST_RESETING:
                        throw new InstallException("h.ex.uninstall.reseting", getName());
                    case HOST_ERROR:
                        String oldAnnotation = resource.getAnnotation();
                        // Issue 437: If resource is in ERROR state we can not uninstall
                        //            however RP is waiting for an answer
                        //   => We change for a short time the state
                        setHostState(HostState.HOST_UNINSTALLING, I18NManager.formatMessage("h.uninstalling", BUNDLE));
                        setHostState(HostState.HOST_ERROR, oldAnnotation);
                        throw new InstallException("h.ex.uninstall.error", getName());
                    case HOST_REMOVED:
                    case HOST_NONE:
                        throw new InstallException("h.ex.uninstall.notInstalled", getName());
                    case HOST_INSTALLED:
                    case HOST_RUNNING:
                    case HOST_STARTING:
                        break;
                    default:
                        throw new InstallException("h.ex.install.ustate", getName(), getHostState());
                }
            }
            setHostState(HostState.HOST_UNINSTALLING, I18NManager.formatMessage("h.uninstalling", BUNDLE));
            return getNextTransitionId();
        } finally {
            lock.unlock();
        }
    }
    
    
    /**
     * Uninstall the execd daemon on this host.
     *
     * @param service the service of this host
     * @param forced if <code>true</code> the uninstall will terminate running jobs
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if an unrecoverable
     *           error during uninstall process occured
     * @throws HostTransitionExpiredException if the transition is expired
     */
    public HostState uninstall(GEServiceAdapterImpl service, long transitionId, boolean forced) throws InstallException, HostTransitionExpiredException {
        if (log.isLoggable(Level.FINER)) {
            log.entering(HostImpl.class.getName(), "uninstall", new Object [] { service, transitionId, forced });
        }
        HostState ret = HostState.HOST_NONE;
        ExecdUninstaller uninstaller = null;
        lock.lock();
        try {
            checkTransitionId(transitionId);

            if ( transIdOfResourcePurge == transitionId ) {
                // forced removal of resource from system (and resource was in error state)
                log.log(Level.FINE, "h.uninstall_finished.purged", getName().toString());
                setHostState(HostState.HOST_REMOVED, I18NManager.formatMessage("h.purged", BUNDLE));
                ret = HostState.HOST_REMOVED;
                log.exiting(HostImpl.class.getName(), "uninstall", ret);
                return ret;
            }
            
            if (!getHostState().equals(HostState.HOST_UNINSTALLING)) {
                throw new InstallException("h.ex.install.ustate", getName(), getHostState());
            }
            uninstaller = new ExecdUninstaller(service, this, forced);
            setInstaller(uninstaller);
        } finally {
            lock.unlock();
        }
        try {
            ret = HostState.HOST_UNINSTALLING;
            uninstaller.execute();
            lock.lock();
            try {
                resetInstaller(uninstaller);
                if (getHostState().equals(HostState.HOST_UNINSTALLING)) {
                    if(uninstaller.isExecdStopped()) {
                        log.log(Level.FINE, "h.uninstall_finished.execd_stopped", getName().toString());
                        setHostState(HostState.HOST_REMOVED, I18NManager.formatMessage("h.notRunning", BUNDLE));
                        ret = HostState.HOST_REMOVED;
                    } else {
                        log.log(Level.FINE, "h.uninstall_finished.execd_not_stopped", getName().toString());
                        resource.setAnnotation(I18NManager.formatMessage("h.waitForDel", BUNDLE));
                        ret = HostState.HOST_UNINSTALLING;
                    }
                }
            } finally {
                lock.unlock();
            }
        } catch (InstallNotPossibleException ex) {
            // Uninstall was not possible, assume that the execd is still active
            lock.lock();
            try {
                // It can happen that an EXECD_MOD event has meanwhile modified
                // the host state
                // Adjust the host only if it is still HOST_UNINSTALLING
                if (getHostState().equals(HostState.HOST_UNINSTALLING)) {
                  // Will send the ASSGINING event
                  setHostState(HostState.HOST_INSTALLING, ex.getLocalizedMessage());
                  // Will send the ASSIGNED event
                  setHostState(HostState.HOST_RUNNING, ex.getLocalizedMessage());
                }
            } finally {
                lock.unlock();
            }
            throw ex;
        } catch (InstallException ex) {
            lock.lock();
            try {
                // It can happen that qmaster sent an EXECD_MOD without load
                // The update method will detect that EXECD is already down
                // It will also set the host state to HOST_REMOVED
                // We set the error state here only if nobody else has modified
                // the host state
                if (getHostState().equals(HostState.HOST_UNINSTALLING)) {
                    setHostState(HostState.HOST_ERROR, ex.getLocalizedMessage());
                }
            } finally {
                lock.unlock();
            }
            throw ex;
        } finally {
            resetInstaller(uninstaller);
        }
        log.exiting(HostImpl.class.getName(), "uninstall", ret);
        return ret;
    }
    
    private static class Complex implements Serializable {
        
        private final static long serialVersionUID = -2007120401L;
        private final String name;
        private final String value;
        private final List<String> propertyNames;
        
        public Complex(String name, String value, Collection<String> propertyNames) {
            this.name = name;
            this.value = value;
            this.propertyNames = Collections.unmodifiableList(new ArrayList<String>(propertyNames));
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }

        public List<String> getPropertyNames() {
            return propertyNames;
        }
    }
    

}
