/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.impl.ge.slo.JobHardRequestFilterAdapter;
import com.sun.grid.grm.service.slo.InvalidatedSLOContextException;
import com.sun.grid.grm.service.slo.impl.DefaultSLOContext;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.jgdi.monitoring.JobSummary;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummary;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * GEServiceSLOContext provides beside a snapshot of the resources
 * of a service also the shapshot of the last qstat run. 
 */
public class GEServiceSLOContext extends DefaultSLOContext {

    private AtomicReference<QueueInstanceSummaryResult> result;

    /**
     * Create a new instance of GEServiceSLOContext
     * @param resources  snapshot of the resources
     * @param result     result of the last qstat
     */
    public GEServiceSLOContext(List<Resource> resources, QueueInstanceSummaryResult result) {
        super(resources);
        if (result == null) {
            this.result = new AtomicReference<QueueInstanceSummaryResult>();
        } else {
            this.result = new AtomicReference<QueueInstanceSummaryResult>(result);
        }
    }

    /**
     *   Invalidate the slo context
     *   @return <p><code>true</code> if the call has marked the SLOContext as invalid
     *           or <code>false</code> if the SLOContext has been already invalidated
     *           before the call.</p>
     */
    @Override
    public boolean invalidate() {
        boolean ret = super.invalidate();
        if (ret) {
            result.set(null);
        }
        return ret;
    }

    /**
     * Get the hostname of the queue instance
     * @param qi  the queue instance
     * @return the hostname
     */
    public static Hostname getHostnameOfQueue(QueueInstanceSummary qi) {
        String name = qi.getName();

        int index = name.indexOf('@');
        if (index > 0) {
            String hostname = name.substring(index + 1);
            return Hostname.getInstance(hostname);
        } else {
            return null;
        }
    }

    private QueueInstanceSummaryResult getResult() throws InvalidatedSLOContextException {
        validate();
        return result.get();
    }

    /**
     * Get the host based statistic from the qstat result
     * @param jobFilter  if not null, include only information about jobs matching this job filter
     * @return map with one HostStatistic object for each host
     * @throws com.sun.grid.grm.service.slo.InvalidatedSLOContextException if the slo context has been invalidated
     */
    public Map<Hostname,HostStatistic> getHostStatistic(Filter<JobSummary> jobFilter) throws InvalidatedSLOContextException {
        QueueInstanceSummaryResult tmpResult = getResult();

        if (tmpResult == null) {
            return Collections.<Hostname, HostStatistic>emptyMap();
        }
        
        Map<Hostname,HostStatistic> ret = new HashMap<Hostname,HostStatistic>();
        JobHardRequestFilterAdapter filterSource = new JobHardRequestFilterAdapter();
        
        for (QueueInstanceSummary qi : tmpResult.getQueueInstanceSummary()) {
            Hostname hn = getHostnameOfQueue(qi);
            if (hn != null) {
                HostStatistic hostStat = ret.get(hn);
                if(hostStat == null) {
                    hostStat = new HostStatistic();
                    ret.put(hn,hostStat);
                }
                // We count only the free slots of active queue instances
                String state = qi.getState();
                if (state == null || state.length() == 0) {
                    hostStat.incrActiveQueueInstanceCount(1);
                    // The getFreeSlots returns the number of total slots of the queue instance
                    // and not the number of free slots.
                    hostStat.incrFreeSlotCount(Math.max(qi.getFreeSlots() - qi.getUsedSlots() - qi.getReservedSlots(),0));
                }
                for (JobSummary job : qi.getJobList()) {
                    filterSource.setFilterObject(job);
                    if (jobFilter == null || jobFilter.matches(filterSource)) {
                        hostStat.incrRunningJobsCount(1);
                    }
                }
            }
        }
        
        return ret;
    }
    
    /**
     * The HostStatistic collects the following statistical values of a host from
     * the QstatResult:
     * 
     *  - total free slots on the host
     *  - number of running jobs on the host
     *  - the number of active queue instance count
     */
    public static class HostStatistic {
        private int freeSlotCount;
        private int runningJobsCount;
        private int activeQueueInstanceCount;

        /**
         * Get the number of free slots
         * @return
         */
        public int getFreeSlotCount() {
            return freeSlotCount;
        }

        private void incrFreeSlotCount(int freeSlotCount) {
            this.freeSlotCount += freeSlotCount;
        }

        /**
         * Get the number of running jobs on the host
         * @return the number of running jobs
         */
        public int getRunningJobsCount() {
            return runningJobsCount;
        }

        private void incrRunningJobsCount(int runningJobsCount) {
            this.runningJobsCount += runningJobsCount;
        }

        /**
         * Get the number of active queue instances of the host.
         * 
         * @return the number of active queues instances
         */
        public int getActiveQueueInstanceCount() {
            return activeQueueInstanceCount;
        }

        private void incrActiveQueueInstanceCount(int activeQueueInstanceCount) {
            this.activeQueueInstanceCount += activeQueueInstanceCount;
        }
    }
    
    /**
     * This information about running jobs is in the qstat result not
     * stored in a single list. Each queue instance has it's own running
     * job list.
     *
     * The returned iterator iterates over all running jobs of all queue instance.
     *
     * @return the iterator
     * @throws com.sun.grid.grm.service.slo.InvalidatedSLOContextException if the SLOContext has been invalidated
     */
    public Iterator<JobSummary> getRunningJobIterator() throws InvalidatedSLOContextException {
        return new RunningJobIterator(getResult());
    }

    /**
     * Get the list of pending jobs.
     *
     * @return list of pending jobs
     * @throws com.sun.grid.grm.service.slo.InvalidatedSLOContextException if the SLOContext has been invalidated
     */
    public List<JobSummary> getPendingJobs() throws InvalidatedSLOContextException {
        QueueInstanceSummaryResult tmpRes = getResult();
        if (tmpRes != null) {
            return tmpRes.getPendingJobs();
        } else {
            return Collections.<JobSummary>emptyList();
        }
    }

    private class RunningJobIterator implements Iterator<JobSummary> {

        private final Iterator<QueueInstanceSummary> queueIterator;
        private Iterator<JobSummary> jobIterator;

        public RunningJobIterator(QueueInstanceSummaryResult qir) {
            if (qir == null) {
                jobIterator = null;
                queueIterator = null;
            } else {
                jobIterator = null;
                queueIterator = qir.getQueueInstanceSummary().iterator();
            }
        }

        public boolean hasNext() {
            if (queueIterator == null) {
                return false;
            }

            if (jobIterator != null && jobIterator.hasNext()) {
                return true;
            }
            jobIterator = null;
            while (jobIterator == null && queueIterator.hasNext()) {
                QueueInstanceSummary qis = queueIterator.next();
                List<JobSummary> jobList = qis.getJobList();
                if (!jobList.isEmpty()) {
                    jobIterator = jobList.iterator();
                }
            }
            return jobIterator != null;
        }

        public JobSummary next() {
            if (hasNext()) {
                return jobIterator.next();
            } else {
                throw new NoSuchElementException();
            }
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
