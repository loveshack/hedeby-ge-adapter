/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.impl.AbstractResourceFactory;
import com.sun.grid.jgdi.configuration.ExecHost;
import java.util.Map;

/**
 * ResourceFactory for a <tt>GEServiceAdapterImpl</tt>.
 */
public class GEResourceFactory extends AbstractResourceFactory<GEResourceAdapter> {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    private final GEServiceAdapterImpl service;

    /**
     * Create a new instance of the <tt>GEResourceFactory</tt>.
     * @param service  the <tt>GEServiceAdapterImpl</tt>
     */
    public GEResourceFactory(GEServiceAdapterImpl service) {
        this.service = service;
    }

    /**
     * Creates a GEResourceAdapter including the resource out of the internal
     * representation of a resource. For a <tt>GEServiceAdapter</tt> the internal
     * represetnation of a resource is an instanceof of <tt>ExecHost</tt>. The jgdi
     * layer of qmaster provides for each exec host such an instance.
     *
     * @param env  the execution env of the system (needed to create the new resource)
     * @param internalRepresentation  the internal representation of the resource
     * @return the new resource adapter
     * @throws com.sun.grid.grm.GrmException if the resource adapter could not be created
     */
    public GEResourceAdapter createResourceAdapter(ExecutionEnv env, Object internalRepresentation) throws GrmException {

        if (internalRepresentation instanceof ExecHost) {
            ExecHost eh = (ExecHost) internalRepresentation;

            Map<String, Object> props = HostResourceType.getInstance().createResourcePropertiesForName(eh.getName());
            Resource res = createResource(env, HostResourceType.getInstance(), props);

            HostImpl hi = new HostImpl(res);
            GEResourceAdapter ret = new GEResourceAdapter(service, hi);
            return ret;
        } else {
            throw new GrmException("gerf.ur", BUNDLE, internalRepresentation);
        }
    }
}
