/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge.ui;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.gridengine.MappingConfig;
import com.sun.grid.grm.ui.AbstractSystemCommand;
import com.sun.grid.grm.ui.Result;
import com.sun.grid.grm.ui.impl.CommandResult;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 *
 */
public class GetMappingCommand extends AbstractSystemCommand<MappingConfig> {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.ui.messages";
    private final static long serialVersionUID = -2007112601L;
    
    private final String mappingName;

    /** Creates a new instance of GetMappingCommand
     * @param mappingName the name of the mapping
     */
    public GetMappingCommand(String mappingName) {
        if (mappingName == null) {
            throw new NullPointerException();
        }
        this.mappingName = mappingName;
    }

    public Result<MappingConfig> execute(ExecutionEnv env) throws GrmException {
        Context ctx = env.getContext();
        try {
            MappingConfig mapping = (MappingConfig) ctx.lookup(MappingUtil.getMappingPath(mappingName));

            return new CommandResult<MappingConfig>(mapping);
        } catch (NamingException ex) {
            throw new GrmException("GetMappingCommand.notFound", ex, BUNDLE, mappingName, ex.getLocalizedMessage());
        } finally {
            try {
                ctx.close();
            } catch (NamingException ex) {
                // Ignore
            }
        }
    }
}
