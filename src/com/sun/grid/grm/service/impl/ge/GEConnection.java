/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.EventListenerSupport;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.jgdi.JGDIFactory;
import com.sun.grid.jgdi.configuration.ClusterQueue;
import com.sun.grid.jgdi.configuration.ExecHost;
import com.sun.grid.jgdi.configuration.Hostgroup;
import com.sun.grid.jgdi.configuration.Job;
import com.sun.grid.jgdi.event.Event;
import com.sun.grid.jgdi.event.EventListener;
import com.sun.grid.jgdi.event.EventTypeEnum;
import com.sun.grid.jgdi.management.JGDIProxy;
import com.sun.grid.jgdi.management.mbeans.JGDIJMXMBean;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryOptions;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryResult;
import java.io.EOFException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A thread safe version of jgdi. It also allows to create a event client for
 * monitoring execd.
 *
 */
public class GEConnection {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";

    private static final Logger log = Logger.getLogger(GEConnection.class.getName(), BUNDLE);
    
    private final GEServiceConfigHelper config;
    
    private final Lock    lock = new ReentrantLock();
    
    private JGDIProxy     jgdiProxy;
    private JGDIJMXMBean  jgdi;
    
    private final EventListenerSupport<EventListener> evtSupport = EventListenerSupport.<EventListener>newSynchronInstance(EventListener.class);
    
    private final EventListener eventForwarder = new EventListener() {
        public void eventOccured(Event evt) {
            // Forward the event to the registered event listener
            evtSupport.getProxy().eventOccured(evt);
        }
    };
    
    /**
     * Creates a new instance of GEconnection
     * @param config  configuration of the GEService
     */
    public GEConnection(GEServiceConfigHelper config) {
        this.config = config;
    }
    
    
    /**
     * Extablish a connection to qmaster.
     *
     * @see com.sun.grid.jgdi.JGDIFactory#newInstance
     * @throws com.sun.grid.grm.GrmException on any error of the JGDI layer
     */
    public void connect() throws GrmException {
        log.entering(GEConnection.class.getName(), "connect");
        lock.lock();
        
        try {
            close();
            String master = config.getMasterHost().getHostname();
            int port = config.getJmxPort();
            Object credentials = config.getCredentials();
            
            config.setupSSLForJGDI();
            jgdiProxy = JGDIFactory.newJMXInstance(master, port, credentials);
            jgdi = jgdiProxy.getProxy();
            
            Set<EventTypeEnum> subscription = new HashSet<EventTypeEnum>(4);

            subscription.add(EventTypeEnum.ExecHostAdd);
            subscription.add(EventTypeEnum.ExecHostDel);
            subscription.add(EventTypeEnum.ExecHostMod);
            
            jgdiProxy.addEventListener(eventForwarder);
            
            jgdi.setSubscription(subscription);
            
            if(log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "gc.connected", new Object [] { master, port });
            }
        } catch(UndeclaredThrowableException ex) {
            jgdiProxy = null;
            jgdi = null;
            throw new GrmException("gc.ex.conn", ex.getUndeclaredThrowable(), BUNDLE, ex.getUndeclaredThrowable().getLocalizedMessage());
        } catch(SecurityException ex) {
            jgdiProxy = null;
            jgdi = null;
            throw new GrmException("gc.ex.perm", ex, BUNDLE);
        } catch(Exception ex) {
            jgdiProxy = null;
            jgdi = null;
            throw new GrmException("gc.ex.conn", ex, BUNDLE, ex.getLocalizedMessage());
        } finally {
            lock.unlock();
        }
            
        log.exiting(GEConnection.class.getName(), "connect");
    }
    
    
    private JGDIJMXMBean getConnection() throws GrmException {
        log.entering(GEConnection.class.getName(), "getConnection");
        if(!isConnected()) {
            throw new GrmException("gc.ex.notconnected", BUNDLE);
        }
        log.exiting(GEConnection.class.getName(), "getConnection", jgdi);
        return jgdi;
    }
    
    
    
    /**
     * Gets the connection status.
     * Is thread safe
     *
     * @return true, if a connection to qmaster exists
     */
    public boolean isConnected() {
        log.entering(GEConnection.class.getName(), "isConnected");
        boolean ret;
        
        lock.lock();
        try {
            ret = (jgdiProxy != null && jgdiProxy.isConnected());            
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(), "isConnected", ret);
        return ret;
    }
    
    /**
     * Performs a real connection test.
     * 
     * @return <code>true</code> if qmaster is reachable else <code>false</code>
     */
    public boolean testConnection() {
        lock.lock();
        try {
            getConnection().getCurrentJGDIVersion();
            return true;
        } catch(Exception ex) {
            close();
            return false;
        } finally {
            lock.unlock();
        }
    }
    
    private void handleError(Exception ex) throws GrmException {
        Throwable realError = ex;
        if(ex instanceof UndeclaredThrowableException) {
            realError = ex.getCause();
        }
        // Test the connection
        testConnection();
        if(realError instanceof EOFException) {
            throw new GrmException("gc.ex.notconnected", BUNDLE);
        } else {
            throw new GrmException("gc.ex.jgdi", realError, BUNDLE, realError.getLocalizedMessage());
        }
    }
    
    /**
     * Execute a qstat.
     * 
     * @return the ret of qstat
     * @see com.sun.grid.jgdi.JGDI#getQueueInstanceSummary
     * @param options options for the qstat
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     */
    public QueueInstanceSummaryResult getQueueInstanceSummary(QueueInstanceSummaryOptions options)  throws GrmException {
        log.entering(GEConnection.class.getName(), "getQueueInstanceSummary");
        QueueInstanceSummaryResult ret = null;
        
        lock.lock();
        try {
            ret = getConnection().getQueueInstanceSummary(options);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        
        log.entering(GEConnection.class.getName(), "getQueueInstanceSummary");
        return ret;
    }
    
    /**
     * Closes the connection to qmaster.
     *
     * @see com.sun.grid.jgdi.JGDI#close
     */
    public void close() {
        log.entering(GEConnection.class.getName(), "close");
        lock.lock();
        try {
            if (jgdiProxy != null) {
                jgdiProxy.close();
                jgdiProxy = null;
                config.resetSSLForJGDI();
                log.fine("ge.closed");
            }
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(), "close");
    }
    
    /**
     * Kill all execd daemons of.
     *
     * @param terminateJobs if <code>true</code> all jobs on the execd 
     *                      will be terminated.
     * 
     * @see com.sun.grid.jgdi.JGDI#killAllExecds
     * @throws com.sun.grid.grm.GrmException on any error on the JGDI layer
     */
    public void killAllExecds(boolean terminateJobs) throws GrmException {
        log.entering(GEConnection.class.getName(),"killExecd", terminateJobs);
        lock.lock();
        try {
            getConnection().killAllExecds(terminateJobs);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(), "killExecd");
    }
    
    /**
     * Removes a host from a specified hostgroup.
     *
     * @param hostgroup - hostgroup name (has to start with an "@"
     * @param hostName - the hostname, which should be removed
     * @return true, if the host was removed
     * @throws com.sun.grid.grm.GrmException on any error on the JGDI layer
     */
    public boolean removeHostfromHostgroup(String hostgroup, Hostname hostName) throws GrmException {
        boolean isRemoved = false;
        lock.lock();
        try {
            JGDIJMXMBean conn = getConnection();
            Hostgroup hGroup = conn.getHostgroup(hostgroup);
            if (hGroup != null) {
                isRemoved = hGroup.removeHost(hostName.getHostname());
                if (isRemoved) {
                    conn.updateHostgroup(hGroup);
                }
            }
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        return isRemoved;
    }
    
    /**
     * Send the scheduler the shutdown event.
     *
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#killScheduler
     */
    public void killScheduler() throws GrmException {
        log.entering(GEConnection.class.getName(),"killScheduler");
        lock.lock();
        try {
            getConnection().killScheduler();
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"killScheduler");
    }
    
    /**
     * Kills the master. Before that, it closes the
     * event client connection.
     *
     * Is thread safe
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#killMaster
     */
    public void killMaster() throws GrmException {
        log.entering(GEConnection.class.getName(),"killMaster");
        
        lock.lock();
        try {
            getConnection().killMaster();
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"killMaster");
    }
    
    
    /**
     * Get the list of execd daemons.
     *
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @return list of exec hosts
     * @see com.sun.grid.jgdi.JGDI#getExecHostList
     *
     */
    @SuppressWarnings(value="unchecked")
    public java.util.List<ExecHost> getExecHostList() throws GrmException {
        log.entering(GEConnection.class.getName(),"getExecHostList");
        List ret = null;
        lock.lock();
        try {
            ret = getConnection().getExecHostList();
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"getExecHostList", ret);
        return (java.util.List<ExecHost>)ret;
    }

    /**
     * Get a job object
     * @param jobId the job id
     * @return the job object
     * @throws com.sun.grid.grm.GrmException
     */
    public Job getJob(int jobId) throws GrmException {
        log.entering(GEConnection.class.getName(),"getJob", jobId);
        Job ret = null;
        lock.lock();
        try {
            ret = getConnection().getJob(jobId);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"getJob", ret);
        return ret;
    }
    
    /**
     * Get a execd damone.
     * Is thread safe
     * @param  hostname  hostname of the exec daeomon
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#getExecHost
     * @return the exec host object
     */
    public ExecHost getExecHost(String hostname) throws GrmException {
        log.entering(GEConnection.class.getName(),"getExecHost", hostname);
        
        ExecHost ret = null;
        lock.lock();
        try {
            ret = getConnection().getExecHost(hostname);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"getExecHost", ret);
        return ret;
    }
    
    /**
     * Get the admin host list.
     * Is thread safe
     * @return the admin host list
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#getAdminHostList
     */
    public java.util.List getAdminHostList() throws GrmException {
        log.entering(GEConnection.class.getName(),"getAdminHostList");
        List ret = null;
        lock.lock();
        try {
            ret = getConnection().getAdminHostList();
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"getAdminHostList", ret);
        return ret;
    }
    
    /**
     * Add an host to the admin host list
     * Is thread safe
     * @param hostname  name of the host
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#addAdminHost
     */
    public void addAdminHost(String hostname) throws GrmException {
        log.entering(GEConnection.class.getName(),"addAdminHost", hostname);
        lock.lock();
        try {
            getConnection().addAdminHost(hostname);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"addAdminHost");
    }
    
    /**
     * Remove a host from the admin host list.
     * @param hostname  name of the host
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#deleteAdminHost
     */
    public void deleteAdminHost(String hostname) throws GrmException {
        log.entering(GEConnection.class.getName(),"deleteAdminHost", hostname);
        lock.lock();
        try {
            getConnection().deleteAdminHost(hostname);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"deleteAdminHost");
    }
    
    /**
     * Delete a list of jobs
     * @param jobIds list of job ids
     * @throws com.sun.grid.grm.GrmException on any communiation error in the jgdi layer
     */
    public void deleteJobs(List<Integer> jobIds) throws GrmException {
        log.entering(GEConnection.class.getName(),"deleteJobs", jobIds);
        lock.lock();
        try {
            for(Integer jobId: jobIds) {
                getConnection().deleteJob(jobId);
            }
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"deleteAdminHost");
        
    }
    
    /**
     * Get the configuration object of a clusteer queue
     * @param queueName name of the queue
     * @see com.sun.grid.jgdi.JGDI#getClusterQueue
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @return the cluster queue configuration object or <code>null</code>
     *         if the queue does not exist
     */
    public ClusterQueue getClusterQueue(String queueName) throws GrmException {
        ClusterQueue ret = null;
        log.entering(GEConnection.class.getName(),"getClusterQueue", queueName);
        lock.lock();
        try {
            ret = getConnection().getClusterQueue(queueName);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"getClusterQueue", ret);
        return ret;
    }
    
    
    /**
     * Update the configuration of a cluster queue
     * Is thread safe
     * @param cQueue  the new configuration of the cluster queue
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#updateClusterQueue
     */
    public void updateClusterQueue(ClusterQueue cQueue) throws GrmException {
        log.entering(GEConnection.class.getName(),"updateClusterQueue", cQueue);
        lock.lock();
        try {
            getConnection().updateClusterQueue(cQueue);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"updateClusterQueue");
    }
    
    /**
     * Get the cluster queue list.
     * @return the cluster queue list
     * @see com.sun.grid.jgdi.JGDI#getClusterQueueList
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     */
    @SuppressWarnings("unchecked")
    public java.util.List<ClusterQueue> getClusterQueueList() throws GrmException {
        java.util.List<ClusterQueue> ret = null;
        log.entering(GEConnection.class.getName(),"getClusterQueueList");
        lock.lock();
        try {
            ret = (java.util.List<ClusterQueue>)getConnection().getClusterQueueList();
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
        log.exiting(GEConnection.class.getName(),"getClusterQueueList", ret);
        return ret;
    }
    
    /**
     * disable a list of queues.
     *
     * @param  qiList  array of queue or queue instance names
     * @param  forced  if set to <code>true</code> running jobs will
     *                 be terminated
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#disableQueues
     */
    public void disableQueues(String qiList[], boolean forced) throws GrmException {
        lock.lock();
        try {
           getConnection().disableQueues(qiList, forced);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
    }
    
    /**
     * Reschedule jobs
     * @param jobList  list with the job ids
     * @param forced   forced mode
     * @throws com.sun.grid.grm.GrmException on any communiation error in the jgdi layer
     */
    public void rescheduleJobs(String jobList[], boolean forced) throws GrmException {
        lock.lock();
        try {
           getConnection().rescheduleJobs(jobList, forced);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }        
    }
    
    /**
     * suspend a list of queues.
     *
     * @param  qiList  array of queue or queue instance names
     * @param  forced  if set to <code>true</code> running jobs will
     *                 be suspended
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#disableQueues
     */
    public void suspendQueues(String qiList[], boolean forced) throws GrmException {
        lock.lock();
        try {
           getConnection().suspendQueues(qiList, forced);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
    }
    
    /**
     * suspend a list of queues.
     *
     * @param  jobList  array of job ids
     * @param  forced  if set to <code>true</code> running jobs will
     *                 be suspended
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#disableQueues
     */
    public void suspendJobs(String jobList[], boolean forced) throws GrmException {
        lock.lock();
        try {
           getConnection().suspendJobs(jobList, forced);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
    }
    
    
    
    /**
     * unsuspend a list of queues.
     *
     * @param  qiList  array of queue or queue instance names
     * @param  forced  if set to <code>true</code> running jobs will
     *                 be unsuspended
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#disableQueues
     */
    public void unsuspendQueues(String qiList[], boolean forced) throws GrmException {
        lock.lock();
        try {
           getConnection().unsuspendQueues(qiList, forced);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
    }
    
    /**
     * enable a list of queues.
     *
     * @param  qiList  array of queue or queue instance names
     * @param forced the forced flag
     * @throws com.sun.grid.grm.GrmException  on any communiation error in the jgdi layer
     * @see com.sun.grid.jgdi.JGDI#enableQueues
     */
    public void enableQueues(String qiList[], boolean forced) throws GrmException {
        lock.lock();
        try {
           getConnection().enableQueues(qiList, forced);
        } catch(Exception ex) {
            handleError(ex);
        } finally {
            lock.unlock();
        }
    }
    
    /**
     *  Register a new JGDI event listener
     *
     *  @param  lis the event listener
     */
    public void addEventListener(EventListener lis) {
        evtSupport.addListener(lis);
    }
    
    /**
     *  Remove a JGDI event listener
     *  @param lis the event listener
     */
    public void removeEventListener(EventListener lis) {
        evtSupport.removeListener(lis);
    }
    

    
}
