/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.util.I18NManager;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  Sequence of installation steps
 */
public class InstallationSequence implements Runnable {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    private final static Logger log = Logger.getLogger(InstallationSequence.class.getName(), BUNDLE);
    private final List<InstallationStep> steps = new LinkedList<InstallationStep>();
    private InstallException error;
    private final String name;
    private InstallationStep currentStep;

    enum State {

        NONE, RUNNING, FINISHED
    }
    private boolean aborted;
    private State state = State.NONE;

    /**
     * Create a new sequence of installation steps
     * @param name name of the sequence (for logging)
     */
    public InstallationSequence(String name) {
        this.name = name;
    }

    /**
     * Abort the installation sequence and wait until it is finished
     */
    public void abort() {
        log.log(Level.FINE, "InstallationSequence.abort", name);
        InstallationStep abortStep = null;
        synchronized (this) {
            aborted = true;
            abortStep = currentStep;
        }
        if (abortStep != null) {
            abortStep.abort();
        }
        synchronized (this) {
            try {
                while (state == State.RUNNING) {
                    this.wait();
                }
            } catch (InterruptedException ex) {
            // Ignore
            }
        }
    }

    /**
     * Execute the installatin steps.
     *
     * If one step fails, all previous steps will be undid
     *
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if an installation has been failed
     */
    public final void execute() throws InstallException {

        ListIterator<InstallationStep> iter = steps.listIterator();
        try {
            synchronized (this) {
                if (aborted) {
                    throw new InstallAbortedException();
                }
                state = State.RUNNING;
            }
            try {
                while (iter.hasNext()) {
                    synchronized (this) {
                        if (aborted) {
                            throw new InstallAbortedException();
                        }
                        currentStep = iter.next();
                    }
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "InstallationSequence.execStep",
                                new Object[]{name, currentStep.getDescription()});
                    }
                    currentStep.execute();
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "InstallationSequence.stepFinished",
                                new Object[]{name, currentStep.getDescription()});
                    }
                }
            } catch (Exception ex) {
                if (ex instanceof InstallException) {
                    throw (InstallException) ex;
                } else {
                    throw new InstallException(ex, "InstallationSequence.unexpectedError",
                            currentStep.getDescription(), ex.getLocalizedMessage());
                }
            }
        } catch (InstallException ex) {
            log.log(Level.WARNING, 
                    I18NManager.formatMessage("InstallationSequence.warning", BUNDLE, name, currentStep.getDescription(), ex.getLocalizedMessage()),
                    ex);
            // The previous step is the failed step, skip it
            iter.previous();
            while (iter.hasPrevious()) {
                InstallationStep step = iter.previous();
                log.log(Level.FINE, "InstallationSequence.undoStep",
                        new Object[]{name, step.getDescription()});
                step.undo();
            }
            throw ex;
        } finally {
            synchronized (this) {
                state = State.FINISHED;
                notifyAll();
            }
        }
    }

    public void run() {
        try {
            execute();
        } catch (InstallException ex) {
            error = ex;
        }
    }

    /**
     *  Add an installation step
     * @param step the installation step
     */
    public void addStep(InstallationStep step) {
        steps.add(step);
    }

    /**
     * Get the error of the installation sequence
     * @return the error or <code>null</code> if no error has been occurred
     */
    public InstallException getError() {
        return error;
    }
}
