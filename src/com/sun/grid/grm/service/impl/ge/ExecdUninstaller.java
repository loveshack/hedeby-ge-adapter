/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.gridengine.JobSuspendMethod;
import com.sun.grid.grm.executor.NoExitValueException;
import com.sun.grid.grm.executor.Result;
import com.sun.grid.grm.service.impl.ge.ExecdInstallerBase.MakeAdminHostStep;
import com.sun.grid.grm.service.impl.ge.ExecdInstallerBase.RemoveAdminHostStep;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.jgdi.configuration.ClusterQueue;
import com.sun.grid.jgdi.configuration.Job;
import com.sun.grid.jgdi.configuration.QueueInstance;
import com.sun.grid.jgdi.monitoring.JobSummary;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummary;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryOptions;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryResult;
import com.sun.grid.jgdi.monitoring.filter.JobStateFilter;
import com.sun.grid.jgdi.monitoring.filter.QueueFilter;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

/**
 *  Installation sequence for uninstalling the exec daemon on a host
 */
public class ExecdUninstaller extends ExecdInstallerBase {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";

    private final boolean isForced;
    
    private final AtomicBoolean execdStopped = new AtomicBoolean();
    
    /**
     * Creates a new instance of ExecdUninstaller
     * @param service the gridengine service
     * @param host   the where the execd will be removed
     * @param isForced if set to <code>false</code> the uninstaller will wait
     *                 for the end of all jobs
     */
    public ExecdUninstaller(GEServiceAdapterImpl service, Host host, boolean isForced) {
        super(service, host, I18NManager.formatMessage("ExecdUninstaller.name", BUNDLE, host.getName()));
        this.isForced = isForced;
        addStep(new DisableHostStep());
        addStep(new WaitForJobsStep());
        addStep(new MakeAdminHostStep());
        addStep(new UninstallExecdStep());
        addStep(new RemoveAdminHostStep());
    }
    
    /**
     * get the execd stopped flag
     * @return the execd stopped flag
     * This method is thread safe
     */
    public boolean isExecdStopped() {
        return execdStopped.get();
    }
    
    /**
     * set the execd stopped flag
     * @param execdStopped the new value of the execd stopped flag
     * This method is thread safe
     */
    public void setExecdStopped(boolean execdStopped) {
        this.execdStopped.set(execdStopped);
    }
 
    class DisableHostStep extends InstallationStep {
        
        private String [] qiList;
        
        public DisableHostStep() {
            super(I18NManager.formatMessage("ExecdUninstaller.DisableHostStep.name", BUNDLE));
        }
        
        /**
         * Disables all queue instances on the host
         * @throws InstallException if jgdi reported an error
         */
        public void execute() throws InstallException {
            try {
                GEConnection jgdi = getService().getJgdi();
                
                List<QueueInstance> queueList = new LinkedList<QueueInstance>();
                Hostname hostname = getHost().getName();
                List cqList = jgdi.getClusterQueueList();
                if (cqList != null) {
                    for (Object obj : cqList) {
                        ClusterQueue cq = (ClusterQueue) obj;
                        for (Object qiObj : cq.getQinstancesList()) {
                            QueueInstance qi = (QueueInstance) qiObj;
                            if (hostname.equals(Hostname.getInstance(qi.getQhostname()))) {
                                queueList.add(qi);
                            }
                        }
                    }
                }
                String [] tmpQiList = new String[queueList.size()];

                for (int i = 0; i < tmpQiList.length; i++) {
                    tmpQiList[i] = queueList.get(i).getFullName();
                }
                
                jgdi.disableQueues(tmpQiList, false);
                this.qiList = tmpQiList;
                
            } catch (GrmException ex) {
                throw new InstallException(ex, "ExecdUninstaller.DisableHostStep.error.jgdi", 
                                           getHost().getName(), ex.getLocalizedMessage());
            }
        }
        
        public void undo() {
            if(qiList != null) {
                try {
                    getService().getJgdi().enableQueues(qiList, false);
                } catch(GrmException ex) {
                    log.log(Level.WARNING,
                            I18NManager.formatMessage("ExecdUninstaller.DisableHostStep.warning.undo", BUNDLE, getHost().getName(), ex.getLocalizedMessage()),
                            ex);
                }
            }
        }
    }
    
    
    class WaitForJobsStep extends InstallationStep {
        
        private boolean aborted;
        private final Lock lock = new ReentrantLock();
        private final Condition abortedCondition = lock.newCondition();
        
        public WaitForJobsStep() {
            super(I18NManager.formatMessage("ExecdUninstaller.WaitForJobsStep.name", BUNDLE));
        }

        @Override
        public void abort() {
            lock.lock();
            try {
                aborted = true;
                abortedCondition.signal();
            } finally {
                lock.unlock();
            }
        }
        
        /**
         * Wait for the end of all running jobs on the host.
         * 
         * <p>If a jobs suspend policy for the GESErvice is defined this method
         *    triggers the migration of the rescheduling of the jobs
         * 
         * @throws InstallAbortedException     if the installer has been interrupted while waiting for
         *                                     the end of the running jobs
         * @throws InstallNotPossibleException if the running jobs did not end with the configured timeout
         * @throws InstallException            if some unexpected error occur (jgdi error, configuraton problem)
         */
        public void execute() throws InstallException, InstallAbortedException, InstallNotPossibleException {
            try {
                GEServiceConfigHelper.SuspendJobPolicyWrapper policy = getService().getConfigHelper().getSuspendJobPolicy();
                
                long timeout = System.currentTimeMillis() + policy.getTimeout().getValueInMillis();

                GEConnection jgdi = getService().getJgdi();
                    
                QueueInstanceSummaryOptions options = new QueueInstanceSummaryOptions();

                options.setShowFullOutput(true);
                JobStateFilter.State states[] = new JobStateFilter.State[]{JobStateFilter.RUNNING};
                options.setJobStateFilter(new JobStateFilter(states));
                QueueFilter qFilter = new QueueFilter();
                qFilter.addQueue("*@" + getHost().getName().getHostname());
                options.setQueueFilter(qFilter);

                List<String> rescheduleJobList = new LinkedList<String>();

                List<String> suspendJobList = new LinkedList<String>();

                List<Integer> waitJobList = new LinkedList<Integer>();

                
                while (true) {
                    waitJobList.clear();
                    rescheduleJobList.clear();
                    suspendJobList.clear();

                    QueueInstanceSummaryResult result = jgdi.getQueueInstanceSummary(options);
                    
                    for (QueueInstanceSummary qis : result.getQueueInstanceSummary()) {
                        for (JobSummary js : qis.getJobList()) {
                            Job job = jgdi.getJob(js.getId());
                            if (policy.suspendJobsWithCheckpointing()) {
                                if (job.getCheckpointName() != null) {
                                    // Its not sure that the checkpointing guarantees that
                                    // the job can be migrated
                                    // This job can be migrated, it has an checkpoint env
                                    suspendJobList.add(Integer.toString(js.getId()));
                                    if(log.isLoggable(Level.FINE)) {
                                        log.log(Level.FINE, "ExecdUninstaller.WaitForJobsStep.sj.ckpt", 
                                                new Object [] { getHost().getName().toString(), js.getId()});
                                    }
                                    continue;
                                }
                            } else if (job.getCheckpointName() != null) {
                                if (log.isLoggable(Level.FINE)) {
                                    log.log(Level.FINE,"ExecdUninstaller.WaitForJobsStep.nsj.ckpt",
                                            new Object [] {getHost().getName().toString(), js.getId(), JobSuspendMethod.SUSPEND_JOBS_WITH_CHECKPOINT.value() });
                                }
                            }

                            // Check if the job in rerunnable
                            if (policy.rescheduleRestartableJobs()) {
                                if (job.getRestart() != 0) {
                                    // the job is restartable
                                    rescheduleJobList.add(Integer.toString(js.getId()));
                                    if(log.isLoggable(Level.FINE)) {
                                        log.log(Level.FINE, "ExecdUninstaller.WaitForJobsStep.rsj.jr", 
                                                new Object [] {getHost().getName().toString(), js.getId()});
                                    }
                                    continue;
                                } else if (log.isLoggable(Level.FINE)) {
                                    log.log(Level.FINE, "ExecdUninstaller.WaitForJobsStep.nrsj.nrsf",
                                            new Object [] {getHost().getName().toString(), js.getId(), JobSuspendMethod.RESCHEDULE_RESTARTABLE_JOBS.value() });
                                }
                            } else if (job.getRestart() != 0) {
                                if (log.isLoggable(Level.FINE)) {
                                    log.log(Level.FINE,"ExecdUninstaller.WaitForJobsStep.nrsj.jr",
                                            new Object [] {getHost().getName().toString(), js.getId(), JobSuspendMethod.RESCHEDULE_RESTARTABLE_JOBS.value() });
                                }
                            }
                            
                            if (policy.rescheduleJobsInRerunQueue()) {
                                int addIndex = qis.getName().indexOf('@');
                                String cqName = qis.getName().substring(0,addIndex);
                                String host = qis.getName().substring(addIndex+1);
                                
                                // May be the queue instance has the rerun flag set
                                ClusterQueue clusterQueue = jgdi.getClusterQueue(cqName);
                                if(clusterQueue == null) {
                                    log.log(Level.WARNING, "ExecdUninstaller.WaitForJobsStep.cqNotFound", cqName);
                                } else if (clusterQueue.getDefaultRerun()) {
                                    // the job can rerun
                                    rescheduleJobList.add(Integer.toString(js.getId()));
                                    if (log.isLoggable(Level.FINE)) {
                                        log.log(Level.FINE, "ExecdUninstaller.WaitForJobsStep.rsj.cqr", 
                                                new Object [] {getHost().getName().toString(), js.getId(), cqName });
                                    }
                                    continue;
                                } else if (clusterQueue.getRerun(host)) {
                                    // the job can rerun
                                    rescheduleJobList.add(Integer.toString(js.getId()));
                                    if (log.isLoggable(Level.FINE)) {
                                        log.log(Level.FINE, "ExecdUninstaller.WaitForJobsStep.rsj.cir", 
                                                new Object [] {getHost().getName().toString(), js.getId(), qis.getName() });
                                    }
                                    continue;
                                } else if (log.isLoggable(Level.FINE)) {
                                    log.log(Level.FINE, "ExecdUninstaller.WaitForJobsStep.nrsj.nrf", 
                                            new Object [] {getHost().getName().toString(), js.getId(), qis.getName(), JobSuspendMethod.RESCHEDULE_JOBS_IN_RERUN_QUEUE.value() } );
                                }
                            } else if (log.isLoggable(Level.FINE)) {
                                // for diagnostic purpose we log the reason why the jobs can not be rescheduled
                                int addIndex = qis.getName().indexOf('@');
                                String cqName = qis.getName().substring(0,addIndex);
                                String host = qis.getName().substring(addIndex+1);
                                
                                ClusterQueue clusterQueue = jgdi.getClusterQueue(cqName);
                                if(clusterQueue == null) {
                                    log.log(Level.WARNING, "ExecdUninstaller.WaitForJobsStep.cqNotFound", cqName);
                                } else if (clusterQueue.getDefaultRerun()) {
                                    if (log.isLoggable(Level.FINE)) {
                                        log.log(Level.FINE, "ExecdUninstaller.WaitForJobsStep.nrsj.cqr",
                                                new Object [] {getHost().getName().toString(), js.getId(), cqName, JobSuspendMethod.RESCHEDULE_JOBS_IN_RERUN_QUEUE.value() }); 
                                    }
                                } else if (clusterQueue.getRerun(host)) {
                                    if (log.isLoggable(Level.FINE)) {
                                        log.log(Level.FINE, "ExecdUninstaller.WaitForJobsStep.nrsj.cir",
                                                new Object [] {getHost().getName().toString(), js.getId(), qis.getName(), JobSuspendMethod.RESCHEDULE_JOBS_IN_RERUN_QUEUE.value() }); 
                                    }
                                }
                            }
                            if (log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE, "ExecdUninstaller.WaitForJobsStep.nrsj", 
                                        new Object [] {getHost().getName().toString(), js.getId() });
                            }
                            waitJobList.add(js.getId());
                        }
                    }

                    if (waitJobList.isEmpty() || isForced) {
                        break;
                    } else {
                        lock.lock();
                        try {
                            if (timeout == 0 || System.currentTimeMillis() > timeout) {
                                throw new InstallNotPossibleException("ExecdUninstaller.WaitForJobsStep.timeout");
                            }
                            if (!aborted) {
                                log.log(Level.INFO, "ExecdUninstaller.WaitForJobsStep.jobs", waitJobList.size());
                                abortedCondition.await(60, TimeUnit.SECONDS);
                            }
                            if (aborted) {
                                throw new InstallAbortedException();
                            }
                        } finally {
                            lock.unlock();
                        }
                    }
                }

                if (rescheduleJobList.size() > 0) {
                    if(log.isLoggable(Level.CONFIG)) {
                        log.log(Level.CONFIG, "ExecdUninstaller.WaitForJobsStep.rsj",
                                new Object [] { getHost().getName().getHostname(), rescheduleJobList.toString() });
                    }
                    jgdi.rescheduleJobs(rescheduleJobList.toArray(new String[rescheduleJobList.size()]), false);
                }
            
                if (suspendJobList.size() > 0) {
                    if(log.isLoggable(Level.CONFIG)) {
                        log.log(Level.CONFIG, "ExecdUninstaller.WaitForJobsStep.sj",
                                new Object [] { getHost().getName().getHostname(), suspendJobList.toString() });
                    }
                    jgdi.suspendJobs(suspendJobList.toArray(new String[suspendJobList.size()]), false);
                }
                
                if (waitJobList.size() > 0 && isForced) {
                    log.log(Level.WARNING, "ExecdUninstaller.WaitForJobsStep.dj",
                            new Object [] { getHost().getName().getHostname(), waitJobList.toString() });
                    jgdi.deleteJobs(waitJobList);
                }

            } catch (InstallException ex) {
                throw ex;
            } catch (GrmException ex) {
                throw new InstallException(ex, "ExecdUninstaller.WaitForJobsStep.error.jgdi",
                        getHost().getName(), ex.getLocalizedMessage());
            } catch (InterruptedException ex) {
                throw new InstallException("ExecdUninstaller.WaitForJobsStep.error.interrupt");
            }
        }
        
        public void undo() {
            // Nothing to undo
        }
    }
    
    class UninstallExecdStep extends InstallationStep {
        
        public UninstallExecdStep() {
            super(I18NManager.formatMessage("ExecdUninstaller.UninstallExecdStep.name", BUNDLE));
        }

        private void executeAndHandleResult(GEServiceConfigHelper.ShellCommandWithTarget cmdWithTarget) throws InstallNotPossibleException, InstallException {

            Result res = ExecdUninstaller.this.execute(cmdWithTarget);

            int exitValue = 0;
            try {
                exitValue = res.getExitValue();
            } catch(NoExitValueException ex) {
                // Should not happen, it would means the Executor did not store
                // the exit value into the result object
                throw new InstallException(ex, "ei.i.error", cmdWithTarget.getCmd().getScript().getRemoteFilename());
            }

            if(exitValue == 0) {
                log.log(Level.FINE,"ei.i.success", cmdWithTarget.getCmd().getScript().getRemoteFilename());
            } else {
                File logFile = saveResult(res);
                throw new InstallException("ei.i.failed", cmdWithTarget.getCmd().getScript().getRemoteFilename(), exitValue, logFile);
            }
        }

        /**
         * Runs the execd uninstll script.
         * 
         * @throws InstallException if the exit code of  execd uninstall was not 0 or
         *                         if executor reported any problem while executing the uninstall script
         * @throws InstallNotPossibleException if execd uninstall script could not be 
         *                         started on the executor
         */
        public void execute() throws InstallException, InstallNotPossibleException {
            Host host = getHost();
            List<GEServiceConfigHelper.ShellCommandWithTarget> cmdList = null;
            try {
                cmdList = getConfig().createExecdUninstallScript(host);
            } catch(GrmException ex) {
                throw new InstallException(ex, "ExecdUninstaller.UninstallExecdStep.error.conf", ex.getLocalizedMessage());
            }

            for(GEServiceConfigHelper.ShellCommandWithTarget cmd: cmdList) {
                this.executeAndHandleResult(cmd);
            }
        }
        
        private File saveResult(Result res) {
            return ExecdUninstaller.this.saveResult(this, res, "uninstall");
        }
        
        
        public void undo() {
            // TODO: What shall we do? reinstall the execd?
        }
    }
    
    
}
