/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.logging.Logger;

/**
 *  Factory class for host object.
 *  This class will be used in future for upgrading spool host objects.
 */
public class HostFactory {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    
    private final static Logger log = Logger.getLogger(HostFactory.class.getName(), BUNDLE);
    
    /**
     * Create a new host object for a resource.
     * @param resource the resource
     * @return the host object
     * @throws com.sun.grid.grm.resource.InvalidResourceException if the resource is not a host resource
     */
    public static Host newInstance(Resource resource) throws InvalidResourceException {
        log.entering(Host.class.getName(), "newInstance", resource);
        Host ret = new HostImpl(resource);
        log.exiting(Host.class.getName(), "newInstance", ret);
        return ret;
    }
    
    private static Host upgrade(Object obj) throws GrmException {
        log.entering(Host.class.getName(), "upgrade", obj);
        
        if(obj == null) {
            throw new GrmException("hf.nullObj");
        } 
        if(obj instanceof HostImpl) {
            log.exiting(Host.class.getName(), "upgrade", obj);
            return (Host)obj;
        }
        throw new GrmException("hf.unknownHostObj", BUNDLE, obj.getClass());
    }
    
    /**
     * Load an host object from an input stream and upgrade it
     * if necessary.
     *  
     * @param  in the input stream 
     * @return the host object
     * @throws java.io.IOException on any IO Error
     * @throws java.lang.ClassNotFoundException if the class of the deserialized obj can not be found
     * @throws com.sun.grid.grm.GrmException if the obj has been read, but the upgrade failed.
     */
    public static Host load(InputStream in) throws IOException, ClassNotFoundException, GrmException {
        log.entering(Host.class.getName(), "load");
        ObjectInputStream oin = new ObjectInputStream(in);
        Object obj = oin.readObject();
        Host ret = upgrade(obj);
        log.exiting(Host.class.getName(), "load", ret);
        return ret;
    }

}
