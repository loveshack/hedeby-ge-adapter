/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.config.gridengine.MaxPendingJobsSLOConfig;
import com.sun.grid.grm.service.impl.ge.slo.MaxPendingsJobsSLO;
import com.sun.grid.grm.service.slo.SLOFactory;

/**
 *   This class creates all SLO for a Grid Engine Service.
 *
 *   This <code>SLOFactory</code> has the default <code>SLOFactory<code> as parent.
 *
 *   @see com.sun.grid.grm.service.slo.SLOFactory#getDefault
 */
public class GEServiceSLOFactory extends SLOFactory {
    
    /** Creates a new instance of GEServiceSLOFactory */
    public GEServiceSLOFactory() {
        super(1, SLOFactory.getDefault());
        registerSLO(MaxPendingJobsSLOConfig.class, MaxPendingsJobsSLO.class);
    }
    
    private static GEServiceSLOFactory instance;
    
    /**
     * Get the singleton instanceof the the <code>GEServiceSLOFactory</code>.
     *
     * @return the singleton instanceof the the <code>GEServiceSLOFactory</code>
     */
    public synchronized static GEServiceSLOFactory getInstance() {
        if(instance == null) {
            instance = new GEServiceSLOFactory();
        }
        return instance;
    }
    
}
