/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.GrmRemoteException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.event.ComponentEventListener;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.ResourceType;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceSnapshot;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.event.ServiceEventListener;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.filter.Filter;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <code>ServiceContainer</code> for the </code>GEServiceAdapter</code>.
 *
 * <p>This class delegates all calls to a instance of GEServiceContainer.</p>
 * 
 * <p><b>Attention!!:</b> This class is not longer used by the SDM system. However existing
 *    installations may reference this class. For the sake of compatibility we keep it in
 *    the system.</p>
 * 
 * @deprecated Has been replaced by {@link GEServiceContainer}
 */
public class GEServiceDelegate implements Service {

    private final GEServiceContainer container;
    
    /**
     * Creates a new instance of GridengineServiceDelegate
     *
     * @param env  the execution env 
     * @param name name of the service
     * @throws com.sun.grid.grm.GrmException if the delegate can not be loaded
     */
    public GEServiceDelegate(ExecutionEnv env, String name) throws GrmException {
        container = new GEServiceContainer(env, name);
    }

    // -------------------------------------------------------------------------
    // Note:
    // The following method do not have jdocs, they all forwards the calls only
    // to the container
    // -------------------------------------------------------------------------
    public void startService() throws GrmException, GrmRemoteException {
        container.startService();
    }

    /**
     * Stop the service.
     * 
     * @param isFreeResources
     * @throws com.sun.grid.grm.GrmException
     * @throws com.sun.grid.grm.GrmRemoteException
     * @see GEServiceContainer#stopService(boolean) 
     */
    public void stopService(boolean isFreeResources) throws GrmException, GrmRemoteException {
        container.stopService(isFreeResources);
    }

    /**
     * Start the component
     * @throws com.sun.grid.grm.GrmException
     * @see GEServiceContainer#start() 
     */
    public void start() throws GrmException {
        container.start();
    }

    /**
     * Stop the component
     * @param isForced
     * @throws com.sun.grid.grm.GrmException
     * @see GEServiceContainer#stop(boolean) 
     */
    public void stop(boolean isForced) throws GrmException {
        container.stop(isForced);
    }

    /**
     * Reload the component.
     * @param isForced
     * @throws com.sun.grid.grm.GrmException
     * @see GEServiceContainer#reload(boolean) 
     */
    public void reload(boolean isForced) throws GrmException {
        container.reload(isForced);
    }

    /**
     * Get the state of the component.
     * 
     * @return the state of the component
     * @throws com.sun.grid.grm.GrmRemoteException
     * @see GEServiceContainer#getState() 
     */
    public ComponentState getState() throws GrmRemoteException {
        return container.getState();
    }

    /**
     * Add a component event listener
     * @param componentEventListener the component event listener
     * @throws com.sun.grid.grm.GrmRemoteException
     * @see GEServiceContainer#addComponentEventListener(ComponentEventListener) 
     */
    public void addComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
        container.addComponentEventListener(componentEventListener);
    }

    /**
     * Remove a component event listener
     * @param componentEventListener the component event listener
     * @throws com.sun.grid.grm.GrmRemoteException
     * @see GEServiceContainer#removeComponentEventListener(ComponentEventListener) 
     */
    public void removeComponentEventListener(ComponentEventListener componentEventListener) throws GrmRemoteException {
        container.removeComponentEventListener(componentEventListener);
    }

    /**
     * Get the name of the component
     * @return the name of the component
     * @throws com.sun.grid.grm.GrmRemoteException
     */
    public String getName() throws GrmRemoteException {
        return container.getName();
    }

    /**
     * Get the host name of the component.
     * @return the hostname of the component
     * @throws com.sun.grid.grm.GrmRemoteException
     */
    public Hostname getHostname() throws GrmRemoteException {
        return container.getHostname();
    }

    /**
     * Get the service state
     * @return the service state
     * @throws com.sun.grid.grm.GrmRemoteException
     */
    public ServiceState getServiceState() throws GrmRemoteException {
        return container.getServiceState();
    }

    public List<SLOState> getSLOStates() throws GrmRemoteException {
        return container.getSLOStates();
    }

    public List<SLOState> getSLOStates(Filter<Resource> resourceFilter) throws GrmRemoteException {
        return container.getSLOStates();
    }

    public void addServiceEventListener(ServiceEventListener eventListener) throws GrmRemoteException {
        container.addServiceEventListener(eventListener);
    }

    public void removeServiceEventListener(ServiceEventListener eventListener) throws GrmRemoteException {
        container.removeServiceEventListener(eventListener);
    }

    public List<Resource> getResources(Filter<Resource> filter) throws ServiceNotActiveException, GrmRemoteException {
        return container.getResources(filter);
    }

    public List<Resource> getResources() throws ServiceNotActiveException, GrmRemoteException {
        return container.getResources();
    }

    public Resource getResource(ResourceId resourceId) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
        return container.getResource(resourceId);
    }

    public void removeResource(ResourceId resourceId, ResourceRemovalDescriptor descr) throws UnknownResourceException, InvalidResourceException, ServiceNotActiveException, GrmRemoteException {
        container.removeResource(resourceId, descr);
    }

    public void resetResource(ResourceId resource) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException {
        container.resetResource(resource);
    }

    public void addResource(Resource resource) throws InvalidResourceException, ServiceNotActiveException, GrmRemoteException {
        container.addResource(resource);
    }
    
    /**
     * Add a new resource to the service
     * @param type        the resource type
     * @param properties the resource properties
     * @return
     * @throws com.sun.grid.grm.service.ServiceNotActiveException  if the service is not active
     * @throws com.sun.grid.grm.resource.InvalidResourceException  if the service did not accept the resource
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the resource properties are not valid
     * @throws com.sun.grid.grm.GrmRemoteException  communication error
     * @throws com.sun.grid.grm.GrmException  if the resource could not be created (reason unknown)
     */
    public Resource addNewResource(ResourceType type, Map<String, Object> properties) throws ServiceNotActiveException, InvalidResourceException, InvalidResourcePropertiesException, GrmRemoteException, GrmException {
        return container.addNewResource(type, properties);
    }
    
    
    /**
     * Method is part of the Service interface
     * dismisses the sloName and calls addResource(Resource)
     * GEServiceDelegate does not need the SLOname 
     * @param resource  the resource to add
     * @param sloName if addResource is consequence of an SLO request, otherwise null
     * @throws com.sun.grid.grm.resource.InvalidResourceException thrown, when the resource is not a HostResource
     * @throws com.sun.grid.grm.service.ServiceNotActiveException if the service is not running
     * @throws com.sun.grid.grm.GrmRemoteException on any remote error
     */
    public void addResource(Resource resource,String sloName) throws InvalidResourceException, ServiceNotActiveException, GrmRemoteException {
        container.addResource(resource);
    }

    public void modifyResource(ResourceId resource, ResourceChangeOperation operation) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
        container.modifyResource(resource, operation);
    }

    public void modifyResource(ResourceId resource, Collection<ResourceChangeOperation> operations) throws UnknownResourceException, ServiceNotActiveException, GrmRemoteException, InvalidResourcePropertiesException {
        container.modifyResource(resource, operations);
    }


    public ServiceSnapshot getSnapshot() throws GrmRemoteException, ServiceNotActiveException {
        return container.getSnapshot();
    }
}
