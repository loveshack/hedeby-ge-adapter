/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.adapter.InvalidResourceStateException;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.impl.ge.Host.HostState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.jgdi.configuration.ExecHost;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

/**
 * Generic interface for host objects.
 * 
 * <p>Host objects represents an execution host in a Grid Engine cluster</p>
 */
public interface Host extends Comparable<Host> {

    public enum HostState {
        /**
         * The host object has been created, no action has been executed on it
         */
        HOST_NONE,
        
        /**
         * The reset on the this host object is ongoing
         */
        HOST_RESETING,
        
        /**
         * The installation of an execd is in progress
         */
        HOST_INSTALLING,
        
        /**
         * The installation of the execd is finished
         */
        HOST_INSTALLED,
        
        /**
         * The startup of the execd is ongoing
         */
        HOST_STARTING,
        
        /**
         * The execd on this host is active. the last EXECD_MOD event contained
         * load values
         */
        HOST_RUNNING,
        
        /**
         * The uninstall process is in progress
         */
        HOST_UNINSTALLING,
        
        /**
         * The host been removed, it is not longer used be the GE service
         */
        HOST_REMOVED,
        
        /**
         * The execd is not running. It is possible that the installation failed
         * or the someone stopped the execd manually. May be it crashed
         */
        HOST_ERROR,
    }
    
    /**
     * Add a new host listener
     * @param lis the host listener
     */
    void addHostListener(HostListener lis);

    /**
     * removes this host from the @allhost group and
     * resets the slots setting in the all.q. These settings
     * are created after the install. There is currently no
     * flag to disable the creation of default configurations.
     *
     * @param jgdi connection to qmaster
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     */
    void cleanupDefault(GEConnection jgdi) throws GrmException;

    /**
     * Gets the current state of the host. This can be installing,
     * uninstalling, starting up and others.
     * thread save
     * @return the host state
     */
    HostState getHostState();

    /**
     * Gets the fully qualified host name (resolved)
     *
     * @return the <code>Hostname</code> object
     */
    Hostname getName();

    /**
     * Reports the number of running jobs on the host
     *
     * @return the number of jobs
     * @param jgdi connection to qmaster
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     */
    int getNumberOfJobsOnThisHost(GEConnection jgdi) throws GrmException;

    /**
     * The the resource object for this host.
     * @return a full resource object which could lack the ResourceId, this is a copy
     */
    Resource getResource();

    /**
     * Get the id of the resource
     * @return the resource id
     */
    ResourceId getResourceId();

    /**
     * Prepare this host for installation.
     * 
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if the preparations failed
     * @return the id of the transition
     */
    public long prepareForInstall() throws InstallException;
    
    /**
     * Install a execd daemon on the host
     *
     * @param  service the service of the host
     * @param  transitionId the transition id
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if the installation has been failed
     * @throws HostTransitionExpiredException if a new action has been started on the host
     * @return the host state after the installation
     */
    public Host.HostState install(GEServiceAdapterImpl service, long transitionId) throws InstallException, HostTransitionExpiredException;
    
    /**
     * Finalize the installation
     * 
     * @param transitionId the id of the transition
     */
    public void finalizeInstall(long transitionId);

    /**
     * Prepare the host for a reset
     * @return the id of the transition
     */
    public long prepareForReset();
    
    /**
     * Reset the host
     * @param service the owning service
     * @param  transitionId the transition id
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if the reinstall of the execd failed
     * @throws HostTransitionExpiredException if a new action has been started on the host
     * @return the host state of the reset
     */
    HostState reset(GEServiceAdapterImpl service, long transitionId) throws InstallException, HostTransitionExpiredException;
    
    /**
     * Finalize the reset of the host
     * 
     * @param transitionId the id of the transition
     */
    public void finalizeReset(long transitionId);
    
    /**
     * This host is an admin host
     * @param jgdi  connection to qmaster
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     * @return <code>true</code> if this host is an admin host
     */
    boolean isAdminHost(GEConnection jgdi) throws GrmException;

    /**
     * Checks whether an execd is running or not.
     * @return if true, than the execd is up and running
     */
    boolean isRunning();

    /**
     * Checks weather the host is static
     *
     * @return true, it is static
     */
    boolean isStatic();

    /**
     * Makes this host to an adminHost, if it is not yet one.
     *
     *
     * @return true - the Host was added as an AdminHost,
     * false - the host was already an AdminHost
     * @param jgdi  connection to qmaster
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     */
    boolean makeAdminHost(GEConnection jgdi) throws GrmException;

    /**
     * Modify the properties of this host
     * @param operations on host properties
     * @param service the owning service
     * @return The changes
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the new properties contains errors
     */
    Collection<ResourceChanged> modify(GEServiceAdapterImpl service, Collection<ResourceChangeOperation> operations) throws InvalidResourcePropertiesException;

    /**
     * Set the ambiguous flag for this host
     * @param ambiguous the ambiguous flag
     * @param annotation the annotation
     */
    void setAmbiguous(boolean ambiguous, String annotation);
    
    /**
     * Changes this host into a none admin host.
     *
     * @param jgdi  connection to qmaster
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     */
    void removeAdminHost(GEConnection jgdi) throws GrmException;

    /**
     * Remove a host listener
     * @param lis the host listener
     */
    void removeHostListener(HostListener lis);

    /**
     * Sets the host state and returns the previous state. Is thread
     * safe. Overrides any existing state. Is thread safe.
     *
     * @param newState the new state to set.
     * @param message the reason why the host state has changed
     * @return the previous state.
     */
    HostState setHostState(HostState newState, String message);

    /**
     * Make this host static/not static
     * @param isStatic the static flag
     */
    void setStatic(boolean isStatic);

    /**
     * Store the host
     * @param out  the output stream
     * @throws java.io.IOException on any IO error
     */
    void store(OutputStream out) throws IOException;

    
    /**
     * Prepare the host for uninstallation
     * @param descr the resource removal descriptor
     * @throws com.sun.grid.grm.service.impl.ge.InstallException
     * @throws InvalidResourceStateException if the resource is not in the state that it can be uninstalled
     * @return the id of the host transition
     */
    long prepareForUninstall(ResourceRemovalDescriptor descr) throws InstallException, InvalidResourceStateException;
    
    /**
     * Uninstall the execd daemon on this host.
     *
     * @param service the service of this host
     * @param force if <code>true</code> the uninstall will terminate running jobs
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if an unrecoverable
     * error during uninstall process occured
     * @throws HostTransitionExpiredException if a new action has been started on the host
     * @param  transitionId the transition id
     * @return the state of the host after the uninstallation
     */
    HostState uninstall(GEServiceAdapterImpl service, long transitionId, boolean force) throws InstallException, HostTransitionExpiredException;

    /**
     * finalize the uninstallation of the host
     * @param transitionId the id of the transition
     */
    public void finalizeUninstall(long transitionId);
    
    /**
     * Update the information about this host
     *
     * @param service  the server to which this host belongs to
     * @param eHost    the exec host object for this host
     * @throws com.sun.grid.grm.GrmException on any communication error with qmaster
     * @return <code>true</code> if the resource properties has changed
     */
    boolean update(GEServiceAdapterImpl service, ExecHost eHost) throws GrmException;

    /**
     * Update the usage of this host
     * @param usage the new usage of this host
     */
    void updateUsage(Usage usage);

}
