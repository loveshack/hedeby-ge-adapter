/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.resource.ResourceStoreException;
import com.sun.grid.grm.resource.impl.AbstractFileStore;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Stores Host objects into a file store.
 */
public class HostFileStore extends AbstractFileStore<Host, ResourceStoreException> {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    
    /**
     * Create a new instance of <code>HostFileStore</code>
     * @param spoolDir the spool directory
     */
    public HostFileStore(File spoolDir) {
        super(spoolDir);
    }

    /**
     * Read a Host object from a file
     * @param file  the file
     * @return the host object
     * @throws com.sun.grid.grm.service.impl.ge.ResourceStoreException if the host object could not be read
     */
    @Override
    protected final Host readObjectFromFile(File file) throws ResourceStoreException {
        try {
            FileInputStream fin = new FileInputStream(file);
            try {
                return HostFactory.load(fin);
            } finally {
                fin.close();
            }
        } catch (Exception ex) {
            throw new ResourceStoreException("hm.ex.load", ex, BUNDLE, file);
        }
    }

    /**
     * Write a host object into a file
     * @param host  the host object
     * @param file the file
     * @throws com.sun.grid.grm.service.impl.ge.ResourceStoreException if the host object could not be stored in the file
     */
    @Override
    protected final void writeObjectToFile(Host host, File file) throws ResourceStoreException {
        try {
            FileOutputStream fout = new FileOutputStream(file);
            try {
                host.store(fout);
            } finally {
                try {
                    fout.close();
                } catch (IOException ex) {
                }
            }
        } catch (IOException ex) {
            throw new ResourceStoreException("hm.ex.store", ex, BUNDLE, host, ex.getLocalizedMessage());
        }
    }

    /**
     * Get the filename of a host object
     * @param obj the host object
     * @return the filename
     */
    @Override
    protected String getFilenamePrefixForObject(Host obj) {
        return getFilenamePrefixForHost(obj);
    }
    
    /**
     * Get the filename for a host object
     * @param obj the host object
     * @return the filename
     */
    static String getFilenamePrefixForHost(Host obj) {
        return obj.getResourceId().getId();
    }
}
