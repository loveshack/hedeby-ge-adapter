/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.executor.NoExitValueException;
import com.sun.grid.grm.executor.Result;
import com.sun.grid.grm.service.impl.ge.ExecdInstallerBase.MakeAdminHostStep;
import com.sun.grid.grm.service.impl.ge.ExecdInstallerBase.RemoveAdminHostStep;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;

/**
 * InstallationSequence for installing the execd on a host
 */
public class ExecdInstaller extends ExecdInstallerBase {
    
    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    private final AtomicBoolean execdStarted = new AtomicBoolean();
    
    /**
     * Creates a new instance of ExecdInstaller.
     *
     * @param service the grid engine service
     * @param host  host where the execd should be installed
     */
    public ExecdInstaller(GEServiceAdapterImpl service, Host host) {
        super(service, host, I18NManager.formatMessage("ei.name", BUNDLE, host.getName()));
        
        addStep(new MakeAdminHostStep());
        addStep(new InstallExecdStep());
        addStep(new RemoveAdminHostStep());
    }
    
    /**
     * set the execd started flag
     * @param execdStarted the execd started flag
     */
    public void setExecdStarted(boolean execdStarted) {
        this.execdStarted.set(execdStarted);
    }
    
    /**
     * get the execd started flag
     * @return the execd started flag
     */
    public boolean isExecdStarted() {
        return this.execdStarted.get();
    }
    
    private class InstallExecdStep extends InstallationStep {
        
        public InstallExecdStep() {
            super(I18NManager.formatMessage("ei.i.name", BUNDLE));
        }

        private void executeAndHandleResult(GEServiceConfigHelper.ShellCommandWithTarget cmdWithTarget) throws InstallNotPossibleException, InstallException {

            Result res = ExecdInstaller.this.execute(cmdWithTarget);
            
            int exitValue = 0;
            try {
                exitValue = res.getExitValue();
            } catch(NoExitValueException ex) {
                // Should not happen, it would means the Executor did not store
                // the exit value into the result object
                throw new InstallException(ex, "ei.i.error", cmdWithTarget.getCmd().getScript().getRemoteFilename());
            }

            File logFile = null;
            switch(exitValue) {
                case 0: // execd successfully installed
                    log.log(Level.FINE, "ei.i.success", cmdWithTarget.getCmd().getScript().getRemoteFilename());
                    break;
                case 2:
                    // execd install script reported that installation is not possible
                    // host was not modified
                    // 2 - means that prerequisities for execution of script that are checked in script are not met
                    // no changes made on host
                    logFile = saveResult(res);
                    throw new InstallNotPossibleException("ei.i.failed", cmdWithTarget.getCmd().getScript().getRemoteFilename(), exitValue, logFile);
                default:
                    logFile = saveResult(res);
                    throw new InstallException("ei.i.failed", cmdWithTarget.getCmd().getScript().getRemoteFilename(), exitValue, logFile);
            }
        }

        
        /**
         *   Runs the execd install script
         * 
         *   @throws InstallNotPossibleException if the executor could not be contacted or
         *           if the execd install script exited with status 2
         *   @throws InstallException  if the exit status execd install script was not 0 or 2
         **/
        public void execute() throws InstallNotPossibleException, InstallException {

            Host host = getHost();
            List<GEServiceConfigHelper.ShellCommandWithTarget> cmdList = null;
            try {
                cmdList = getConfig().createExecdInstallScript(host);
            } catch(GrmException ex) {
                throw new InstallException(ex, "ei.i.ex.cs", ex.getLocalizedMessage());
            }

            boolean first = true;
            for(GEServiceConfigHelper.ShellCommandWithTarget cmd: cmdList) {
                try {
                    this.executeAndHandleResult(cmd);
                    first = false;
                } catch(InstallNotPossibleException ex) {
                    if(first) {
                        throw ex;
                    } else {
                        // At least one script has been already executed
                        // The resource is dirty
                        // => make out of  the InstallNotPossibleException an InstallException
                        throw new InstallException(ex, "ei.i.ex.np", host.getName(), ex.getLocalizedMessage());
                    }
                }
            }
        }
        
        private File saveResult(Result res) {
            return ExecdInstaller.this.saveResult(this, res, "install");
        }
        
        public void undo() {
            // We can not undo the changes of a user defined script
        }

    }
}
