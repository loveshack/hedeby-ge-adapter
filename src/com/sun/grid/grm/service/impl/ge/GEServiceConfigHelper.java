/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.ComponentState;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.common.SLOConfig;
import com.sun.grid.grm.config.common.TimeInterval;
import com.sun.grid.grm.config.gridengine.ConnectionConfig;
import com.sun.grid.grm.config.gridengine.ExecuteOn;
import com.sun.grid.grm.config.gridengine.GEServiceConfig;
import com.sun.grid.grm.config.gridengine.InstallConfig;
import com.sun.grid.grm.config.gridengine.InstallTemplate;
import com.sun.grid.grm.config.gridengine.JobSuspendMethod;
import com.sun.grid.grm.config.gridengine.JobSuspendPolicy;
import com.sun.grid.grm.config.gridengine.MappingConfig;
import com.sun.grid.grm.executor.Executor;
import com.sun.grid.grm.executor.RemoteFile;
import com.sun.grid.grm.executor.impl.ShellCommand;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.slo.SLO;
import com.sun.grid.grm.service.slo.SLOFactory;
import com.sun.grid.grm.resource.filter.ResourceVariableResolver;
import com.sun.grid.grm.util.FileUtil;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.TimeIntervalHelper;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterException;
import com.sun.grid.grm.util.filter.FilterHelper;
import com.sun.grid.grm.validate.ValidatorService;
import com.sun.grid.jgdi.JGDIException;
import com.sun.grid.jgdi.management.JGDIProxy;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 *  Helper class for managing the configuration if the Grid Engine Service
 *  Adapter
 */
public class GEServiceConfigHelper {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    private final static Logger log = Logger.getLogger(GEServiceConfigHelper.class.getName(), BUNDLE);
    
    private final static TimeIntervalHelper DEFAULT_RECONNECT_INTERVAL = new TimeIntervalHelper(60, TimeIntervalHelper.SECONDS);
    
    private final static TimeIntervalHelper DEFAULT_EXECD_STARTUP_TIMEOUT = new TimeIntervalHelper(60, TimeIntervalHelper.SECONDS);
    private final static TimeIntervalHelper DEFAULT_EXECD_SHUTDOWN_TIMEOUT = new TimeIntervalHelper(60, TimeIntervalHelper.SECONDS);
    
    /** Per default all possible job suspend methods are allowed */
    private final static List<JobSuspendMethod> DEFAULT_JOB_SUSPEND_METHODS = 
            Arrays.<JobSuspendMethod>asList(
                  JobSuspendMethod.RESCHEDULE_JOBS_IN_RERUN_QUEUE, 
                  JobSuspendMethod.RESCHEDULE_RESTARTABLE_JOBS, 
                  JobSuspendMethod.SUSPEND_JOBS_WITH_CHECKPOINT);
    
    private final static SuspendJobPolicyWrapper DEFAULT_SUSPEND_JOB_POLICY = new SuspendJobPolicyWrapper(
            new TimeIntervalHelper(2, TimeIntervalHelper.MINUTES), DEFAULT_JOB_SUSPEND_METHODS);
    
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final GEServiceAdapterImpl service;
    
    private GEServiceConfig config;
    private File root;
    private File commonDir;
    private File actQMasterFile;
    private File caTop;    
    private Hostname masterHost;
    private long lastUpdate;
    private MappingConfig mappingConfig;
    private List<SLO> slos = Collections.<SLO>emptyList();
    private Set<Hostname> staticHosts = Collections.<Hostname>emptySet();
    private TimeIntervalHelper sloUpdateInterval;
    private TimeIntervalHelper qmasterCheckInterval;
    private TimeIntervalHelper qmasterReconnectInterval;
    private TimeIntervalHelper execdStartupTimeout;
    private TimeIntervalHelper execdShutdownTimeout;
    
    private KeyStore keyStore;
    private List<InstallConfigWrapper> installConfig = Collections.<InstallConfigWrapper>emptyList();
    
    private SuspendJobPolicyWrapper suspendJobPolicy;
    
    /**
     * Create a new instance of GEServiceConfigHelper
     * @param aService the service
     */
    public GEServiceConfigHelper(GEServiceAdapterImpl aService) {
        service = aService;
    }
    
    private void check() {
        if(config == null) {
            throw new IllegalStateException(I18NManager.formatMessage("shc.ex.nc", BUNDLE));
        }
    }
    
    
    /**
     * Set a new configuration.
     * 
     * @param aConfig the configuration
     * @param aMappingConfig the complex mapping configuration
     * @throws com.sun.grid.grm.GrmException if the configuration is not valid
     */
    public void setConfig(GEServiceConfig aConfig, MappingConfig aMappingConfig) throws GrmException {
        
        ValidatorService.validate(service.getExecutionEnv(), aConfig);
        
        File newRoot = new File(aConfig.getConnection().getRoot());
        
        if (!newRoot.exists()) {
            throw new GrmException("shc.ex.root", BUNDLE, newRoot);
        }
        File newCommonDir = new File(newRoot, aConfig.getConnection().getCell() + File.separatorChar + "common");
        if (!newCommonDir.exists()) {
            throw new GrmException("shc.ex.commondir", BUNDLE, newCommonDir);
        }
        File newActQMasterFile = new File(newCommonDir, "act_qmaster");
        
        Set<Hostname> newStaticHosts = Collections.unmodifiableSet(createStaticHosts(aConfig));
        
        List<SLO> newSlos = Collections.unmodifiableList(createSLOs(aConfig));
        
        TimeIntervalHelper newSloUpdateInterval = new TimeIntervalHelper(aConfig.getSloUpdateInterval());
        
        TimeIntervalHelper newQmasterReconnectInterval = DEFAULT_RECONNECT_INTERVAL;
        if(aConfig.getQmasterReconnectInterval() != null) {
            newQmasterReconnectInterval = new TimeIntervalHelper(aConfig.getQmasterReconnectInterval());
        }
        
        TimeIntervalHelper newExecdStartupTimeout = DEFAULT_EXECD_STARTUP_TIMEOUT;
        if(aConfig.getExecdStartupTimeout() != null) {
            newExecdStartupTimeout = new TimeIntervalHelper(aConfig.getExecdStartupTimeout());
        }

        TimeIntervalHelper newExecdShutdownTimeout = DEFAULT_EXECD_SHUTDOWN_TIMEOUT;
        if(aConfig.getExecdShutdownTimeout() != null) {
            newExecdShutdownTimeout = new TimeIntervalHelper(aConfig.getExecdShutdownTimeout());
        }
        
        String username = aConfig.getConnection().getUsername();
        if (username == null || username.length() == 0) {
            throw new GrmException("shc.ex.noUsername", BUNDLE);
        }

        File newCaTop = new File(newCommonDir, "sgeCA");
        
        KeyStore newKeyStore = null;
        char[] pw = null;
        String keyStorePath = aConfig.getConnection().getKeystore();
        if(keyStorePath == null) {
            String password = aConfig.getConnection().getPassword();
            if(password == null) {
                throw new GrmException("shc.ex.pwOrks", BUNDLE);
            }
            
        } else {
            if (aConfig.getConnection().isSetPassword()) {
                pw = aConfig.getConnection().getPassword().toCharArray();
            } else {
                pw = new char[0];
            }
            newKeyStore = createKeyStore(keyStorePath, username, pw);
        }
        
        List<InstallConfigWrapper> newInstallConfig = createInstallConfig(aConfig);
        
        
        SuspendJobPolicyWrapper newSuspendJobPolicy = DEFAULT_SUSPEND_JOB_POLICY;
        if(aConfig.isSetJobSuspendPolicy()) {
            newSuspendJobPolicy = new SuspendJobPolicyWrapper(aConfig.getJobSuspendPolicy());
        }
        
        // Now we have a good chance that the new configuration is valid
        lock.writeLock().lock();
        try {
            keyStore = newKeyStore;
            caTop = newCaTop;
            config = aConfig;
            sloUpdateInterval = newSloUpdateInterval;
            qmasterReconnectInterval = newQmasterReconnectInterval;
            execdStartupTimeout = newExecdStartupTimeout;
            execdShutdownTimeout = newExecdShutdownTimeout;
            root = newRoot;
            commonDir = newCommonDir;
            actQMasterFile = newActQMasterFile;
            slos = newSlos;
            staticHosts = newStaticHosts;
            mappingConfig = aMappingConfig;
            keyStore = newKeyStore;
            installConfig = newInstallConfig;
            suspendJobPolicy = newSuspendJobPolicy;
            // We have to reset the master host
            // it will be read from act_qmaster file
            masterHost = null;
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    private List<InstallConfigWrapper> createInstallConfig(GEServiceConfig aConfig) throws GrmException {
        List<InstallConfigWrapper> ret = Collections.<InstallConfigWrapper>emptyList();
        
        if (aConfig.isSetExecd()) {
            ret = new ArrayList<InstallConfigWrapper>(aConfig.getExecd().size());
            for(InstallConfig setting: aConfig.getExecd()) {
                try {
                    ret.add(new InstallConfigWrapper(setting));
                } catch(FilterException ex) {
                    throw new GrmException("shc.ex.LSF", ex, BUNDLE, setting.getFilter());
                }
            }
        }
        return ret;
    }
            
    private KeyStore createKeyStore(String path, String username, char [] pw) throws GrmException {

        try {
            File keyStoreFile = new File(path);
            FileInputStream fin = new FileInputStream(keyStoreFile);

            try {
                KeyStore ret = KeyStore.getInstance(KeyStore.getDefaultType());
                ret.load(fin, pw);

                if(!ret.isKeyEntry(username)) {
                    throw new GrmException("shc.ex.invalidKeyStore", BUNDLE, path, username);
                }
                return ret;
            } finally {
                fin.close();
            }
        } catch(Exception ex) {
            throw new GrmException("shc.ex.ks", ex, BUNDLE, path, ex.getLocalizedMessage());
        }
        
    }
    
    /**
     * Setup SSL for JGDI (only if a keystore is configured)
     */
    public void setupSSLForJGDI()  {
        lock.readLock().lock();
        try {
            if(keyStore != null) {
                char [] pw = null;
                if (config.getConnection().isSetPassword()) {
                    pw = config.getConnection().getPassword().toCharArray();
                } else {
                    pw = new char[0];
                }
                JGDIProxy.setupSSL(caTop, keyStore, pw);
            }
        } finally {
            lock.readLock().unlock();
        }        
    }
    
    /**
     *  Reset the SSL setup for JGDI
     */
    public void resetSSLForJGDI() {
        lock.readLock().lock();
        try {
            if(keyStore != null) {
                JGDIProxy.resetSSL(caTop);
            }
        } finally {
            lock.readLock().unlock();
        }        
    }
    
    /**
     *  Get the SLO list
     *
     *  @return the SLO list
     */
    public List<SLO> getSLOs() {
        lock.readLock().lock();
        try {
            return slos;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Create the SLOs for GEService from the GEServiceConfig
     * @param aConfig  the GEServiceConfig
     * @return  list of SLOs
     * @throws com.sun.grid.grm.GrmException if the configuration contains an invalid SLO definition
     */
    public static List<SLO> createSLOs(GEServiceConfig aConfig) throws GrmException {
        if(!aConfig.isSetSlos()) {
            return Collections.<SLO>emptyList();
        }
        List<SLOConfig> sloConfigs = aConfig.getSlos().getSlo();
        List<SLO> ret = new ArrayList<SLO>(sloConfigs.size());
        SLOFactory fac = GEServiceSLOFactory.getInstance();
        for(SLOConfig sloc: sloConfigs) {
            
            SLO slo = null;
            try {
                slo = fac.createSLO(sloc);
            } catch(GrmException ex) {
                throw new GrmException("shc.ex.slo", ex, BUNDLE, sloc.getName(), ex.getLocalizedMessage());
            }
            if(slo == null) {
                throw new GrmException("shc.ex.uslo", BUNDLE, 
                                       sloc.getName(), sloc.getClass().getSimpleName());
            } else {
                ret.add(slo);
            }
        }
        return ret;
    }
    
    /**
     * Get the configuration object 
     * @return the configuration object
     */
    public GEServiceConfig getConfig() {   
        lock.readLock().lock();
        try {
            check();
            return config;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    
    /**
     * Get interval between two SLO updates
     * @return interval between two SLO updates
     */
    public TimeIntervalHelper getSloUpdateInterval() {
        lock.readLock().lock();
        try {
            check();
            return sloUpdateInterval;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    
    /**
     * Get interval between two qmaster connection checks
     * @return check interval
     */
    public TimeIntervalHelper getQMasterCheckInterval() {
        lock.readLock().lock();
        try {
            check();
            return qmasterCheckInterval;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get interval between two reconnects to qmaster
     * @return reconnect interval
     */
    public TimeIntervalHelper getQMasterReconnectInterval() {
        lock.readLock().lock();
        try {
            check();
            return qmasterReconnectInterval;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Get the timeout value for execd startup
     * @return timeout value for execd startup
     */
    public TimeIntervalHelper getExecdStartupTimeout() {
        lock.readLock().lock();
        try {
            check();
            return execdStartupTimeout;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Get the timeout value for execd shutdown
     * @return timeout value for execd shutdown
     */
    public TimeIntervalHelper getExecdShutdownTimeout() {
        lock.readLock().lock();
        try {
            check();
            return execdShutdownTimeout;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    
    /**
     * Get the cell of the Grid Engine instance
     * @return the cell
     */
    public String getCell() {
        lock.readLock().lock();
        try {
            check();
            return config.getConnection().getCell();
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Get the port where qmaster is listening
     * @return the qmaster port
     */
    public int getMasterPort() {
        lock.readLock().lock();
        try {
            check();
            return config.getConnection().getMasterPort();
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get the port where mbean server of qmaster is listening
     * @return the port of the mbean server
     */
    public int getJmxPort() {
        lock.readLock().lock();
        try {
            check();
            return config.getConnection().getJmxPort();
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Gets the SGE_ROOT path
     *
     * @return the root path
     */
    public File getRoot() {
        lock.readLock().lock();
        try {
            check();
            return root;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     *  Get the JMX credential for the  connection to qmaster
     *  @return the credential for JMX authentication
     * @throws com.sun.grid.jgdi.JGDIException only of keystore authentication is used
     * 
     */
    public Object getCredentials() throws JGDIException {
        lock.readLock().lock();
        try {
            check();
            if(keyStore != null) {
                char [] pw = null;
                if(config.getConnection().isSetPassword()) {
                    pw = config.getConnection().getPassword().toCharArray();
                } else {
                    pw = new char[0];
                }
                return JGDIProxy.createCredentialsFromKeyStore(keyStore, config.getConnection().getUsername(), pw);
            } else {
                return new String [] {
                    config.getConnection().getUsername(),
                    config.getConnection().getPassword()
                };
            }
            
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * returns the $SGE_ROOT/$SGE_CELL/common dir.
     * @return  $SGE_ROOT/$SGE_CELL/common dir. 
     */
    public File getCommonDir() {        
        lock.readLock().lock();
        try {
            check();
            return commonDir;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    
    /**
     * Gets the master host
     * @return the master host
     * @throws java.io.IOException file access problems
     */
    public Hostname getMasterHost() throws IOException {
        // We have to ensure that only one thread read
        // the actQMasterFile => use writelock
        lock.writeLock().lock();
        try {
            check();
            if(masterHost == null || lastUpdate < actQMasterFile.lastModified()) {
                BufferedReader br = new BufferedReader(new FileReader(actQMasterFile));
                Hostname newMasterHost = Hostname.getInstance(br.readLine());
                br.close();
                if(log.isLoggable(Level.INFO) 
                   && masterHost != null && !newMasterHost.equals(masterHost)) {
                    log.log(Level.INFO, "shc.mc", new Object [] { service.getName(), masterHost, newMasterHost });
                }
                masterHost = newMasterHost;
                lastUpdate = actQMasterFile.lastModified();
            }
            return masterHost;
        } finally {
            lock.writeLock().unlock();
        }
    }
    
    
    /**
     * Get the complex mapping configuration
     * @return the mapping configuration
     */
    public MappingConfig getMapping() {
        lock.readLock().lock();
        try {
            return mappingConfig;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get the install config for a host
     * @param host  the host
     * @return the install config or <code>null</code> if no install config is defined
     */
    public InstallConfig getInstallConfig(Host host) {
        
        List<InstallConfigWrapper> sets = null;
        lock.readLock().lock();        
        try {
           sets = installConfig;
        } finally {
            lock.readLock().unlock();
        }
        if(sets.isEmpty()) {
            return null;
        }
        for(InstallConfigWrapper ret: sets) {
            if(ret.matches(host)) {
                return ret.getConfig();
            }
        }
        return null;
    }

    /**
     * Get the suspend job policy from the configuration
     * @return the suspend job policy
     */
    public SuspendJobPolicyWrapper getSuspendJobPolicy() {
        lock.readLock().lock();        
        try {
            return suspendJobPolicy;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    public static class SuspendJobPolicyWrapper {
        private final TimeIntervalHelper timeout;
        private final List<JobSuspendMethod> methods;
        
        public SuspendJobPolicyWrapper(JobSuspendPolicy policy) {
            this(new TimeIntervalHelper(policy.getTimeout()), policy.getSuspendMethods());
        }
        
        public SuspendJobPolicyWrapper(TimeIntervalHelper timeout, List<JobSuspendMethod> methods) {
            this.methods = methods;
            this.timeout = timeout;
        }
        
        /**
         * Get the timeout of the suspend job policy
         * @return the timeout of the suspend job policy
         */
        public TimeIntervalHelper getTimeout() {
            return timeout;
        }

        /**
         * Can GE Adapter suspend jobs with an checkpointing environment.
         * @return <code>true</code> if jobs with checkpointing environment can be
         *         suspended
         */
        public boolean suspendJobsWithCheckpointing() {
            return methods.contains(JobSuspendMethod.SUSPEND_JOBS_WITH_CHECKPOINT);
        }
        
        /**
         * Can GE Adapter reschedule jobs with the restart flag.
         * @return <code>true</code> if jobs with the restart flag can be rescheduled
         */
        public boolean rescheduleRestartableJobs() {
            return methods.contains(JobSuspendMethod.RESCHEDULE_RESTARTABLE_JOBS);
        }
        
        /**
         * Can GE Adapter reschedule jobs which are running in a queue or queue
         * instance with the rerun flag.
         * @return <code>true</code> if jobs running in a "rerun" queue can be rescheduled
         */
        public boolean rescheduleJobsInRerunQueue() {
            return methods.contains(JobSuspendMethod.RESCHEDULE_JOBS_IN_RERUN_QUEUE);
        }
                
    }
    private static class InstallConfigWrapper {
        
        private final Filter<Resource> filter;
        private final InstallConfig config;
        
        public InstallConfigWrapper(InstallConfig config) throws FilterException {
            if(config.isSetFilter()) {
                filter = FilterHelper.<Resource>parse(config.getFilter());
            } else {
                filter = null;
            }
            this.config = config;
        }
        
        public boolean matches(Host host) {
            if(filter != null) {
                ResourceVariableResolver resolver = new ResourceVariableResolver();
                resolver.setResource(host.getResource());
                return filter.matches(resolver);
            } else {
                return true;
            }
        }
        
        public InstallConfig getConfig() {
            return config;
        }
        
    }
    
    private enum InstallKey {
        SGE_CLUSTER_NAME,
        SGE_ROOT,
        CELL_NAME,
        EXEC_HOST,
        SGE_EXECD_PORT,
        SGE_QMASTER_PORT,
        EXEC_HOST_LIST,
        EXEC_HOST_LIST_RM,
        DEFAULT_DOMAIN,
        HOSTNAME_RESOLVING,
        ADD_TO_RC,
        REMOVE_RC,
        SGE_ENABLE_SMF,
        ADMIN_HOST_LIST,
        SUBMIT_HOST_LIST,
        EXECD_SPOOL_DIR_LOCAL,
    }
    
    private final static TimeIntervalHelper DEFAULT_MAX_RUNTIME = new TimeIntervalHelper(3, TimeIntervalHelper.MINUTES);
    
    private static class InstallParams {
        
        
        private String username = "root";
        private File   scriptTemplate;
        private File   confTemplate;
        private final Map<String,String> params = new HashMap<String,String>();
        private TimeIntervalHelper maxRuntime = DEFAULT_MAX_RUNTIME;
        private ExecuteOn target;
        
        public String getUsername() {
            return username;
        }
        
        private static String toParamKey(InstallKey key) {
            return toParamKey(key.toString());
        }

        private static String toParamKey(String str) {
            StringBuilder ret = new StringBuilder(str.length() + 6);
            ret.append("@@@");
            ret.append(str);
            ret.append("@@@");
            return ret.toString();
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public File getInstallScriptTemplate() {
            return scriptTemplate;
        }


        public File getInstallConfTemplate() {
            return confTemplate;
        }

        public void put(InstallKey key, String value) {
            params.put(toParamKey(key), value);
        }

        public void putResourceProperties(Resource resource) {
            for(Map.Entry<String,Object> entry: resource.getProperties().entrySet()) {
                params.put(toParamKey("RESOURCE:" + entry.getKey()), entry.getValue() == null ? "" : entry.getValue().toString());
            }
        }
        
        public String get(InstallKey key) {
            return params.get(toParamKey(key));
        }

        private File getAbsoluteFile(File file) {
            if (file.isAbsolute()) {
                return file;
            }
            return new File(PathUtil.getDefaultDistDir(), file.getPath());
        }
        
        public void setScriptTemplate(File scriptTemplate) {
            this.scriptTemplate = getAbsoluteFile(scriptTemplate);
        }

        public void setConfTemplate(File confTemplate) {
            if (confTemplate == null) {
                this.confTemplate = null;
            } else {
                this.confTemplate = getAbsoluteFile(confTemplate);
            }
        }
        
        public void setMaxRuntime(TimeInterval ti) {
            this.maxRuntime = new TimeIntervalHelper(ti);
        }
        
        public ShellCommand createShellCommand() throws GrmException {

            RemoteFile script = new RemoteFile(scriptTemplate.getName());
            try {
                PrintWriter pw = script.createPrintWriter();
                FileUtil.readAndReplace(scriptTemplate, pw, params);
                pw.close();
            } catch (IOException ex) {
                throw new GrmException("shc.ex.scriptTempl", ex, BUNDLE, scriptTemplate, ex.getLocalizedMessage());
            }

            ShellCommand cmd = new ShellCommand(script);
            cmd.setUser(username);
            cmd.setMaxRuntime(maxRuntime.getValueInMillis());

            if (confTemplate != null) {
                RemoteFile conf = new RemoteFile(confTemplate.getName());
                try {
                    PrintWriter pw = conf.createPrintWriter();
                    FileUtil.readAndReplace(confTemplate, pw, params);
                    pw.close();
                } catch (IOException ex) {
                    throw new GrmException("shc.ex.confTempl", ex, BUNDLE, confTemplate, ex.getLocalizedMessage());
                }

                cmd.addFile(conf);
            }
            return cmd;
        }

        /**
         * @return the target
         */
        public ExecuteOn getTarget() {
            return target;
        }

        /**
         * @param target the target to set
         */
        public void setTarget(ExecuteOn target) {
            this.target = target;
        }
    }

    private List<InstallParams> createInstallParamsList(Host host, boolean install) {
        List<InstallParams> ret = null;
        lock.readLock().lock();
        try {
            InstallConfig aInstallConfig =  getInstallConfig(host);
            if(aInstallConfig != null) {
                List<InstallTemplate> templList = install ? aInstallConfig.getInstallTemplate() : aInstallConfig.getUninstallTemplate();
                if (templList == null || templList.isEmpty()) {
                    // The config has an install configuration, but no template for this host
                    // => create a default config
                    ret = Collections.<InstallParams>singletonList(createInstallParams(host, install, aInstallConfig, null));
                } else {
                    // We have template for the host
                    ret = new ArrayList<InstallParams>(templList.size());
                    for(InstallTemplate templ: templList) {
                        ret.add(createInstallParams(host, install, aInstallConfig, templ));
                    }
                }
            } else {
                // The config has no install configuration for the host
                // => Create a default configuration
                ret = Collections.<InstallParams>singletonList(createInstallParams(host, install, aInstallConfig, null));
            }
        } finally {
            lock.readLock().unlock();
        }
        return ret;
    }
    
    private InstallParams createInstallParams(Host host, boolean install, InstallConfig aInstallConfig, InstallTemplate templ) {
    
        InstallParams ret = new InstallParams();
        
        // Intialize all parameter with an empty string
        for(InstallKey key: InstallKey.values()) {
            ret.put(key, "");
        }
        
        ret.put(InstallKey.HOSTNAME_RESOLVING, "None");
        ret.put(InstallKey.SGE_ENABLE_SMF, "false");
        ret.put(InstallKey.EXEC_HOST, host.getName().toString());
        ret.put(InstallKey.ADD_TO_RC, "false");
        ret.put(InstallKey.REMOVE_RC, "false");
        ret.put(InstallKey.SGE_CLUSTER_NAME, config.getConnection().getClusterName());
        ret.put(InstallKey.SGE_ROOT, getRoot().getAbsolutePath());
        ret.put(InstallKey.CELL_NAME, getCell());
        ret.put(InstallKey.SGE_EXECD_PORT, Integer.toString(config.getConnection().getExecdPort()));
        ret.put(InstallKey.SGE_QMASTER_PORT, Integer.toString(config.getConnection().getMasterPort()));

        File templatePath = PathUtil.getTemplatePath(service.getExecutionEnv());
        File moduleTemplatePath = new File(templatePath, "ge-adapter");
        
        if(install) {
            ret.setConfTemplate(new File(moduleTemplatePath, "install_execd.conf"));
            ret.setScriptTemplate(new File(moduleTemplatePath, "install_execd.sh"));
        } else {
            ret.setConfTemplate(new File(moduleTemplatePath, "uninstall_execd.conf"));
            ret.setScriptTemplate(new File(moduleTemplatePath, "uninstall_execd.sh"));
        }
        
        if (aInstallConfig != null) {
            if (aInstallConfig.isSetDefaultDomain()) {
                ret.put(InstallKey.DEFAULT_DOMAIN, aInstallConfig.getDefaultDomain());
            }

            ret.put(InstallKey.HOSTNAME_RESOLVING, Boolean.toString(!aInstallConfig.isIgnoreFQDN()));

            if(install) {
                ret.put(InstallKey.ADD_TO_RC, Boolean.toString(aInstallConfig.isRcScript()));
                if (aInstallConfig.isSetSubmitHost()) {
                   ret.put(InstallKey.SUBMIT_HOST_LIST, host.getName().getHostname());
                }
            } else {
                ret.put(InstallKey.REMOVE_RC, Boolean.toString(aInstallConfig.isRcScript()));
            }

            if (aInstallConfig.isSetLocalSpoolDir()) {
                ret.put(InstallKey.EXECD_SPOOL_DIR_LOCAL, aInstallConfig.getLocalSpoolDir());
            }
            
            if (aInstallConfig.isSetAdminUsername()) {
                ret.setUsername(aInstallConfig.getAdminUsername());
            }

            if (templ != null) {
                if(templ.isSetMaxRuntime()) {
                    ret.setMaxRuntime(templ.getMaxRuntime());
                }
                if(templ.isSetScript()) {
                    ret.setScriptTemplate(new File(templ.getScript()));
                }

                if(templ.isNeedsConfigFile()) {
                    if(templ.isSetConf()) {
                        ret.setConfTemplate(new File(templ.getConf()));
                    }
                } else {
                    ret.setConfTemplate(null);
                }
            }
        }
        
        ret.put(InstallKey.EXEC_HOST_LIST, host.getName().getHostname());
        ret.put(InstallKey.EXEC_HOST_LIST_RM, host.getName().getHostname());
        ret.putResourceProperties(host.getResource());

        // Set the target into the result
        ret.setTarget(ExecuteOn.EXEC_HOST);
        if(aInstallConfig != null) {
            if (templ != null && ExecuteOn.QMASTER_HOST.equals(templ.getExecuteOn())) {
                ret.setTarget(ExecuteOn.QMASTER_HOST);
            }
        }
        
        
        return ret;
    }
    
    public static class ShellCommandWithTarget {

        private final ExecuteOn target;
        private final ShellCommand cmd;

        public ShellCommandWithTarget(ExecuteOn target, ShellCommand cmd) {
            this.target = target;
            this.cmd = cmd;
        }

        /**
         * @return the target
         */
        public ExecuteOn getTarget() {
            return target;
        }

        /**
         * @return the cmd
         */
        public ShellCommand getCmd() {
            return cmd;
        }

    }
    
    /**
     * Create the execd install script for host.
     * @param host the host
     * @return the list of execd install scripts
     * @throws com.sun.grid.grm.GrmException if the execd install script cound not be created 
     */
    public List<ShellCommandWithTarget> createExecdInstallScript(Host host) throws GrmException {
        List<InstallParams> params = createInstallParamsList(host, true);
        List<ShellCommandWithTarget> ret = new ArrayList<ShellCommandWithTarget>(params.size());
        for(InstallParams param: params) {
            ret.add(new ShellCommandWithTarget(param.getTarget(), param.createShellCommand()));
        }
        return ret;
    }
    
    /**
     * Create the execd uninstall script for a host
     * @param host the host
     * @return the list of execd uninstall scripts
     * @throws com.sun.grid.grm.GrmException if the execd uninstall script cound not be created 
     */
    public List<ShellCommandWithTarget> createExecdUninstallScript(Host host) throws GrmException {
        List<InstallParams> params = createInstallParamsList(host, false);
        List<ShellCommandWithTarget> ret = new ArrayList<ShellCommandWithTarget>(params.size());
        for(InstallParams param: params) {
            ret.add(new ShellCommandWithTarget(param.getTarget(), param.createShellCommand()));
        }
        return ret;
    }
    
    /**
     * Get the set of static hosts.
     * 
     * @return set of static hosts
     */
    public Set<Hostname> getStaticHosts() {
        lock.readLock().lock();
        try {
            return staticHosts;
        } finally {
            lock.readLock().unlock();
        }
    }
    
    /**
     * Find out if <code>hostname</code> is a static host.
     *
     * Host is static if
     *
     * <ul>
     *   <li>the hostname is defined as static host in the configuration of the GE service.</li>
     *   <li>it is the master host of the GE service</li>
     *   <li>the executeOn attribute of the execd installation or uninstallation config is
     *       set to EXEC_HOST and the executor on this host is not active.</li>
     * </ul>
     *
     * @param env       the execution env
     * @param host      the host object
     * @return <code>true</code> if host is a static host
     */
    public boolean isStaticHost(ExecutionEnv env, Host host) {

        boolean ret = getStaticHosts().contains(host.getName());

        if (ret == false) {
            try {
                ret = host.getName().equals(getMasterHost());
            } catch (IOException ex) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, 
                            I18NManager.formatMessage("shc.isStaticHost.ex.masterHost", BUNDLE, host.getName().toString(), ex.getLocalizedMessage()),
                            ex);
                }
                ret = true;
            }
        }
        if (ret == false) {
            // Check that the install config defines that an executor is required
            // on the target system
            
            // Issue 582: If user did not define a install configuration we need an executor
            //            If we have a user-defined install configuration it depends on the
            //            value of the executeOn attribute of the (un)installTemplate.
            if (installationNeedsExecutorOnHost(host)) {
                
                // In all these cases we need an active executor on the targeted host
                try {
                    // Issue 582: Checking that the executor on the host exists
                    //            is not enough, we must prove that it is also started
                    Executor exe = ComponentService.getComponentByTypeAndHost(env, Executor.class, host.getName());
                    if (exe == null) {
                        // We found no executor on the host, it is STATIC
                        ret = true;
                    } else {
                        // If executor is STARTED, then the host is NOT static
                        ret = exe.getState() != ComponentState.STARTED;
                    }
                } catch (GrmException ex) {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, 
                                I18NManager.formatMessage("shc.isStaticHost.ex.executor", BUNDLE, host.getName().toString(), ex.getLocalizedMessage()),
                                ex);
                    }
                    ret = true;
                }
            }
        }
        return ret;
    }

    private boolean installationNeedsExecutorOnHost(Host host) {
            InstallConfig conf = getInstallConfig(host);

            // Issue 582: If user did not define a install configuration we need an executor
            //            If we have a user-defined install configuration it depends on the
            //            value of the executeOn attribute of the (un)installTemplate.
            if (conf == null) {
                return true;
            }
            
            List<InstallTemplate> installTemplates = conf.getInstallTemplate();
            if (installTemplates.isEmpty()) {
                return true;
            }
            for(InstallTemplate templ: installTemplates) {
                if(!templ.isSetExecuteOn() || templ.getExecuteOn().equals(ExecuteOn.EXEC_HOST) ) {
                    return true;
                }
            }
            
            List<InstallTemplate> uninstallTemplates = conf.getUninstallTemplate();
            if (uninstallTemplates.isEmpty()) {
                return true;
            }
            for(InstallTemplate templ: uninstallTemplates) {
                if(!templ.isSetExecuteOn() || templ.getExecuteOn().equals(ExecuteOn.EXEC_HOST) ) {
                    return true;
                }
            }

            return false;
    }
    
    private static Set<Hostname> createStaticHosts(GEServiceConfig config) {
        HashSet<Hostname> ret = new HashSet<Hostname>();

        if(!config.isSetStaticHosts()) {
            return Collections.<Hostname>emptySet();
        }
        List<String> staticHosts = config.getStaticHosts().getInclude();
        for (String host : staticHosts) {
            ret.add(Hostname.getInstance(host));
        }
        
        return Collections.unmodifiableSet(ret);
    }
    
    /**
     *  Determine if a configuration addresses the same
     *  cluster as the current configuration.
     *
     *  @param aConfig  the configuration
     *  @return <code>true</code> if <code>aConfig</code> addresses the same cluster
     */
    public boolean isSameCluster(GEServiceConfig aConfig) {
        
        ConnectionConfig c1 = config.getConnection();
        ConnectionConfig c2 = aConfig.getConnection();
        
        return c1.getRoot().equals(c2.getRoot())
            && c1.getCell().equals(c2.getCell())
            && c1.getJmxPort() == c2.getJmxPort();
        
    }
    
}
