/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.util.I18NManager;
import java.io.Serializable;

/**
 * Execption of this class are thrown if the startup method of the host
 * wants to report an error
 */
public class StartupException extends GrmException implements Serializable {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    private static final long serialVersionUID = -2007091300L;
    
    /**
     * Creates a new instance of <code>StartupException</code> without detail message.
     */
    public StartupException() {
       super();
    }
    
    /**
     * Constructs an instance of <code>StartupException</code> with the specified
     * detail message.
     * @param msg the detail message.
     */
    public StartupException(String msg) {
        super(I18NManager.formatMessage(msg, BUNDLE));
    }
    
    /**
     * Constructs an instance of <code>StartupException</code> with the specified
     * internationalized detail message.
     * @param msg the detail message.
     * @param params the parameters to the message
     */
    public StartupException(String msg, Object... params) {
        super(I18NManager.formatMessage(msg, BUNDLE, params));
    }
    
    /**
     * Constructs an instance of <code>GrmException</code> with the specified
     * detail message and source.
     * @param msg the detail message.
     * @param cause the source Throwable
     */
    public StartupException(Throwable cause, String msg) {
        super(I18NManager.formatMessage(msg, BUNDLE), cause);
    }
    
    /**
     * Constructs an instance of <code>GrmException</code> with the specified
     * internationalized detail message.
     * @param msg the detail message.
     * @param cause the source Throwable
     * @param params the parameters to the message
     */    
    public StartupException(Throwable cause, String msg, Object... params) {
        super(I18NManager.formatMessage(msg, BUNDLE, params), cause);
    }
    
}
