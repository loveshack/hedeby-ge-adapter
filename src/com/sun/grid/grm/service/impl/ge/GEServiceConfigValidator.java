/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.config.gridengine.ConnectionConfig;
import com.sun.grid.grm.config.gridengine.GEServiceConfig;
import com.sun.grid.grm.config.gridengine.MappingConfig;
import com.sun.grid.grm.service.impl.ge.ui.GetMappingsCommand;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterHelper;
import com.sun.grid.grm.validate.GrmValidationException;
import com.sun.grid.grm.validate.Validation;
import com.sun.grid.grm.validate.Validator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Validation(type=GEServiceConfig.class)
public class GEServiceConfigValidator implements Validator<GEServiceConfig> {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";

    public void validate(ExecutionEnv env, GEServiceConfig obj, GEServiceConfig old) throws GrmValidationException {
        
        // Validate that the mapping for the config exists
        List<String> errors = new LinkedList<String>();
        
        ConnectionConfig cc = obj.getConnection();

        if (cc == null) {
            errors.add(I18NManager.formatMessage("GEServiceConfigValidator.noConnection", BUNDLE));
        } else {
            if (cc.getClusterName() == null || cc.getClusterName().length() == 0) {
                errors.add(I18NManager.formatMessage("GEServiceConfigValidator.missingAttr", BUNDLE, "clusterName", "connection"));
            }
            if (cc.getCell() == null || cc.getCell().length() == 0) {
                errors.add(I18NManager.formatMessage("GEServiceConfigValidator.missingAttr", BUNDLE, "cell", "connection"));
            }
            if (cc.getRoot() == null || cc.getRoot().length() == 0) {
                errors.add(I18NManager.formatMessage("GEServiceConfigValidator.missingAttr", BUNDLE, "root", "connection"));
            }
            if (!cc.isSetExecdPort()) {
                errors.add(I18NManager.formatMessage("GEServiceConfigValidator.missingAttr", BUNDLE, "execdPort", "connection"));
            }
            if (!cc.isSetJmxPort()) {
                errors.add(I18NManager.formatMessage("GEServiceConfigValidator.missingAttr", BUNDLE, "jmxPort", "connection"));
            }
            if (!cc.isSetMasterPort()) {
                errors.add(I18NManager.formatMessage("GEServiceConfigValidator.missingAttr", BUNDLE, "masterPort", "connection"));
            }
        }
        
        if(obj.getMapping() == null) {
            errors.add(I18NManager.formatMessage("GEServiceConfigValidator.nomapping", BUNDLE));
        } else {
            GetMappingsCommand cmd = new GetMappingsCommand();

            try {
                Filter<String> filter = FilterHelper.<String>parse("name = \"%s\"", obj.getMapping());
                cmd.setFilter(filter);

                Map<String, MappingConfig> res = env.getCommandService().execute(cmd).getReturnValue();

                if (!res.containsKey(obj.getMapping())) {
                    errors.add(I18NManager.formatMessage("GEServiceConfigValidator.um", BUNDLE, obj.getMapping()));
                }
            } catch (GrmException ex) {
                errors.add(I18NManager.formatMessage("GEServiceConfigValidator.mse", BUNDLE, obj.getMapping(), ex.getLocalizedMessage()));
            }
        }
        
        if (!obj.isSetSlos() || obj.getSlos().getSlo().isEmpty()) {
           errors.add(I18NManager.formatMessage("GEServiceConfigValidator.noSLOs", BUNDLE));
        }
        
        if (errors.size() > 0) {
            throw new GrmValidationException(errors);
        }
    }

}
