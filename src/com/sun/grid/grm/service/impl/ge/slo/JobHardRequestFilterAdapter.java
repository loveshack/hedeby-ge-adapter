/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge.slo;

import com.sun.grid.grm.util.filter.VariableResolver;
import com.sun.grid.jgdi.monitoring.HardRequestValue;
import com.sun.grid.jgdi.monitoring.JobSummary;

/**
 *  Filter adapter Jobs Hard requests
 */
public class JobHardRequestFilterAdapter implements VariableResolver<JobSummary> {
    
    private JobSummary job;
    
    
    /**
     *  Get a hard request of a job
     *  @param name the name of the hard request
     *  @return the value of the hard request or <code>null</code>
     *          if the jobs has not such a hard request
     */
    public Object getValue(String name) {
        if(job == null) {
            return null;
        }
        HardRequestValue req = job.getHardRequestValue(name);
        if(req != null) {
            return req.getValue();
        } else {
            return null;
        }
    }

    /**
     * The the job
     * @param aJob the job 
     */
    public void setFilterObject(JobSummary aJob) {
        job = aJob;
    }
    
}
