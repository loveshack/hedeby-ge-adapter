/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2008 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.ComponentNotActiveException;
import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.gridengine.GEServiceConfig;
import com.sun.grid.grm.impl.AbstractDynamicComponentAdapterFactory;
import com.sun.grid.grm.impl.ExecutorServiceFactory;
import com.sun.grid.grm.service.Service;
import com.sun.grid.grm.service.impl.AbstractServiceContainer;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
  * <code>ServiceContainer</code> for the </code>GEServiceAdapter</code>.
 *
 * <p>This class loads dynamically the <code>GEServiceAdapterImpl</code> class. This
 *   <code>ServiceAdapter</code> needs jgdi.jar and juti.jar in it's classpath, hence
 *   a <code>AbstractDynamicComponentAdapterFactory</code> is used as <code>ComponentAdapterFactory</code>.
 * </p>
 */
public class GEServiceContainer extends AbstractServiceContainer<GEServiceAdapter, ScheduledExecutorService, GEServiceConfig>
   implements Service {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    private final static Logger log = Logger.getLogger(GEServiceContainer.class.getName(), BUNDLE);
    
    /**
     * Creates a new instance of GridengineServiceDelegate
     *
     * @param env  the execution env 
     * @param name name of the service
     * @throws com.sun.grid.grm.GrmException if the delegate can not be loaded
     */
    public GEServiceContainer(ExecutionEnv env, String name) throws GrmException {
        super(env, name,
                new MyExecutorServiceFactory(),
                new MyServiceAdapterFactory());
    }
    
    private static class  MyExecutorServiceFactory implements ExecutorServiceFactory<GEServiceContainer, ScheduledExecutorService, GEServiceConfig> {

        public ScheduledExecutorService createExecutor(GEServiceContainer comp) throws ComponentNotActiveException {
            log.entering(MyExecutorServiceFactory.class.getName(), "createExecutor");
            // The thread which performs communication with qmaster needs
            // context class loader which has access to the jgdi classes
            // otherwise we get a ClassCastException when we try to cast
            // the object from JGDIProxy to our local objects
            
            ScheduledExecutorService ret = Executors.newScheduledThreadPool(3, 
                    new NamedThreadFactory(comp.getName(), comp.getAdapter().getClass().getClassLoader()));
            log.exiting(MyExecutorServiceFactory.class.getName(), "createExecutor");
            return ret;
        }

        /**
         * The type of the executor is <code>ExecutorService</code>
         * @return always <code>ExecutorService.class</code>
         */
        public Class<? extends ExecutorService> getType() {
            return ExecutorService.class;
        }
    }
    
    private static class NamedThreadFactory implements ThreadFactory {
        
        private final ClassLoader contextClassLoader;
        private final String threadName;
        private AtomicInteger instanceCount = new AtomicInteger();
        
        public NamedThreadFactory(String componentName, ClassLoader contextClassLoader) {
            this.threadName = String.format("%s", componentName);
            this.contextClassLoader = contextClassLoader;
        }
        
        public Thread newThread(Runnable r) {
            log.entering(NamedThreadFactory.class.getName(), "newThread");
            
            Thread ret = new Thread(r, String.format("%s[%d]",threadName, instanceCount.getAndIncrement()));
            ret.setContextClassLoader(contextClassLoader);
            
            log.entering(NamedThreadFactory.class.getName(), "newThread", ret);
            return ret;
        }
    }

    /**
     * This class constructs out of a <code>GEServiceConfig</code> the URLs for
     * a classloader which has access to sdm-ge-adapter-impl.jar, $SGE_ROOT/lib/jgdi.jar
     * and $SGE_ROOT/lib/juti.jar.
     */
    private static class MyServiceAdapterFactory extends AbstractDynamicComponentAdapterFactory<GEServiceContainer, GEServiceAdapter, ScheduledExecutorService, GEServiceConfig> {

        @Override
        protected URL[] getClasspath(GEServiceContainer component, GEServiceConfig config) throws GrmException {
            log.entering(MyServiceAdapterFactory.class.getName(), "getClasspath");
            File sgeRoot = new File(config.getConnection().getRoot());
            File jgdiJar = new File(sgeRoot, "lib" + File.separatorChar + "jgdi.jar");

            if (!jgdiJar.exists()) {
                throw new GrmException("GEServiceContainer.missingJar", BUNDLE, jgdiJar, component.getName());
            }

            File jutiJar = new File(sgeRoot, "lib" + File.separatorChar + "juti.jar");
            if (!jutiJar.exists()) {
                throw new GrmException("GEServiceContainer.missingJar", BUNDLE, jutiJar, component.getName());
            }

            File serviceImplJar = new File(PathUtil.getDistLibPath(component.getExecutionEnv()), "sdm-ge-adapter-impl.jar");
            if (!serviceImplJar.exists()) {
                throw new GrmException("GEServiceContainer.missingJar", BUNDLE, serviceImplJar, component.getName());
            }

            URL urls[];
            try {

                urls = new URL[]{
                            serviceImplJar.toURI().toURL(),
                            jgdiJar.toURI().toURL(),
                            jutiJar.toURI().toURL()
                        };
            } catch (MalformedURLException ex) {
                throw new IllegalStateException("File.toURL throwed exception", ex);
            }
            if (log.isLoggable(Level.FINER)) {
                log.exiting(MyServiceAdapterFactory.class.getName(), "getClasspath", Arrays.toString(urls));
            }
            return urls;
        }

        /**
         * The classname is always GEServiceAdapterImpl, the class is in the same package.
         * @param component the component
         * @param config    the configuration of the component
         * @return the classname of the ServiceAdapter
         */
        @Override
        protected String getAdapterClassname(GEServiceContainer component, GEServiceConfig config) {
        return getClass().getPackage().getName() + ".GEServiceAdapterImpl";
        }
    }
}
