/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.impl.ge.ui;

import com.sun.grid.grm.config.common.ResourceMapping;
import com.sun.grid.grm.config.common.ResourceProperty;
import com.sun.grid.grm.config.common.ResourcePropertySet;
import com.sun.grid.grm.config.gridengine.MappingConfig;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.ResourcePropertyType;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Utility class from complex mapping
 */
public class MappingUtil {
    
    
    /** path in CS to the complex mappings */
    public final static String MAPPING_PATH = "module.ge-service-adapter.complex_mapping";
    
    /** name of the default complex mapping */
    public final static String DEFAULT_MAPPING_NAME = "default";

    private final static String MAPPING_PATH_PATTERN = "module.ge-service-adapter.complex_mapping.%s";
    
    /**
     * Get the path for a mapping is CS
     * @param mappingName name of the mapping
     * @return the path of the mapping
     */
    public static String getMappingPath(String mappingName) {
        return String.format(MAPPING_PATH_PATTERN, mappingName);
    }
    
    /**
     * Create the default mapping
     * @return the default mapping
     */
    public static MappingConfig createDefaultMapping() {
        
        MappingConfig ret = new MappingConfig();

        addArch(ret.getResource(), "sol-sparc64", "sparcv9", "Solaris", null);
        addArch(ret.getResource(), "sol-amd64", "amd64", "Solaris", null);
        addArch(ret.getResource(), "sol-x86", "x86", "Solaris", null);
        addArch(ret.getResource(), "lx24-x86", "x86", "Linux", "24");
        addArch(ret.getResource(), "lx24-amd64", "amd64", "Linux", "24");
        addArch(ret.getResource(), "lx26-x86", "x86", "Linux", "26");
        addArch(ret.getResource(), "lx-x86", "x86", "Linux", null);
        addArch(ret.getResource(), "lx26-amd64", "amd64", "Linux", "26");
        addArch(ret.getResource(), "lx-amd64", "amd64", "Linux", null);
        addArch(ret.getResource(), "win-x86", "x86", "Windows", null);
        addArch(ret.getResource(), "darwin-x86", "x86", "Darwin", null);

        
        ret.getResource().add(createValueResourceMapping("num_proc", HostResourceType.HW_CPU_COUNT));
        ret.getResource().add(createValueResourceMapping("mem_total", HostResourceType.HW_TOTAL_MEMORY));
        ret.getResource().add(createValueResourceMapping("virtual_total", HostResourceType.TOTAL_VIRTUAL_MEMORY));
        ret.getResource().add(createValueResourceMapping("swap_total", HostResourceType.TOTAL_SWAP));
        
        return ret;
    }
    
    private static ResourceMapping createValueResourceMapping(String consumable, ResourcePropertyType property) {
        ResourceMapping ret = new ResourceMapping();
        
        ResourceProperty src = new ResourceProperty();
        ResourcePropertySet target = new ResourcePropertySet();
        ret.setSource(src);
        ret.setTarget(target);
        
        src.setName(consumable);
        src.setValue("*");
        addProp(target, property.getName(), VALUE_PATTERN);
        return ret;
    }
    

    private static void addArch(List<ResourceMapping> list, String arch, String hw, String os, String version) {

        ResourceMapping conf = new ResourceMapping();

        ResourceProperty src = new ResourceProperty();
        ResourcePropertySet target = new ResourcePropertySet();
        conf.setSource(src);
        conf.setTarget(target);

        src.setName("arch");
        src.setValue(arch);
        addProp(target, HostResourceType.HW_CPU_ARCH.getName(), hw);
        addProp(target, HostResourceType.OS_NAME.getName(), os);
        if (version != null) {
            addProp(target, HostResourceType.OS_RELEASE.getName(), version);
        }
        list.add(conf);
    }
    

    private static void addProp(ResourcePropertySet props, String name, String value) {
        ResourceProperty p = new ResourceProperty();
        p.setName(name);
        p.setValue(value);
        props.getProperty().add(p);
    }
    
    private final static String VALUE_PATTERN = "$VALUE";
    private final static String WILD_CARD = "*";
    /**
     * Map a complex to resource properties.
     * 
     * @param mapping    the complex to resource mapping
     * @param name       name of the complex
     * @param value      value of the complex
     * @return map with the resource properties.
     */
    public static Map<String,Object> mapComplex(MappingConfig mapping, String name, String value) {
        
        if (mapping != null) {
            for (ResourceMapping rmap : mapping.getResource()) {
                ResourceProperty src = rmap.getSource();
                if (!name.equals(src.getName())) {
                    continue;
                }
                
                if (src.getValue().equals(WILD_CARD) || src.getValue().equals(value)) {
                    List<ResourceProperty> target = rmap.getTarget().getProperty();
                    Map<String,Object> ret = new HashMap<String,Object>(target.size());
                    for (ResourceProperty prop : target) {
                        if(VALUE_PATTERN.equals(prop.getValue())) {
                            ret.put(prop.getName(), value);   
                        } else {
                            ret.put(prop.getName(), prop.getValue());
                        }
                    }
                    return ret;
                }
            }
        }
        return Collections.<String,Object>emptyMap();
    }

    
    
}
