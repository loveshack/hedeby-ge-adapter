/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.impl.ge.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.table.AbstractDefaultTableModel;
import com.sun.grid.grm.cli.table.AbstractSortedTableCliCommand;
import com.sun.grid.grm.cli.table.Table;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.common.ResourceMapping;
import com.sun.grid.grm.config.common.ResourceProperty;
import com.sun.grid.grm.config.gridengine.MappingConfig;
import com.sun.grid.grm.config.gridengine.NamedMappingConfig;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.service.impl.ge.ui.GetMappingsCommand;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 */
@CliCommandDescriptor(
    name = "ShowMappingsCliCommand",
    hasShortcut = true,
    category = CliCategory.MONITORING,
    bundle="com.sun.grid.grm.service.impl.ge.cli.messages",
    requiredPrivileges= { UserPrivilege.READ_BOOTSTRAP_CONFIG, UserPrivilege.ADMINISTRATOR })
public class ShowMappingsCliCommand extends AbstractSortedTableCliCommand {

    @CliOptionDescriptor(
    name = "ShowMappingsCliCommand.name",
            numberOfOperands = 1,
            hasShortcut=false,
            required = false
            )
    private String name;
    
    @CliOptionDescriptor(
    name = "ShowMappingsCliCommand.output",
            numberOfOperands = 1,
            hasShortcut=true,
            required = false
            )
    private String outputStr;
    
    enum Output {
        NAMES, XML, TABLE
    }

    public void execute(AbstractCli cli) throws GrmException {
        
        Output output;
        if(outputStr == null) {
            output = Output.TABLE;
        } else if(getBundle().getString("ShowMappingsCliCommand.names").equals(outputStr)) {
            output = Output.NAMES;
        } else if(getBundle().getString("ShowMappingsCliCommand.xml").equals(outputStr)) {
            output = Output.XML;
        } else if(getBundle().getString("ShowMappingsCliCommand.table").equals(outputStr)) {
            output = Output.TABLE;
        } else {
            throw new GrmException("ShowMappingsCliCommand.ex.unknownOutput", getBundleName(), outputStr);
        }
            
        GetMappingsCommand cmd = new GetMappingsCommand();
        
        if(name != null)  {
            Filter<String> filter = FilterHelper.<String>parse("name = \"%s\"", name);
            cmd.setFilter(filter);
        }
        
        Map<String,MappingConfig> mappings = cli.getExecutionEnv().getCommandService().execute(cmd).getReturnValue();
        
        List<String> names = new ArrayList<String>(mappings.keySet());
        Collections.sort(names);
        
        switch(output) {
            case XML:
                NamedMappingConfig nmc = new NamedMappingConfig();
                for (String mn : names) {
                    nmc.setName(mn);
                    nmc.setMapping(mappings.get(mn));
                    XMLUtil.write(nmc, cli.out().getPrintWriter());
                }
                break;
            case TABLE:
                MyTableModel mtm = new MyTableModel(mappings);
                Table table = createTable(mtm, cli);
                table.print(cli);
                break;
            case NAMES:
                for(String mn: names) {
                    cli.out().printlnDirectly(mn);
                }
                break;
            default:
                throw new IllegalStateException("unknown output " + output);
        }
    }
    
    private class MyTableModel extends AbstractDefaultTableModel {

        private final static long serialVersionUID = -2008012301L;
        
        private final List<Row> rows = new LinkedList<Row>();
        
        public MyTableModel(Map<String,MappingConfig> mappings) {
            
            for(Map.Entry<String,MappingConfig> entry: mappings.entrySet()) {
                
                for(ResourceMapping mapping: entry.getValue().getResource()) {
                    for(ResourceProperty prop: mapping.getTarget().getProperty()) {
                        Row row = new Row(entry.getKey(), mapping.getSource(), prop);
                        rows.add(row);
                    }
                }
            }
        }
        
        public int getRowCount() {
            return rows.size();
        }

        public int getColumnCount() {
            return 5;
        }

        private String getValue(ResourceProperty prop) {
            if(prop == null || prop.getValue() == null) {
                return "";
            } else {
                return prop.getValue();
            }
        }

        private String getName(ResourceProperty prop) {
            if(prop == null || prop.getName() == null) {
                return "";
            } else {
                return prop.getName();
            }
        }
        
        public Object getValueAt(int rowIndex, int columnIndex) {
            Row row = rows.get(rowIndex);
            switch(columnIndex) {
                case 0:  return row.getName();
                case 1:  return getName(row.getSource());
                case 2:  return getValue(row.getSource());
                case 3:  return getName(row.getTarget());
                case 4:  return getValue(row.getTarget());
                default: throw new IllegalArgumentException("Unknown column " + columnIndex);
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch(columnIndex) {
                case 0: return getBundle().getString("ShowMappingsCliCommand.col.name");
                case 1: return getBundle().getString("ShowMappingsCliCommand.col.complex");
                case 2: return getBundle().getString("ShowMappingsCliCommand.col.cvalue");
                case 3: return getBundle().getString("ShowMappingsCliCommand.col.resource");
                case 4: return getBundle().getString("ShowMappingsCliCommand.col.rvalue");
                default: throw new IllegalArgumentException("Unknown column " + columnIndex);
            }
        }
    }
    
    private static class Row {
        
        private final String name;
        private final ResourceProperty source;
        private final ResourceProperty target;

        public Row(String name, ResourceProperty source, ResourceProperty target) {
            this.name = name;
            this.source = source;
            this.target = target;
        }
        public String getName() {
            return name;
        }
        
        public ResourceProperty getSource() {
            return source;
        }

        public ResourceProperty getTarget() {
            return target;
        }

    }
    
}



