/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.gridengine.GEServiceConfig;
import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.InvalidResourceIdException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.ResourceFactory;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.UnknownResourceException;
import com.sun.grid.grm.resource.adapter.RAOperation;
import com.sun.grid.grm.resource.adapter.RAOperationException;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.resource.adapter.ResourceAdapterStore;
import com.sun.grid.grm.resource.adapter.ResourceTransition;
import com.sun.grid.grm.service.ServiceNotActiveException;
import com.sun.grid.grm.service.ServiceState;
import com.sun.grid.grm.service.impl.AbstractServiceAdapter;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManagerSetup;
import com.sun.grid.grm.service.impl.ge.slo.MaxPendingsJobsSLO;
import com.sun.grid.grm.service.impl.ge.ui.GetMappingCommand;
import com.sun.grid.grm.util.TimeIntervalHelper;
import com.sun.grid.jgdi.event.Event;
import java.util.List;
import java.util.logging.Logger;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.service.slo.SLOContextFactory;
import com.sun.grid.grm.service.slo.impl.RunnableSLOManager;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import com.sun.grid.grm.util.InvalidStateTransistionException;
import com.sun.grid.jgdi.configuration.ExecHost;
import com.sun.grid.jgdi.configuration.ExecHostImpl;
import com.sun.grid.jgdi.event.ConnectionClosedEvent;
import com.sun.grid.jgdi.event.ConnectionFailedEvent;
import com.sun.grid.jgdi.event.EventListener;
import com.sun.grid.jgdi.event.ExecHostAddEvent;
import com.sun.grid.jgdi.event.ExecHostDelEvent;
import com.sun.grid.jgdi.event.ExecHostModEvent;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryOptions;
import com.sun.grid.jgdi.monitoring.QueueInstanceSummaryResult;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

/**
 * This is the implementation of a Service Adapter for the Grid Engine 6.2.
 * 
 * 
 * <h2>Thread saveness</h2>
 * 
 * <p>This class itself is not thread safe, the caller must take care about
 *    synchronization.</p>
 */
public class GEServiceAdapterImpl extends AbstractServiceAdapter<ScheduledExecutorService,GEServiceConfig, Hostname, GEResourceAdapter>
        implements GEServiceAdapter {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    private final static Logger log = Logger.getLogger(GEServiceAdapterImpl.class.getName(), BUNDLE);
    
    private final MySLOContextFactory sloContextFactory;
    
    private final GEConnection jgdi;
    private final GEServiceConfigHelper config = new GEServiceConfigHelper(this);
    
    private final ConnectionObserver connectionObserver = new ConnectionObserver();

    private final MyJGDIEventListener jgdiListener = new MyJGDIEventListener();
    
    /**
     * Creates a new instance of GEServiceAdapterImpl.
     * @param component The owning component
     */
    public GEServiceAdapterImpl(GEServiceContainer component) {
        super(component);
        jgdi = new GEConnection(config);

        jgdi.addEventListener(connectionObserver);
        jgdi.addEventListener(jgdiListener);
        sloContextFactory = new MySLOContextFactory();
    }

    /**
     * Creates out of the GEServiceConfig the setup for the SLOManager
     * @param config the GEServiceConfig
     * @return the setup of the SLOManager
     * @throws com.sun.grid.grm.GrmException if the config is invalid (e.g contains an unsupported SLO)
     */
    @Override
    protected RunnableSLOManagerSetup createSLOManagerSetup(GEServiceConfig config) throws GrmException {
        
        TimeIntervalHelper ti = new TimeIntervalHelper(config.getSloUpdateInterval());
        
        return new RunnableSLOManagerSetup(ti.getValueInMillis(), GEServiceConfigHelper.createSLOs(config));
    }

    /**
     * Create the resource factory of the ge service
     * @return the resource factory
     */
    @Override
    protected ResourceFactory<GEResourceAdapter> createResourceFactory() {
        return new GEResourceFactory(this);
    }
    
    

    /**
     * Create the SLOManager
     * @return the SLOManager
     */
    @Override
    protected RunnableSLOManager createSLOManager() {
        log.entering(GEServiceAdapterImpl.class.getName(),"createSLOManager");
        RunnableSLOManager ret = new RunnableSLOManager(getName(), this);
        log.exiting(GEServiceAdapterImpl.class.getName(),"createSLOManager");
        return ret;
    }

    /**
     * The GEService uses GEResourceAdapterStore
     * @return a GEResourceAdapterStore
     */
    @Override
    protected ResourceAdapterStore<Hostname, GEResourceAdapter> createResourceAdapterStore(GEServiceConfig config) {
        log.entering(GEServiceAdapterImpl.class.getName(),"createResourceAdapterStore");
        
        File spoolDir = PathUtil.getSpoolDirForComponent(getExecutionEnv(), getName());
        GEResourceAdapterStore ret = new GEResourceAdapterStore(this, spoolDir);
        
        log.exiting(GEServiceAdapterImpl.class.getName(),"createResourceAdapterStore");
        return ret;
    }
    
    /**
     * The timeout for freeing resources of the GEServiceAdapter is 
     * 2 times the execd shutdown timeout.
     * @return the timeout for freeing resources
     */
    @Override
    protected final long getFreeResourcesTimeout() {
        return this.config.getExecdShutdownTimeout().getValueInMillis() * 2;
    }
    

    private void updateConfig(GEServiceConfig newConfig) throws GrmException {
        log.entering(GEServiceAdapterImpl.class.getName(),"updateConfig");
        
        GetMappingCommand cmd = new GetMappingCommand(newConfig.getMapping());
        // init the configuration
        config.setConfig(newConfig, getExecutionEnv().getCommandService().execute(cmd).getReturnValue());
        
        log.exiting(GEServiceAdapterImpl.class.getName(),"updateConfig");
    }

    /**
     * This method performs the necessary actions to start the GE service.
     * 
     * @param newConfig the configuration of the GE service
     * @throws com.sun.grid.grm.GrmException if qmaster can not be contacted or
     *                if the configuration is invalid
     */
    @Override
    protected Map<Hostname,Object> doStartService(GEServiceConfig newConfig) throws GrmException {
        log.entering(GEServiceAdapterImpl.class.getName(),"doStartService");
        
        log.log(Level.INFO, "gsi.startService", getName());
        
        File spoolDir = PathUtil.getSpoolDirForComponent(getExecutionEnv(), getName());
        if (!spoolDir.exists()) {
            if(!spoolDir.mkdirs()) {
                throw new GrmException("Can not create spooldirectory" + spoolDir);
            }
        }
        updateConfig(newConfig);
        sloContextFactory.clearCache();
        
        if (!jgdi.isConnected()) {
            jgdi.connect();
        }
        log.log(Level.FINE, "gsi.connect", getName());
        
        Map<Hostname,Object> ret = null;
        try {
            ret = getAllExecds();
            log.log(Level.INFO, "gsi.started", getName());
        } catch(GrmException ex) {
            jgdi.close();
            throw ex;
        }
        
        execdObserver.start();
        
        log.exiting(GEServiceAdapterImpl.class.getName(),"doStartService", ret);
        return ret;
    }

    private Map<Hostname,Object> getAllExecds() throws GrmException {
        log.entering(GEServiceAdapterImpl.class.getName(),"getAllExecds");
        
        List<ExecHost> execHosts = jgdi.getExecHostList();
        Map<Hostname,Object> ret = new HashMap<Hostname,Object>(execHosts.size());
        for (ExecHost eh : execHosts) {
            if(eh.getName().equals("template") || eh.getName().equals("global")) {
                continue;
            }
            ret.put(Hostname.getInstance(eh.getName()), eh);
        }

        log.exiting(GEServiceAdapterImpl.class.getName(),"getAllExecds", ret);
        return ret;
    }
    
    /**
     * Stop the GE service.
     * 
     * <p>This method shutdown the slo calculation , stops the host manager and
     *    closes the connection to qmaster.</p>
     * @param forced if <code>true</code> force the stop
     * @return always <code>ServiceState.UNKNOWN</code>
     */
    @Override
    protected ServiceState doStopService(boolean forced) {
        log.entering(GEServiceAdapterImpl.class.getName(), "doStopService", forced);
        
        jgdi.close();
        sloContextFactory.clearCache();
        execdObserver.stop(forced);
        
        log.exiting(GEServiceAdapterImpl.class.getName(), "doStopService", ServiceState.UNKNOWN);
        return ServiceState.UNKNOWN;
    }
    
    /**
     * Reload the configuration of the GE service adapter.
     * 
     * @param newConfig  the new configuration
     * @param forced if <code>true</code> force the reload (is currently not used
     * @throws com.sun.grid.grm.GrmException if the configuration is invalid
     */
    @Override
    protected Map<Hostname,Object> doReloadService(GEServiceConfig newConfig, boolean forced) throws GrmException {
        log.entering(GEServiceAdapterImpl.class.getName(),"doReloadService");
        
        GEServiceConfig oldConfig = config.getConfig();
        
        execdObserver.stop(false);
        
        try {
            log.log(Level.INFO, "gsi.reconfigure", getName());
            updateConfig(newConfig);
        } catch(GrmException ex) {
            log.log(Level.FINE, "gsi.disconnect", getName());
            jgdi.close();
            throw ex;
        }
        sloContextFactory.clearCache();
        
        Map<Hostname,Object> ret = null;
        // We have a valid configuration, check if a reconnect is necessary
        if (config.isSameCluster(oldConfig) && jgdi.isConnected()) {
            try {
                ret = getAllExecds();
            } catch (GrmException ex) {
                jgdi.close();
                throw ex;
            }
        } else {
            log.log(Level.INFO, "gsi.newCluster", getName());
            // We have a completly new cluster
            // We really have to stop and restart this component
            doStopService(false);
            ret = doStartService(newConfig);
        }
        
        execdObserver.start();
        
        log.entering(GEServiceAdapterImpl.class.getName(),"doReloadService", ret);
        return ret;
    }

    /**
     * Cancel all scheduled actions for a resource
     * @param ra the ResourceAdapter which represents the resource
     */
    final void cancelScheduledActions(GEResourceAdapter ra) {
        execdObserver.cancelActions(ra.getId());
    }
    
    /**
     * Schedule an observer for startup of an execd
     * @param ra   the resource adapter
     * @param transitionId the id of the transition which has installed the execd
     * @param timeout if this timeout has elapsed an the exed is still not running
     *                the resource will be set into ERROR state
     * @param unit time unit of the timeout
     */
    final void startExecdStartupObserver(GEResourceAdapter ra, long transitionId, long timeout, TimeUnit unit) {
        execdObserver.scheduleExecdStartupObserver(ra, transitionId, timeout, unit);
    }
    
    /**
     * Schedule an observer for shutdown of an execd
     * @param ra   the resource adapter
     * @param transitionId the id of the transition which has uninstalled the execd
     * @param timeout if this timeout has elapsed an the exed is still running
     *                the resource will be set into ERROR state
     * @param unit time unit of the timeout
     */
    final void startExecdShutdownObserver(final GEResourceAdapter ra, final long transitionId, long timeout, TimeUnit unit) {
        execdObserver.scheduleExecdShutdownObserver(ra, transitionId, timeout, unit);
    }
    
    /**
     *   Get the jgdi connection to qmaster.
     *
     *   @return jgdi connection to qmaster
     */
    GEConnection getJgdi() {
        return jgdi;
    }

    /**
     *  Get the configuration helper.
     *
     *  @return the configuration helper
     */
    GEServiceConfigHelper getConfigHelper() {
        return config;
    }

    /**
     * 
     * @return the SLO context
     * @throws com.sun.grid.grm.GrmException
     */
    @Override
    public SLOContext createSLOContext() throws GrmException {
        log.entering(GEServiceAdapterImpl.class.getName(),"createSLOContext");
        SLOContext ret = sloContextFactory.createSLOContext();
        log.exiting(GEServiceAdapterImpl.class.getName(),"createSLOContext");
        return ret;
    }
    
    /**
     * The MySLOContextFactory creates instances of GEServiceSLOContext. 
     * 
     * The createSLOContext uses a cached result of the qstat over jgdi.
     */
    private class MySLOContextFactory implements SLOContextFactory {

        private QueueInstanceSummaryResult result;
        private long lastQstatUpdate;
        
        public synchronized void clearCache() {
            result = null;
        }
        public SLOContext createSLOContext() throws GrmException {
            log.entering(MySLOContextFactory.class.getName(),"createSLOContext");
            
            long timeout = getConfigHelper().getSloUpdateInterval().getValueInMillis();
            
            boolean needUpdate = false;
            // Currently the QueueInstanceSummaryResult is only used by 
            // the MaxPendingsJobsSLO.
            // => run qstat only if such an SLO is registered
            if(getSLOManager().hasSLO(MaxPendingsJobsSLO.class)) {
                synchronized(this) {
                    if(result == null || lastQstatUpdate + timeout < System.currentTimeMillis()) {
                        needUpdate = true;
                        result = null;
                    }
                }
            }
            if (needUpdate) {
                log.log(Level.FINE, "gsi.update");
                QueueInstanceSummaryOptions options = new QueueInstanceSummaryOptions();

                options.setShowRequestedResourcesForJobs(true);
                options.setShowFullOutput(true);
                options.setShowArrayJobs(true);
                options.setShowExtendedSubTaskInfo(true);
                
                long ts = System.currentTimeMillis();
                QueueInstanceSummaryResult tmpRes = getJgdi().getQueueInstanceSummary(options);

                log.log(Level.FINE, "gsi.updated");
                synchronized(this) {
                    result = tmpRes;
                    lastQstatUpdate = ts;
                }
            }            
            GEServiceSLOContext ret = new GEServiceSLOContext(getResources(), result);
            log.exiting(MySLOContextFactory.class.getName(),"createSLOContext");
            return ret;
        }
    }
    
    
    private void connectionLost() {
        log.entering(GEServiceAdapterImpl.class.getName(), "connectionLost");
        
        // Stop the slo calculation
        try {
            sloContextFactory.clearCache();
            // submit reconnect action
            reconnect(new ReconnectAction());
        } catch (InvalidStateTransistionException ex) {
            log.log(Level.FINE, "gsi.connecionLost.notActive", getName());
        } catch (GrmException ex) {
            log.log(Level.WARNING, "gsi.connecionLost.connectNotPos",
                    new Object [] { getName(), ex.getLocalizedMessage() });
        }
        log.exiting(GEServiceAdapterImpl.class.getName(), "connectionLost");
    }

    private class ConnectionObserver implements EventListener {

        public void eventOccured(Event evt) {
            if(evt instanceof ConnectionClosedEvent ||
               evt instanceof ConnectionFailedEvent) {
                connectionLost();
            }
        }
    }

    /**
     *   The ReconnectAction tries connection qmaster
     */
    class ReconnectAction implements Callable<Map<Hostname,Object>> {

        public Map<Hostname,Object> call() throws InterruptedException, InvalidResourceException {
            log.entering(ReconnectAction.class.getName(),"call");

            if (log.isLoggable(Level.INFO)) {
                log.log(Level.INFO, "gsi.reconnect", new Object[]{getName(), config.getQMasterReconnectInterval().getValueInSeconds()});
            }
            while (!Thread.currentThread().isInterrupted()) {

                TimeIntervalHelper reconnectInterval = config.getQMasterReconnectInterval();

                Thread.sleep(reconnectInterval.getValueInMillis());

                try {
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "gsi.tryreconnect", getName());
                    }
                    jgdi.connect();
                } catch (GrmException ex) {
                    continue;
                }
                log.log(Level.FINE, "gsi.connect", getName());
                try {
                    Map<Hostname,Object> ret = getAllExecds();
                    log.log(Level.INFO, "gsi.started", getName());
                    log.exiting(ReconnectAction.class.getName(), "call");
                    return ret;
                } catch(GrmException ex) {
                    jgdi.close();
                    continue;
                }
            }
            throw new InterruptedException();
        }
    }
    
    private final ExecdObserver execdObserver = new ExecdObserver();
    
    private class ExecdObserver {
        
        private final Lock execdObserverLock = new ReentrantLock();
        private ScheduledExecutorService scheduler;
        private Map<ResourceId,List<ScheduledFuture>> scheduledAction = new HashMap<ResourceId,List<ScheduledFuture>>();
        
        
        public void start() {

            ScheduledExecutorService exe = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {

                public Thread newThread(Runnable r) {
                    Thread ret = new Thread(r, String.format("%s[instFinalizer]", getName()));
                    ret.setContextClassLoader(GEServiceAdapterImpl.class.getClassLoader());
                    return ret;
                }
            });

            execdObserverLock.lock();
            try {
                scheduler = exe;
            } finally {
                execdObserverLock.unlock();
            }
        }

        public void stop(boolean forced) {
            ScheduledExecutorService exe = null;
            execdObserverLock.lock();
            try {
                exe = scheduler;
                scheduler = null;
                scheduledAction.clear();
            } finally {
                execdObserverLock.unlock();
            }
            if (exe != null) {
                exe.shutdownNow();
                log.log(Level.FINE, "gsi.ob.shutdown", getName());
                if (forced == false) {
                    try {
                        if (!exe.awaitTermination(60, TimeUnit.SECONDS)) {
                            log.log(Level.WARNING, "gsi.ob.shutdown.timeout", getName());
                        } else {
                            log.log(Level.FINE, "Service {0}: Execd observer successfully stopped", getName());
                        }
                    } catch (InterruptedException ex) {
                        log.log(Level.WARNING, "gsi.ob.shutdown.int", getName());
                    }
                }
            }
        }
        
        public void cancelActions(ResourceId id) {
            List<ScheduledFuture> list = null;
            execdObserverLock.lock();
            try {
                list = scheduledAction.remove(id);
            } finally {
                execdObserverLock.unlock();
            }
            
            if (list != null) {
                for(ScheduledFuture f: list) {
                    f.cancel(true);
                }
            }
        }
        
        private boolean schedule(Runnable r, ResourceId resId, long time, TimeUnit unit) {
            boolean scheduled = false;
            execdObserverLock.lock();
            try {
                if (scheduler != null && !scheduler.isShutdown()) {
                    
                    List<ScheduledFuture> list = scheduledAction.get(resId);
                    if(list == null) {
                        list = new LinkedList<ScheduledFuture>();
                        scheduledAction.put(resId, list);
                    }
                    
                    // Cleanup finished actions
                    ListIterator<ScheduledFuture> iter = list.listIterator();
                    while(iter.hasNext()) {
                        ScheduledFuture tmpFi = iter.next();
                        if(tmpFi.isCancelled() || tmpFi.isDone()) {
                            iter.remove();
                        }
                    }
                    
                    //Schedule the new action
                    ScheduledFuture f = scheduler.schedule(r, time, unit);
                    list.add(f);
                    scheduled = true;
                }
            } finally {
                execdObserverLock.unlock();
            }
            return scheduled;
        }
        
        public void scheduleExecdStartupObserver(final GEResourceAdapter ra, final long transitionId, long timeout, TimeUnit unit) {
            Runnable r = new Runnable() {

                public void run() {
                    try {
                        processUpdateResourceOperation(ra.getKey(), new ExecHostFinalizeInstallOperation(transitionId));
                    } catch (UnknownResourceException ex) {
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "gsi.ob.startup.del", new Object[]{getName(), ra.getKey()});
                        }
                    } catch (ServiceNotActiveException ex) {
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "gsi.ob.startup.notActive", new Object[]{getName(), ra.getKey()});
                        }
                    } catch (RAOperationException ex) {
                        if (log.isLoggable(Level.WARNING)) {
                            log.log(Level.WARNING, 
                                    I18NManager.formatMessage("gsi.ob.startup.failed", BUNDLE, getName(), ra.getKey(), ex.getLocalizedMessage()),
                                    ex);
                        }
                    }
                }
            };
            if (!schedule(r, ra.getId(), timeout, unit)) {
                log.log(Level.FINE, "gsi.ob.startup.schedule.notActive", getName());
            }
        }
        
        void scheduleExecdShutdownObserver(final GEResourceAdapter ra, final long transitionId, long timeout, TimeUnit unit) {
            Runnable r = new Runnable() {

                public void run() {
                    try {
                        processUpdateResourceOperation(ra.getKey(), new ExecHostFinalizeUninstallOperation(transitionId));
                    } catch (UnknownResourceException ex) {
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "gsi.ob.shutdown.del", new Object[]{getName(), ra.getKey()});
                        }
                    } catch (ServiceNotActiveException ex) {
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "gsi.ob.shutdown.notActive", new Object[]{getName(), ra.getKey()});
                        }
                    } catch (RAOperationException ex) {
                        if (log.isLoggable(Level.WARNING)) {
                            log.log(Level.WARNING, 
                                    I18NManager.formatMessage("gsi.ob.shutdown.failed", BUNDLE, getName(), ra.getKey(), ex.getLocalizedMessage()),
                                    ex);
                        }
                    }
                }
            };
            if (!schedule(r, ra.getId(), timeout, unit)) {
                log.log(Level.FINE, "gsi.ob.shutdown.schedule.notActive", getName());
            }
        }
        
    }
    
    
    private class ExecHostFinalizeInstallOperation implements RAOperation {
        private final long transitionId;
        
        public ExecHostFinalizeInstallOperation(long transitionId) {
            this.transitionId = transitionId;
        }    
        
        public ResourceTransition execute(ResourceAdapter ra) {
            return ((GEResourceAdapter)ra).finalizeInstall(transitionId);
        }

        public String getOperationName() {
            return String.format("FinalizeInstall(%d)", transitionId);
        }
    }
    
    
    
    private class ExecHostFinalizeUninstallOperation implements RAOperation {
        private final long transitionId;
        
        public ExecHostFinalizeUninstallOperation(long transitionId) {
            this.transitionId = transitionId;
        }    
        
        public ResourceTransition execute(ResourceAdapter ra) {
            return ((GEResourceAdapter)ra).finalizeUninstall(transitionId);
        }

        public String getOperationName() {
            return String.format("FinalizeUninstall(%d)", transitionId);
        }
    }
    
    
    private class ExecHostUpdateOperation implements RAOperation {

        private final ExecHost execHost;
        
        public ExecHostUpdateOperation(ExecHost execHost) {
           this.execHost = execHost;
        }
        
        public ResourceTransition execute(ResourceAdapter resourceAdapter) {
            log.entering(ExecHostUpdateOperation.class.getName(),"execute");
            ResourceTransition ret = ((GEResourceAdapter)resourceAdapter).processExecdHostEvent(execHost);
            log.exiting(ExecHostUpdateOperation.class.getName(),"execute", ret);
            return ret;
        }

        public String getOperationName() {
            return "ProcessExecdEvent";
        }
    }

    private class MyJGDIEventListener implements EventListener {

        
        private void updateHost(ExecHost host) throws InvalidResourcePropertiesException, InvalidResourceIdException, InvalidResourceException, RAOperationException {
            log.entering(MyJGDIEventListener.class.getName(),"updateHost");

            try {
                processUpdateOrCreateResourceOperation(Hostname.getInstance(host.getName()), host, new ExecHostUpdateOperation(host));
            } catch(ServiceNotActiveException ex) {
                log.log(Level.FINE, "gsi.execdUpdate.notActive", getName());
            }
            log.exiting(MyJGDIEventListener.class.getName(),"updateHost");
        }
        
        private void deleteHost(ExecHost host)  throws InvalidResourcePropertiesException , InvalidResourceIdException, RAOperationException {
            log.entering(MyJGDIEventListener.class.getName(),"deleteHost");
            ExecHostUpdateOperation op = new ExecHostUpdateOperation(host);

            try {
                processUpdateResourceOperation(Hostname.getInstance(host.getName()), op);
            } catch (UnknownResourceException ex) {
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "gsi.execdUpdate.delRemoved", new Object [] { getName(), host.getName()});
                }
            } catch(ServiceNotActiveException ex) {
                log.log(Level.FINE, "gsi.execdUpdate.notActive", getName());
            }
            log.exiting(MyJGDIEventListener.class.getName(),"deleteHost");
        }
        /**
         *   JGDI event handling method.
         *
         *   @param evt the JGDI event
         */
        @SuppressWarnings("unchecked")
        public void eventOccured(Event evt) {
            log.entering(MyJGDIEventListener.class.getName(), "eventOccured", evt);

            try {
                if (evt instanceof ExecHostAddEvent) {
                    ExecHostAddEvent addEvt = (ExecHostAddEvent) evt;
                    if (addEvt.get() != null) {
                        updateHost(addEvt.get());
                    }
                } else if (evt instanceof ExecHostDelEvent) {
                    ExecHostDelEvent delEvt = (ExecHostDelEvent) evt;
                    // Issue 571:
                    // The EXECD_DEL event must not remove the host
                    // Instead the update method should detect that the host
                    // is not longer active.
                    ExecHost eh = delEvt.get();
                    // Isue 645:
                    // The EXECD_DEL event does not deliver the exec host
                    // object. delEvt.get() returns always null
                    // We construct a dummy exec host object without load values
                    // The Host object will detect that the host has been removed
                    if (eh == null) {
                        ExecHostImpl ehi = new ExecHostImpl();
                        ehi.setName(delEvt.getName());
                        eh = ehi;
                    }
                    // The EXECD_DEL event must not auto discover the host
                    // call deleteHost instead of updateHost
                    deleteHost(eh);
                } else if (evt instanceof ExecHostModEvent) {
                    ExecHostModEvent modEvt = (ExecHostModEvent) evt;
                    if (modEvt.get() != null) {
                        updateHost(modEvt.get());
                    }
                } else if (!(evt instanceof ConnectionClosedEvent || evt instanceof ConnectionFailedEvent)) {
                    // ConnectionClosedEvent and ConnectionFailedEvent are handled by the ConnectionObserver
                    // All other events are unexpected
                    log.log(Level.WARNING, "hm.unknown.jgdi.evt", new Object [] { getName(), evt });
                }
            } catch (Exception e) {
                log.log(Level.WARNING, I18NManager.formatMessage("hm.ex.evt", BUNDLE, getName(), evt, e.getLocalizedMessage()), e);
            }
            log.exiting(MyJGDIEventListener.class.getName(), "eventOccured");
        }
    }
    

}
