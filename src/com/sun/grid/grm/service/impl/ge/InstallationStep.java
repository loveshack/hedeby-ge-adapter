/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

/**
 *  Abstract base class for an Installation step
 */
public abstract class InstallationStep {
    
    private String description;
    
    /**
     * Create a new installation step
     * @param description the description of the step
     */
    protected InstallationStep(String description) {
        this.description = description;
    }
    
    /**
     * Execute this installatin step
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if the execution has failed
     */
    public abstract void execute() throws InstallException;
    
    /**
     * Abort this installation step
     */
    public void abort() {
        // Empty implementation
    }
    
    /**
     * Undo the previously executed installatin step
     */
    public abstract void undo();
    
    
    /**
     * Get the description of the installatin step
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    
    
}
