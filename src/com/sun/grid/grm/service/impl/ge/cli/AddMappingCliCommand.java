/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.impl.ge.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.cli.UsageException;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.gridengine.MappingConfig;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.service.impl.ge.ui.AddMappingCommand;
import com.sun.grid.grm.service.impl.ge.ui.GetMappingCommand;
import com.sun.grid.grm.service.impl.ge.ui.MappingUtil;
import java.io.File;

/**
 *
 */
@CliCommandDescriptor(
    name = "AddMappingCliCommand",
    hasShortcut = true,
    category = CliCategory.ADMINISTRATION,
    bundle="com.sun.grid.grm.service.impl.ge.cli.messages",
    requiredPrivileges = { UserPrivilege.ADMINISTRATOR, UserPrivilege.READ_BOOTSTRAP_CONFIG }
)
public class AddMappingCliCommand extends AbstractCliCommand {

    @CliOptionDescriptor(
    name = "AddMappingCliCommand.name",
            numberOfOperands = 1,
            required=true
            )
    private String name;
    
    @CliOptionDescriptor(
    name = "AddMappingCliCommand.file",
            numberOfOperands = 1,
            required = false
            )
    private File file;

    @CliOptionDescriptor(
    name = "AddMappingCliCommand.template",
            numberOfOperands = 1,
            required = false
            )
    private String template;
    
    
    public void execute(AbstractCli cli) throws GrmException {
        
        
        if(template != null && file != null) {
            throw new UsageException("AddMappingCliCommand.torf", getBundleName());
        } else if(file != null) {
            addFromFile(cli);
        } else {
            addInteractive(cli);
        }
    }

    
    private void addFromFile(AbstractCli cli) throws GrmException {
        ExecutionEnv env = cli.getExecutionEnv();
        
        MappingConfig mapping = (MappingConfig) XMLUtil.load(file);

        AddMappingCommand cmd = new AddMappingCommand(name, mapping);
        env.getCommandService().execute(cmd);
        cli.out().println("AddMappingCliCommand.success", getBundleName(), name);
        
    }
    
    
    private void addInteractive(AbstractCli cli) throws GrmException {
        ExecutionEnv env = cli.getExecutionEnv();
        MappingConfig mapping = null;
        if(template != null) {
            GetMappingCommand cmd = new GetMappingCommand(template);
            mapping = env.getCommandService().execute(cmd).getReturnValue();
        } else {
            mapping = MappingUtil.createDefaultMapping();
        }
        
        MappingConfig newMapping = XMLUtil.<MappingConfig>edit(env, mapping, "ge_mapping");
        
        if(newMapping != null) {
            // User modified the mapping
            mapping = newMapping;
            AddMappingCommand cmd = new AddMappingCommand(name, mapping);
            env.getCommandService().execute(cmd);
            cli.out().println("AddMappingCliCommand.success", getBundleName(), name);
        } else {
            cli.out().println("AddMappingCliCommand.failed", getBundleName());
        }        
    }
}
