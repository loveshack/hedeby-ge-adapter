/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge.slo;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.config.gridengine.MaxPendingJobsSLOConfig;
import com.sun.grid.grm.resource.HostResourceType;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.service.Need;
import com.sun.grid.grm.service.Usage;
import com.sun.grid.grm.service.impl.ge.GEServiceSLOContext;
import com.sun.grid.grm.service.slo.SLOContext;
import com.sun.grid.grm.service.slo.SLOState;
import com.sun.grid.grm.service.slo.impl.AbstractSLO;
import com.sun.grid.grm.service.slo.impl.DefaultSLOState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.TimeIntervalHelper;
import com.sun.grid.grm.util.filter.Filter;
import com.sun.grid.grm.util.filter.FilterHelper;
import com.sun.grid.jgdi.monitoring.JobSummary;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  This SLO counts the number of pending jobs which matches a job filter.
 *  
 *  <p>If the number of pending jobs which can not be scheduled by the Grid Engine Scheduler
 *  exceeds a maximum value it produces a need according to the requestFilter of the slo
 *  config.</p>
 *  <p>If the MaxPendingJobSLresumesO detects a need all resources matching the resourceFilter and 
 *  having running matching jobs will get as usage the urgency of the SLO.</p>
 *  <p>If a new Resource is assigned to the service and it matches the resourceFilter
 *  it will get as usage the urgency of this SLO until a matching job is running on the resource
 *  or the maxWaitTimeForJobs for this resource expires. The MaxPendingJobsSLO assumes in this
 *  that this resource is not suitable to solve the need.</p>
 * 
 *  <p>To find out the number of not scheduleable jobs it uses the following formula</p>:
 *  <pre>
 *  number_of_not_scheduleable_jobs =
 *          qstat_pending_job_count - pending_resources_count * averageSlotsPerHost
 * 
 *  qstat_pending_job_count = number of pending jobs reported by qstat and matching the
 *                            job filter
 *  pending_resources_count = number of resource assigned to the service which did not get a 
 *                            jobs and the maxWaitTimeForJobs is not timed out
 *  </pre>
 * 
 *  <p>Quantity of the need is calculated with the following formula:</p>
 *  <pre>
 *  quantity = number_of_not_scheduleable_jobs / averageSlotsPerHost
 *  number_of_not_scheduleable_jobs =
 *         qstat_pending_job_count - pending_resources_count * averageSlotsPerHost
 *
 *  </pre>
 *  <pre>
 *  The SLO does not consider any pending job that is submitted on hold (<code>qsub -h</code>)
 *  This is important because a job on hold will not be scheduled, thus it should not produce
 *  any need.    
 *  </pre>
 *  <p>If the <code>number_of_not_scheduleable_jobs % averageSlotsPerHost</code> is not 0 the quantity
 *  is incremented by 1.</p>
 */
public class MaxPendingsJobsSLO extends AbstractSLO {

    public final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.slo.messages";
    private final static Logger log = Logger.getLogger(MaxPendingsJobsSLO.class.getName(), BUNDLE);
    /** If not maxWaitTimeForJobs is configured we use 2 minutes */
    private final static TimeIntervalHelper DEFAULT_WAIT_TIME_FOR_JOBS =
            new TimeIntervalHelper(2, TimeIntervalHelper.MINUTES);
    private final Map<Resource, Long> resourceCanidates = new HashMap<Resource, Long>();
    private final Set<Resource> timedOutResourceSet = new HashSet<Resource>();
    private final Filter<JobSummary> jobFilter;
    private final TimeIntervalHelper maxWaitTimeForJobs;

    /**
     * Creates a new instance of MaxPendingsJobsSLO
     * @param config   the configuration of the SLO
     * @throws com.sun.grid.grm.GrmException 
     */
    public MaxPendingsJobsSLO(MaxPendingJobsSLOConfig config) throws GrmException {
        super(config);
        if (config.isSetJobFilter()) {
            jobFilter = FilterHelper.<JobSummary>parse(config.getJobFilter());
        } else {
            jobFilter = null;
        }
        if (config.isSetMaxWaitTimeForJobs()) {
            maxWaitTimeForJobs = new TimeIntervalHelper(config.getMaxWaitTimeForJobs());
        } else {
            maxWaitTimeForJobs = DEFAULT_WAIT_TIME_FOR_JOBS;
        }
        if (config.getAverageSlotsPerHost() <= 0) {
            throw new GrmException("mjps.ex.AverageSlotsPerHost", BUNDLE, config.getName(), config.getAverageSlotsPerHost());
        }
    }

    /**
     * Update this SLO.
     * @throws com.sun.grid.grm.GrmException of the SLO could not be updated
     * @return the new SLOState
     */
    public SLOState update(SLOContext ctx) throws GrmException {
        log.entering(MaxPendingsJobsSLO.class.getName(), "update");

        log.log(Level.FINE, "mpjs.update", getName());
        long startTime = System.currentTimeMillis();

        GEServiceSLOContext geCtx = null;
        try {
            geCtx = (GEServiceSLOContext) ctx;
        } catch (ClassCastException ex) {
            throw new GrmException("mpjs.ex.invalidCtx", ex, BUNDLE, ctx.getClass().getName());

        }
        MaxPendingJobsSLOConfig config = (MaxPendingJobsSLOConfig) getConfig();
        
        List<Resource> matchingResources = geCtx.getResources(getRequestFilter());

        int count = 0;
        int max = config.getMax();

        // If the SLOContext has been invalided stop the SLO calcuation
        ctx.validate();

        if (jobFilter == null) {
            for (JobSummary job : geCtx.getPendingJobs()) {
                //if there is a "h" this implies that the job is in hold state should not be considered!
                String state = job.getState();
                if (state == null || !state.contains("h")) {
                    count += job.getSlots();
                }
                ctx.validate();
            }
        } else {
            JobHardRequestFilterAdapter filterSource = new JobHardRequestFilterAdapter();
            for (JobSummary job : geCtx.getPendingJobs()) {
                //if there is a "h" this implies that the job is in hold state should not be considered!
                String state = job.getState();
                if (state == null || !state.contains("h")) {
                    filterSource.setFilterObject(job);
                    if (jobFilter.matches(filterSource)) {
                        count += job.getSlots();
                    }
                }
                // If the SLOContext has been invalided => stop the SLO calcuation
                ctx.validate();
            }
        }

        Map<Hostname, GEServiceSLOContext.HostStatistic> statMap = geCtx.getHostStatistic(jobFilter);
        Map<Resource, Usage> usageMap = new HashMap<Resource, Usage>();
        Map<Resource, Long> orgResourceCanidates = new HashMap<Resource, Long>(resourceCanidates);
        Set<Resource> orgTimedOutResources = new HashSet<Resource>(timedOutResourceSet);

        resourceCanidates.clear();
        timedOutResourceSet.clear();

        long timeout = maxWaitTimeForJobs.getValueInMillis();
        long now = System.currentTimeMillis();


        int totalFreeSlots = 0;
        final int averageSlotsPerHost = config.getAverageSlotsPerHost();

        for (Resource res : matchingResources) {
            Hostname hn = (Hostname) HostResourceType.HOSTNAME.getValue(res.getProperties());
            GEServiceSLOContext.HostStatistic hstat = statMap.get(hn);

            // Issue 689
            // host without queue instances must not be ignored
            if (hstat != null && hstat.getRunningJobsCount() > 0) {
                // The host has already running jobs
                usageMap.put(res, getUsage());
                if (log.isLoggable(Level.FINE)) {
                    log.log(Level.FINE, "mpjs.jobsForRes",
                            new Object[]{getName(), hn.toString(), getUsage().toString()});
                }
                if (hstat.getActiveQueueInstanceCount() > 0 && hstat.getFreeSlotCount() > 0) {
                    if (count > 0 && log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "mpjs.realfreeSlotsOnHost", new Object[]{getName(), hn.toString(), hstat.getFreeSlotCount()});
                    }
                    totalFreeSlots += hstat.getFreeSlotCount();
                }
            } else {
                // Is the host a resource canidate?
                boolean isCanidateForJobs = false;
                Long timeoutForResource = orgResourceCanidates.get(res);
                if (timeoutForResource == null) {
                    if (orgTimedOutResources.contains(res)) {
                        // We detected already a timeout for this resource
                        timedOutResourceSet.add(res);
                    } else {
                        // It is a new resource, give the scheduler the chances 
                        // for scheduling jobs on the host
                        resourceCanidates.put(res, now + timeout);
                        isCanidateForJobs = true;
                        if (count > 0) {
                            usageMap.put(res, getUsage());
                            if (log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE, "mpjs.newRes",
                                        new Object[]{getName(), hn.toString(), getUsage().toString(), maxWaitTimeForJobs});
                            }
                        }
                    }
                } else if (timeoutForResource > now) {
                    // May be the scheduler has not scheduled a job for this host
                    resourceCanidates.put(res, timeoutForResource);
                    isCanidateForJobs = true;
                    if (count > 0) {
                        usageMap.put(res, getUsage());
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "mpjs.noJobForRes",
                                    new Object[]{getName(), hn.toString(), getUsage().toString()});
                        }
                    }
                } else {
                    // timeout is reached, grid engine scheduler did not schedule a job onto this resource
                    // This SLO does not need the resource
                    timedOutResourceSet.add(res);
                    if (log.isLoggable(Level.FINE)) {
                        log.log(Level.FINE, "mpjs.giveupRes",
                                new Object[]{getName(), hn.toString(), maxWaitTimeForJobs});
                    }
                }

                if (isCanidateForJobs) {
                    if (hstat != null) {
                        if (hstat.getFreeSlotCount() > 0) {
                            // We know the concrete slots count
                            totalFreeSlots += hstat.getFreeSlotCount();
                            if (log.isLoggable(Level.FINE)) {
                                log.log(Level.FINE, "mpjs.realfreeSlotsOnHost", new Object[]{getName(), hn.toString(), hstat.getFreeSlotCount()});
                            }
                        }
                    } else {
                        // We do not know the concrete slots count
                        // use the averageSlotsPerHost count
                        totalFreeSlots += averageSlotsPerHost;
                        if (log.isLoggable(Level.FINE)) {
                            log.log(Level.FINE, "mpjs.assumedFreeSlostsOnHost", new Object[]{getName(), hn.toString(), averageSlotsPerHost});
                        }
                    }
                }
            }
        }

        List<Need> needs = null;
        int assumedNotSchedulableJobCount = 0;

        if (totalFreeSlots > 0 && count > 0) {
            if (log.isLoggable(Level.FINE)) {
                log.log(Level.FINE, "mpjs.freeSlots",
                        new Object[]{getName(), Math.min(totalFreeSlots, count), count, totalFreeSlots});
            }

            // We assume that the scheduler will schedule some jobs on the pending
            // resources => reduce the number of pending resource by the scheduleableJobsCount
            // It can happen that this assumption is not correct. It this case need will
            // increase after the maxWaittimeForJobs timeout of a resource occured
            assumedNotSchedulableJobCount = Math.max(0, count - totalFreeSlots);

        } else {
            assumedNotSchedulableJobCount = count;
        }

        if (assumedNotSchedulableJobCount > 0 && log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "mpjs.assumedNotSchedulableJobCount",
                    new Object[]{getName(), assumedNotSchedulableJobCount});
        }

        if (assumedNotSchedulableJobCount < max) {
            // The SLO is fulfilled
            needs = Collections.<Need>emptyList();
        } else {
            int quantity = assumedNotSchedulableJobCount / averageSlotsPerHost;
            if (quantity < 1) {
                quantity = 1;
            } else {
                if (assumedNotSchedulableJobCount % averageSlotsPerHost != 0) {
                    quantity++;
                }
            }

            Need need = createNeed(quantity);
            needs = Collections.singletonList(need);
        }

        DefaultSLOState ret = new DefaultSLOState(getName(), usageMap, needs);
        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINE, "mpjs.finished", new Object[]{getName(), System.currentTimeMillis() - startTime});
        }
        log.exiting(MaxPendingsJobsSLO.class.getName(), "update", ret);
        return ret;
    }
}
