/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.resource.InvalidResourcePropertiesException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.adapter.InvalidResourceStateException;
import com.sun.grid.grm.resource.adapter.RAOperationResult;
import com.sun.grid.grm.resource.adapter.ResourceAdapter;
import com.sun.grid.grm.resource.ResourceChangeOperation;
import com.sun.grid.grm.resource.ResourceChanged;
import com.sun.grid.grm.resource.ResourceId;
import com.sun.grid.grm.resource.adapter.ResourceTransition;
import com.sun.grid.grm.resource.adapter.impl.AddResourceOPR;
import com.sun.grid.grm.resource.adapter.impl.DefaultResourceTransition;
import com.sun.grid.grm.resource.adapter.impl.ListOPR;
import com.sun.grid.grm.resource.adapter.impl.NullOPR;
import com.sun.grid.grm.resource.adapter.impl.RemoveResourceOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceChangedOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceAddedOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceErrorOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceRejectedOPR;
import com.sun.grid.grm.resource.adapter.impl.ResourceRemovedOPR;
import com.sun.grid.grm.resource.impl.ResourceNotChanged;
import com.sun.grid.grm.service.ResourceRemovalDescriptor;
import com.sun.grid.grm.service.descriptor.AbstractForcedResourceRemovalDescriptor;
import com.sun.grid.grm.service.impl.ge.Host.HostState;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.jgdi.configuration.ExecHost;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  ResourceAdapter for the Grid Engine Service Adapter.
 * 
 *  This ResourceAdapter supports host resources. It installs a Grid Engine
 *  execution daemon host the host which is represented by the resource
 * 
 */
public class GEResourceAdapter implements ResourceAdapter<Hostname> {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";
    private final static Logger log = Logger.getLogger(GEResourceAdapter.class.getName(), BUNDLE);
    private final Host host;
    private final GEServiceAdapterImpl service;
    private final ResourceId resId;
    
    /**
     * Create a new instance of GEResourceAdapter
     * @param service the Grid Engine service adapter
     * @param host The host object
     */
    public GEResourceAdapter(GEServiceAdapterImpl service, Host host) {
        this.host = host;
        this.service = service;
        this.resId = host.getResource().getId();
    }

    /**
     * get the key of the resource
     * @return the key of the resource
     */
    public Hostname getKey() {
        return host.getName();
    }

    @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!GEResourceAdapter.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        return getKey().equals(((GEResourceAdapter)obj).getKey());
    }

    @Override
    public final int hashCode() {
        return getKey().hashCode();
    }

    
    /**
     * Get the host object 
     * @return
     */
    Host getHost() {
        return host;
    }

    /**
     * Get the id of the resource
     * @return the resource id
     */
    public ResourceId getId() {
        return resId;
    }

    /**
     * Get a clone of the resource
     * @return
     */
    public Resource getResource() {
        return host.getResource();
    }

    /**
     * Is the resource static
     * @return <code>true</code> if the resource is static
     */
    public boolean isStatic() {
        return host.isStatic();
    }

    private ResourceTransition createResourceTransition(long id, RAOperationResult result) {
        return new DefaultResourceTransition(id, result);
    }

    private ResourceTransition createResourceTransition(long id, RAOperationResultBuilder resultBuilder) {
        return createResourceTransition(id, resultBuilder.getResult());
    }

    /**
     * Prepare the installation
     * @return the resource transition
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException
     */
    public ResourceTransition prepareInstall() throws InvalidResourceStateException {
        try {
            RAOperationResultBuilder resultBuilder = new RAOperationResultBuilder();
            host.addHostListener(resultBuilder);
            long id;
            try {
                id = host.prepareForInstall();
                service.cancelScheduledActions(this);
            } finally {
                host.removeHostListener(resultBuilder);
            }
            return createResourceTransition(id, resultBuilder);
        } catch (InstallException ex) {
            throw new InvalidResourceStateException(ex.getLocalizedMessage(), ex);
        }
    }
    
    /**
     * Prepare the uninstallation
     * @param descr the descriptor of the resource remove operation
     * @return the resource transition
     * @throws com.sun.grid.grm.resource.adapter.InvalidResourceStateException
     */
    public ResourceTransition prepareUninstall(ResourceRemovalDescriptor descr) throws InvalidResourceStateException {
        try {
            RAOperationResultBuilder resultBuilder = new RAOperationResultBuilder(descr);
            host.addHostListener(resultBuilder);
            long id;
            try {
                id = host.prepareForUninstall(descr);
                service.cancelScheduledActions(this);
            } finally {
                host.removeHostListener(resultBuilder);
            }
            return createResourceTransition(id, resultBuilder);
        } catch (InstallException ex) {
            throw new InvalidResourceStateException(ex.getLocalizedMessage(), ex);
        }
    }

    /**
     * Prepare the reset
     * @return the resource transition
     */
    public ResourceTransition prepareReset() {
        RAOperationResultBuilder resultBuilder = new RAOperationResultBuilder();
        host.addHostListener(resultBuilder);
        long id;
        try {
            id = host.prepareForReset();
            service.cancelScheduledActions(this);
        } finally {
            host.removeHostListener(resultBuilder);
        }
        return createResourceTransition(id, resultBuilder);
    }

    /**
     * Installs the  execd on the host represented by this ResourceAdapter
     * @param transition the transition which has prepared the Resource for install
     * @return the final resource transition
     */
    public ResourceTransition install(final ResourceTransition transition) {
        try {
            RAOperationResultBuilder resultBuilder = new RAOperationResultBuilder();

            host.addHostListener(resultBuilder);
            try {
                host.install(service, transition.getId());
            } finally {
                host.removeHostListener(resultBuilder);
            }

            service.startExecdStartupObserver(this, transition.getId(),
                    service.getConfigHelper().getExecdStartupTimeout().getValueInMillis(), TimeUnit.MILLISECONDS);
            
            return createResourceTransition(transition.getId(), resultBuilder);
        } catch (InstallNotPossibleException ex) {
            return createResourceTransition(transition.getId(),
                    new ResourceRejectedOPR(host.getResource(), ex.getLocalizedMessage()));
        } catch (InstallException ex) {
            return createResourceTransition(transition.getId(),
                    new ResourceErrorOPR(host.getResource(), ex.getLocalizedMessage()));
        } catch (HostTransitionExpiredException ex) {
            return createResourceTransition(transition.getId(), NullOPR.getInstance());
        }
    }
    
    /**
     * This method checks whether qmaster has send a EXECD event containing
     * load values. If it has not the resource state is set to ERROR and
     * the returned ResourceTransition contains a ResourceErrorOPR.
     * 
     * @param transitionId the transition which installed the execd
     * @return  the resource transition
     */
    public ResourceTransition finalizeInstall(long transitionId) {
        RAOperationResultBuilder resultBuilder = new RAOperationResultBuilder();
        host.addHostListener(resultBuilder);
        try {
             host.finalizeInstall(transitionId);
        } finally {
            host.removeHostListener(resultBuilder);
        }
        return createResourceTransition(transitionId, resultBuilder);
    }

    /**
     * This method uninstalls the execd from the host represented by the resource
     * @param descr the descriptor of the resource remove operation
     * @param transition  the transition which has prepared the resource for uninstallation
     * @return the resource transition
     */
    public ResourceTransition uninstall(ResourceTransition transition, ResourceRemovalDescriptor descr) {
        RAOperationResultBuilder resultBuilder = new RAOperationResultBuilder(descr);
        host.addHostListener(resultBuilder);
        try {
            boolean forced = (descr instanceof AbstractForcedResourceRemovalDescriptor)
                    && ((AbstractForcedResourceRemovalDescriptor)descr).isForced();

            host.uninstall(service, transition.getId(), forced);
            service.startExecdShutdownObserver(this, transition.getId(),
                    service.getConfigHelper().getExecdShutdownTimeout().getValueInMillis(), TimeUnit.MILLISECONDS);
        } catch (InstallException ex) {
            // Ignore, error has been reported via the host listener
        } catch (HostTransitionExpiredException ex) {
            // Ignore, error has been reported via the host listener
        } finally {
            host.removeHostListener(resultBuilder);
        }
        return createResourceTransition(transition.getId(), resultBuilder.getResult());
    }
    
    /**
     * This method checks whether qmaster has already sent the EXECD_DEL event
     * for the previously uninstalled execd.
     * If it has not the resource state is set to ERROR and the return value 
     * contains a ResourceErrorOPR
     * @param transitionId the transition which has performed the uninstall
     * @return the result transition result
     */
    public ResourceTransition finalizeUninstall(long transitionId) {
        RAOperationResultBuilder resultBuilder = new RAOperationResultBuilder();
        host.addHostListener(resultBuilder);
        try {
             host.finalizeUninstall(transitionId);
        } finally {
            host.removeHostListener(resultBuilder);
        }
        return createResourceTransition(transitionId, resultBuilder);
    }
    
    /**
     * Reset the execd in the host represented by the resource.
     * 
     * This method reinstalls the exec on the host
     * @param transition the transition which has prepared the resource for install
     * @return the resource transtion result
     */
    public ResourceTransition reset(final ResourceTransition transition) {
        try {
            RAOperationResultBuilder resultBuilder = new RAOperationResultBuilder();

            host.addHostListener(resultBuilder);
            try {
                host.reset(service, transition.getId());
            } finally {
                host.removeHostListener(resultBuilder);
            }

            service.startExecdStartupObserver(this, transition.getId(),
                    service.getConfigHelper().getExecdStartupTimeout().getValueInMillis(), TimeUnit.MILLISECONDS);
            
            return createResourceTransition(transition.getId(), resultBuilder.getResult());
        } catch (InstallException ex) {
            return createResourceTransition(transition.getId(),
                    new ResourceErrorOPR(host.getResource(), ex.getLocalizedMessage()));
        } catch (HostTransitionExpiredException ex) {
            return createResourceTransition(transition.getId(),
                    new ResourceErrorOPR(host.getResource(), ex.getLocalizedMessage()));
        }
    }

    /**
     * This method updated the host object with the information from an ExecHost object.
     * 
     * <p>If the underlying resource is in ASSINGING state and the ExecHost object contains
     * load values the return value will contain the ResourceAddedOPR, the resource state will be
     * set to ASSIGNED</p>.
     * 
     * <p>If the underlying resource is in UNASSIGING state and the ExecHost object contains
     *    no load values the return value will contain the ResourceRemoveOPR, the resource
     *    state will be set to UNASSIGNED.</p> 
     * 
     * <p>If the underlying resource is in ASSIGNED state and the ExecHost object contains no
     *    load values the return value will contains the ResourceErrorOPR, the resource
     *    state will be set to ERROR.</p>
     * 
     * <p>In all cases a complex to resource properties mapping is performed and if a 
     *    resource property has changed the return value will contain a ResourceChangedOPR.</p>
     * 
     * @param execHost the ExecHost event
     * @return the result of the transition
     */
    public ResourceTransition processExecdHostEvent(ExecHost execHost) {
        log.entering(GEResourceAdapter.class.getName(), "processExecdHostEvent", execHost);
        RAOperationResultBuilder resultBuilder = new RAOperationResultBuilder();

        host.addHostListener(resultBuilder);
        try {
            // We have discovered a not host
            host.update(service, execHost);
        } catch (GrmException ex) {
            log.log(Level.WARNING, "Could not update host " + host + " with execd event: " + ex.getLocalizedMessage(), ex);
            host.setHostState(Host.HostState.HOST_ERROR, ex.getLocalizedMessage());
        } finally {
            host.removeHostListener(resultBuilder);
        }
        
        // We do not care about the transition id for update events
        ResourceTransition ret = createResourceTransition(0, resultBuilder.getResult());
        log.exiting(GEResourceAdapter.class.getName(), "processExecdHostEvent", ret);
        return ret;
    }

    /**
     * Modify the resource properties
     * @param operation  the operation
     * @return if the operation has changed the resource the return value will contains
     *         a ResourceChangedOPR
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation would produces
     *            an invalid resource
     */
    public ResourceChanged modifyResource(ResourceChangeOperation operation) throws InvalidResourcePropertiesException {
        Collection<ResourceChanged> changes = host.modify(service, Collections.singleton(operation));
        if(changes.isEmpty()) {
            return ResourceNotChanged.getInstance();
        } else {
            return changes.iterator().next();
        }
    }

    /**
     * Modify the resource properties
     * @param operations  the operation
     * @return if the operation has changed the resource the return value will contains
     *         a ResourceChangedOPR
     * @throws com.sun.grid.grm.resource.InvalidResourcePropertiesException if the operation would produces
     *            an invalid resource
     */
    public Collection<ResourceChanged> modifyResource(Collection<ResourceChangeOperation> operations) throws InvalidResourcePropertiesException {
        return host.modify(service, operations);
    }

    /**
     * The HostImpl class sends out all modification on the resource via
     * the HostListeners. 
     * This class collects out of the host events the RAOperationResults 
     */
    class RAOperationResultBuilder implements HostListener {

        private final ListOPR result = new ListOPR();
        private final ResourceRemovalDescriptor descr;
        public RAOperationResultBuilder() {
            this(null);
        }
        
        public RAOperationResultBuilder(ResourceRemovalDescriptor descr) {
            this.descr = descr;
        }
        
        public void addResult(RAOperationResult res) {
            result.add(res);
        }

        public void resourceChanged(Host host, Collection<ResourceChanged> changed, String message) {
            result.add(new ResourceChangedOPR(host.getResource(), changed, message));
        }

        public RAOperationResult getResult() {
            if (result.isEmpty()) {
                return NullOPR.getInstance();
            } else {
                return result;
            }
        }

        public void hostStateChanged(Host host, HostState oldHostState, HostState newHostState, String message) {
            switch (newHostState) {
                case HOST_RESETING:
                case HOST_INSTALLING:
                    result.add(new AddResourceOPR(host.getResource(), message));
                    break;
                case HOST_UNINSTALLING:
                    result.add(new RemoveResourceOPR(host.getResource(), message, descr));
                    break;
                case HOST_RUNNING:
                    result.add(new ResourceAddedOPR(host.getResource(), message));
                    break;
                case HOST_REMOVED:
                    result.add(new ResourceRemovedOPR(host.getResource(), message, descr));
                    break;
                case HOST_ERROR:
                    result.add(new ResourceErrorOPR(host.getResource(), message));
                    break;
                default: // Nothing to do
            }
        }
    }

    /**
     * Map a host state to the corresponding resource state
     * @param state   the host state
     * @return the resource state
     * @throws java.lang.IllegalArgumentException if the host state is unknown
     */
    public static Resource.State mapHostStateToResourceState(Host.HostState state) {
        switch (state) {
            case HOST_RUNNING:
                return Resource.State.ASSIGNED;
            case HOST_INSTALLING:
                return Resource.State.ASSIGNING;
            case HOST_INSTALLED:
                return Resource.State.ASSIGNING;
            case HOST_ERROR:
                return Resource.State.ERROR;
            // Issue 488: Service must not use resource state UNASSIGNED. If a host is
            //            not installed it should only use ASSIGNING
            case HOST_NONE:
                return Resource.State.ASSIGNING;
            // Issue 488: Service must not use resource state UNASSIGNED. If a host is
            //            not installed it should only use ASSIGNING
            case HOST_RESETING:
                return Resource.State.ASSIGNING;
            case HOST_REMOVED:
                return Resource.State.UNASSIGNING;
            case HOST_STARTING:
                return Resource.State.ASSIGNING;
            case HOST_UNINSTALLING:
                return Resource.State.UNASSIGNING;

            default:
                throw new IllegalArgumentException("Unknown host state " + state);
        }
    }
    
    @Override
    public final String toString() {
        return getId().toString();
    }
}
