/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.resource.ResourceChanged;
import java.util.Collection;

/**
 *  Listener class for hosts
 */
public interface HostListener {
    
    /**
     * Inform the listener that the resource properties of the host has been changed.
     * 
     * @param host    the host
     * @param changed properties that have changed
     * @param message description why the resource properties have been changed
     */
    public void resourceChanged(Host host, Collection<ResourceChanged> changed, String message);
    
    /**
     * Inform the listener that the state of the host has changed.
     * @param host      the host
     * @param message   the reason for the state transition
     * @param oldHostState the old state of the host
     * @param newHostState the new state of the host     
     */
    public void hostStateChanged(Host host, Host.HostState oldHostState, Host.HostState newHostState, String message);
}
