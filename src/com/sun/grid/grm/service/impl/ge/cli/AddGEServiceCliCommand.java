/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/

package com.sun.grid.grm.service.impl.ge.cli;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.cli.AbstractCli;
import com.sun.grid.grm.cli.AbstractCliCommand;
import com.sun.grid.grm.cli.CliCategory;
import com.sun.grid.grm.cli.CliCommandDescriptor;
import com.sun.grid.grm.cli.CliOptionDescriptor;
import com.sun.grid.grm.config.XMLUtil;
import com.sun.grid.grm.config.common.FixedUsageSLOConfig;
import com.sun.grid.grm.config.common.GlobalConfig;
import com.sun.grid.grm.config.common.JvmConfig;
import com.sun.grid.grm.config.common.SLOSet;
import com.sun.grid.grm.config.common.TimeInterval;
import com.sun.grid.grm.config.gridengine.ConnectionConfig;
import com.sun.grid.grm.config.gridengine.ExecuteOn;
import com.sun.grid.grm.config.gridengine.GEServiceConfig;
import com.sun.grid.grm.config.gridengine.InstallConfig;
import com.sun.grid.grm.config.gridengine.InstallTemplate;
import com.sun.grid.grm.config.gridengine.JobSuspendMethod;
import com.sun.grid.grm.config.gridengine.JobSuspendPolicy;
import com.sun.grid.grm.security.UserPrivilege;
import com.sun.grid.grm.service.impl.ge.GEServiceContainer;
import com.sun.grid.grm.ui.component.AddComponentWithConfigurationCommand;
import com.sun.grid.grm.ui.component.GetGlobalConfigurationCommand;
import com.sun.grid.grm.ui.component.InstantiateComponentCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.TimeIntervalHelper;
import com.sun.grid.grm.validate.ValidatorService;
import java.io.File;

  /**
 *  This cli command gets the global configuration from CS, opens
 *  it in an editor, validate the modified configuration and
 *  sends it back to CS
 */
@CliCommandDescriptor(
    name = "AddGEServiceCliCommand",
    hasShortcut = true,
    category = CliCategory.SERVICES,
    bundle="com.sun.grid.grm.service.impl.ge.cli.messages",
    requiredPrivileges = { UserPrivilege.ADMINISTRATOR, UserPrivilege.READ_BOOTSTRAP_CONFIG }
)
public class AddGEServiceCliCommand extends AbstractCliCommand {
    @CliOptionDescriptor(
    name = "AddGEServiceCliCommand.j",
            numberOfOperands = 1,
            required = true
            )
    private String jvmName;
    
    @CliOptionDescriptor(
    name = "AddGEServiceCliCommand.h",
            numberOfOperands = 1,
            required = true
            )
    private String host;
    
    @CliOptionDescriptor(
    name = "AddGEServiceCliCommand.n",
            numberOfOperands = 1,
            required = true
            )
    private String name;
    
    //Optional parameter for import configuration file
    @CliOptionDescriptor(
    name = "AddGEServiceCliCommand.f",
            numberOfOperands = 1
            )
    private String fileName = null;
    
    @CliOptionDescriptor(
    name = "AddGEServiceCliCommand.start",
            numberOfOperands = 0
            )
    private boolean start = false;
    
    public void execute(AbstractCli cli) throws GrmException {
        File tmpFile = null;
        GEServiceConfig geConf = null;
        
        GetGlobalConfigurationCommand cmd = new GetGlobalConfigurationCommand();
        GlobalConfig conf = cli.getExecutionEnv().getCommandService().execute(cmd).getReturnValue();
               
        JvmConfig tmpJvm = null;
        
        ExecutionEnv env = cli.getExecutionEnv();
        
        //Check whether the Jvm exists
        for (JvmConfig jvm : conf.getJvm()) {
            if(jvm.getName().equals(jvmName)) {
                tmpJvm = jvm;
                break;
            }
        }
        //Jvm does not exist
        if(tmpJvm==null) {
            throw new GrmException("AddGEServiceCliCommand.jvm_not_exists", getBundleName(), jvmName);
        }
        
        // try to import configuration file
        if (fileName != null) {
            tmpFile = new File(fileName);
            if (!tmpFile.exists()) {
                throw new GrmException("AddGEServiceCliCommand.missing_importfile", getBundleName(), fileName);
            }
            geConf = (GEServiceConfig)XMLUtil.loadAndValidate(tmpFile);
            
            ValidatorService.validateChain(env, geConf);
        } else {
            //Create template GEService Configuration that can be edited
            geConf = createDefaultConfig();
            geConf = XMLUtil.<GEServiceConfig>edit(env, geConf, "ge_config");
            if(geConf == null) {
                cli.out().println("AddGEServiceCliCommand.unchanged", getBundleName());
                return;
            }
        }
        
        /* Convert any provided hostname string to the canonical name if possible.
           This especially covers the case where the provided hostname is 'localhost' */
        Hostname temp = Hostname.getInstance(host);
        if (temp.isResolved()) {
            host = temp.getHostname();
        } else {
            throw new GrmException("AddGEServiceCliCommand.failed.host", getBundleName(), name, host);
        }
        
        AddComponentWithConfigurationCommand aeCmd =
                AddComponentWithConfigurationCommand.newInstanceForSingleton()
                   .componentName(name)
                   .classname(GEServiceContainer.class.getName())
                   .config(geConf)
                   .hostname(host)
                   .jvmName(jvmName);

        
        //add service to Global config and create service config
        cli.getExecutionEnv().getCommandService().execute(aeCmd);
        if (start) {
            try {
                InstantiateComponentCommand cmdStart = new InstantiateComponentCommand();
                cmdStart.setComponentName(name);
                cmdStart.setHostName(host);
                cmdStart.setJvmName(jvmName);
                cli.getExecutionEnv().getCommandService().execute(cmdStart);
            } catch (Throwable ex) {
                throw new GrmException("AddGEServiceCliCommand.failed.start", ex, getBundleName(), name);
            }
        }
        cli.out().println("AddGEServiceCliCommand.success", getBundleName(), name);
    }
    
    /**
     * Create a instance of the Grid Engine Service Adapter
     * configuration with default values
     * @return the configuration object with the default values
     */
    public static GEServiceConfig createDefaultConfig() {
        
        GEServiceConfig ret = new GEServiceConfig();
        
        ConnectionConfig conConf= new ConnectionConfig();
        conConf.setClusterName("sge");
        conConf.setCell("default");
        conConf.setRoot("");
        conConf.setExecdPort(0);
        conConf.setMasterPort(0);
        conConf.setJmxPort(0);
        conConf.setUsername("username");
        conConf.setPassword("");
        conConf.setKeystore("");
        ret.setConnection(conConf);

        // add default execd config for cloud adapter BEFORE general config
        ret.getExecd().add(createDefaultCloudInstallConfig());
        // Add the default execd configuration
        ret.getExecd().add(createDefaultInstallConfig());

        TimeInterval ti = new TimeInterval();
        ti.setUnit(TimeIntervalHelper.MINUTES);
        ti.setValue(5);
        ret.setSloUpdateInterval(ti);
        
        // Add the default SLO (fixed usage 50)
        SLOSet slos = new SLOSet();

        FixedUsageSLOConfig slo = new FixedUsageSLOConfig();
        slo.setName("fixed_usage");
        slo.setUrgency(50);
        
        slos.getSlo().add(slo);
        
        ret.setSlos(slos);
        
        ret.setMapping("default");
        
        // Job suspend policy
        JobSuspendPolicy policy = new JobSuspendPolicy();
        ti = new TimeInterval();
        ti.setUnit(TimeIntervalHelper.MINUTES);
        ti.setValue(2);
        policy.setTimeout(ti);
        policy.getSuspendMethods().add(JobSuspendMethod.RESCHEDULE_JOBS_IN_RERUN_QUEUE);
        policy.getSuspendMethods().add(JobSuspendMethod.RESCHEDULE_RESTARTABLE_JOBS);
        policy.getSuspendMethods().add(JobSuspendMethod.SUSPEND_JOBS_WITH_CHECKPOINT);
        ret.setJobSuspendPolicy(policy);
        
        return ret;
    }
    
    private static InstallConfig createDefaultInstallConfig() {
        InstallConfig instConf = new InstallConfig();
        instConf.setAdminHost(true);
        instConf.setCleanupDefault(true);
        instConf.setDefaultDomain("");
        instConf.setIgnoreFQDN(true);
        instConf.setRcScript(false);
        instConf.setSubmitHost(false);
        instConf.setAdminUsername("root");
        return instConf;
    }
    
    /**
     * Creates the execd install configuration for cloud hosts.
     * 
     * <p>The exec install configuration matches against resource with
     *    resource property cloudResource = "true".</p>
     * <p>This configuration executes for installing an execd the following
     *    steps.</p>
     * <ol>
     *    <li>copy the common directory of the gridengine cluster to the cloud host.
     *        (executed on master host)</li>
     *    <li>Patch the files bootstrap, settings.sh, setting.csh, sgeexecd and sgemaster
     *        (admin_user=root, SGE_ROOT=/opt/sge, executed on cloud host).</li>
     *    <li>Execute the execd auto installer with fixed SGE_ROOT (/opt/sge, executed
     *        on cloud host).</li>
     * </ol>
     * 
     * <p>The execd uninstallation is also executed with the fixed SGE_ROOT.</p>
     * @return the created execd install config for cloud hosts
     */
    private static InstallConfig createDefaultCloudInstallConfig() {
        InstallConfig instConfCA = new InstallConfig();
        instConfCA.setAdminHost(true);
        instConfCA.setCleanupDefault(true);
        instConfCA.setDefaultDomain("");
        instConfCA.setIgnoreFQDN(true);
        instConfCA.setRcScript(false);
        instConfCA.setSubmitHost(false);
        instConfCA.setAdminUsername("root");
        // Only for cloud hosts
        instConfCA.setFilter("cloudResource=\"true\"");

        InstallTemplate copySgeRootToCloudTemplate = new InstallTemplate();
        copySgeRootToCloudTemplate.setScript("util/templates/ge-adapter/copy_sge_root_to_cloud.sh");
        copySgeRootToCloudTemplate.setNeedsConfigFile(false);
        copySgeRootToCloudTemplate.setExecuteOn(ExecuteOn.QMASTER_HOST);
        instConfCA.getInstallTemplate().add(copySgeRootToCloudTemplate);
        
//        <ge_adapter:installTemplate needsConfigFile="false"
//                                    executeOn="exec_host">
//            <ge_adapter:script>util/templates/ge-adapter/patch_bootstrap_files_on_cloud.sh</ge_adapter:script>
//        </ge_adapter:installTemplate>
        InstallTemplate patchBootstrapTemplate = new InstallTemplate();
        patchBootstrapTemplate.setExecuteOn(ExecuteOn.EXEC_HOST);
        patchBootstrapTemplate.setNeedsConfigFile(false);
        patchBootstrapTemplate.setScript("util/templates/ge-adapter/patch_bootstrap_files_on_cloud.sh");
        instConfCA.getInstallTemplate().add(patchBootstrapTemplate);
        
//        <ge_adapter:installTemplate needsConfigFile="true"
//                                    executeOn="exec_host">
//            <ge_adapter:script>util/templates/ge-adapter/install_execd_cloud.sh</ge_adapter:script>
//            <ge_adapter:conf>util/templates/ge-adapter/install_execd_cloud.conf</ge_adapter:conf>
//        </ge_adapter:installTemplate>
        InstallTemplate installExecdTemplate = new InstallTemplate();
        installExecdTemplate.setExecuteOn(ExecuteOn.EXEC_HOST);
        installExecdTemplate.setScript("util/templates/ge-adapter/install_execd_cloud.sh");
        installExecdTemplate.setConf("util/templates/ge-adapter/install_execd_cloud.conf");
        instConfCA.getInstallTemplate().add(installExecdTemplate);
        
//        <ge_adapter:uninstallTemplate needsConfigFile="true"
//                                      executeOn="exec_host">
//            <ge_adapter:script>util/templates/ge-adapter/uninstall_execd_cloud.sh</ge_adapter:script>
//            <ge_adapter:conf>util/templates/ge-adapter/uninstall_execd_cloud.conf</ge_adapter:conf>
//        </ge_adapter:uninstallTemplate>
        InstallTemplate uninstallExecdTemplate = new InstallTemplate();
        uninstallExecdTemplate.setExecuteOn(ExecuteOn.EXEC_HOST);
        uninstallExecdTemplate.setScript("util/templates/ge-adapter/uninstall_execd_cloud.sh");
        uninstallExecdTemplate.setConf("util/templates/ge-adapter/uninstall_execd_cloud.conf");
        instConfCA.getUninstallTemplate().add(uninstallExecdTemplate);
        
        return instConfCA;
    }
    
}
