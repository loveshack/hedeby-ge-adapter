/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2001 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.GrmException;
import com.sun.grid.grm.bootstrap.ComponentService;
import com.sun.grid.grm.bootstrap.ExecutionEnv;
import com.sun.grid.grm.bootstrap.PathUtil;
import com.sun.grid.grm.config.gridengine.ExecuteOn;
import com.sun.grid.grm.config.gridengine.InstallConfig;
import com.sun.grid.grm.executor.Executor;
import com.sun.grid.grm.executor.Result;
import com.sun.grid.grm.executor.impl.ShellCommand;
import com.sun.grid.grm.util.Hostname;
import com.sun.grid.grm.util.I18NManager;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Abstract base class for execd installers.
 *
 * This class provides the common steps which are need for installation and
 * uninstallation of a execd.
 */
public abstract class ExecdInstallerBase extends InstallationSequence {

    private final static String BUNDLE = "com.sun.grid.grm.service.impl.ge.messages";

    protected final Logger log = Logger.getLogger(getClass().getName(), BUNDLE);
    private final Host host;
    private final GEServiceAdapterImpl service;
    private boolean adminHostAdded;
    private InstallConfig installConfig;
    /**
     * Creates a new instance of ExecdInstallerBase.
     * @param service the grid engine service
     * @param host the host where the execd will be installed/uninstalled
     * @param name name of the step
     */
    protected ExecdInstallerBase(GEServiceAdapterImpl service, Host host, String name) {
        super(name);
        this.host = host;
        this.service = service;
        this.installConfig = getConfig().getInstallConfig(host);
    }
    
    
    /**
     * Get the host where the execd will be installed
     * @return the host
     */
    public Host getHost() {
        return host;
    }
    
    /**
     * Get the installation configuration for the execd
     * @return InstallConfig Object
     */
    public InstallConfig getInstallConfig() {
        return installConfig;
    }
    
    protected class MakeAdminHostStep extends InstallationStep {
        
        public MakeAdminHostStep() {
            super(I18NManager.formatMessage("ExecdInstallerBase.MakeAdminHostStep.name", BUNDLE));
        }
        
        /**
         * make the host to an admin host
         * @throws InstallException if jgdi reported and error
         */
        public void execute() throws InstallException {
            try {
                adminHostAdded = host.makeAdminHost(service.getJgdi());
            } catch (GrmException ex) {
                throw new InstallException(ex, "ExecdInstallerBase.MakeAdminHostStep.error.jgdi", getHost().getName(), ex.getLocalizedMessage());
            }
        }
        
        public void undo() {
            if(adminHostAdded) {
                try {
                    host.removeAdminHost(service.getJgdi());
                } catch (GrmException ex) {
                    if(log.isLoggable(Level.WARNING)) {
                        log.log(Level.WARNING,
                                I18NManager.formatMessage("ExecdInstallerBase.RemoveAdminHostStep.error.jgdi", BUNDLE, getHost().getName(), ex.getLocalizedMessage()),
                                ex);
                    }
                }
            }
        }
    }
    
    protected class RemoveAdminHostStep extends InstallationStep {
        
        public RemoveAdminHostStep() {
            super(I18NManager.formatMessage("ExecdInstallerBase.RemoveAdminHostStep.name", BUNDLE));
        }
        
        /**
         * removes the host from the admin host list if it has been added by this
         * installer.
         * 
         * @throws InstallException if jgdi reported an error
         */
        public void execute() throws InstallException {
            if (installConfig != null && !installConfig.isAdminHost() && adminHostAdded) {
                try {
                    host.removeAdminHost(getService().getJgdi());
                } catch (GrmException ex) {
                    throw new InstallException(ex, "ExecdInstallerBase.RemoveAdminHostStep.error.jgdi", 
                                               getHost().getName(), ex.getLocalizedMessage());
                }
            }
        }
        
        public void undo() {
            // Nothing to undo
        }
    }

    /**
     * Get the responsible gridengine service
     * @return the gridengine service
     */
    public GEServiceAdapterImpl getService() {
        return service;
    }
    
    /**
     * Get the config helper of the gridengine service
     * @return the config helper
     */
    public GEServiceConfigHelper getConfig() {
        return service.getConfigHelper();
    }
    
    /**
     * Get the execution env of the gridengine service
     * @return the execution env
     */
    public ExecutionEnv getExecutionEnv() {
        return service.getExecutionEnv();
    }
    
    /**
     * Store the result of a executed command in the components log direcory
     * @param step
     * @param res
     * @param filenamePrefix prefix for the filename
     * @return the file where the result has been saved or <code>null</code> if the
     *         log directory of the service can not be created
     */
    protected File saveResult(InstallationStep step, Result res, String filenamePrefix) {
        File logDir = PathUtil.getLogPathForComponent(getExecutionEnv(), getService().getName());
        if (!logDir.exists()) {
            if (!logDir.mkdirs()) {
                if (log.isLoggable(Level.WARNING)) {
                    log.log(Level.WARNING, "ExecdInstallerBase.ex.mkdirLogDir", new Object[]{step.getDescription(), logDir});
                }
                return null;
            }
        }
        String hostname = getHost().getName().toString();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd_hh:mm:ss");
        String date = df.format(new Date());

        String filename = String.format("%1$s_%2$s_%3$s.log", hostname, filenamePrefix, date);

        File logFile = new File(logDir, filename);
        try {
            FileWriter fr = new FileWriter(logFile);
            try {
                String str = res.getResultAsString("");
                fr.write(str, 0, str.length());
                return logFile;
            } finally {
                try {
                    if (fr != null) {
                        fr.close();
                    }
                } catch (IOException ex) {
                    // Ignore
                }
            }
        } catch (IOException ex) {
            if (log.isLoggable(Level.WARNING)) {
                log.log(Level.WARNING, "ExecdInstallerBase.ex.saveResult", new Object[]{step.getDescription(), ex.getLocalizedMessage()});
            }
            return null;
        }
    }

    /**
     * Executes a script on the a targeted host.
     *
     * <p>The host is determined from the target property of the <code>cmdWithTarget</code>
     * parameters. If this property is equal to <code>ExecuteOn.QMASTER_HOST</code> then
     * the script is executed on the qmaster host of the cluster. Otherwise the
     * script is executed on the host of this <code>ExecInstaller</code>.</p>
     * 
     * @param cmdWithTarget  the command with the target
     * @return the result of the command
     * @throws com.sun.grid.grm.service.impl.ge.InstallNotPossibleException if the command failed but did not modify the host
     * @throws com.sun.grid.grm.service.impl.ge.InstallException if the command failed and modified the host
     */
    protected Result execute(GEServiceConfigHelper.ShellCommandWithTarget cmdWithTarget) throws InstallNotPossibleException, InstallException {

        Hostname execHost = getHost().getName();

        if (ExecuteOn.QMASTER_HOST.equals(cmdWithTarget.getTarget())) {
            try {
                execHost = getConfig().getMasterHost();
            } catch (IOException ex) {
                throw new InstallNotPossibleException(ex, "eib.ex.act_qmaster", ex.getLocalizedMessage());
            }
        }

        Executor executor = null;

        try {
            executor = ComponentService.getComponentByTypeAndHost(getExecutionEnv(), Executor.class, execHost);
        } catch (GrmException ex) {
            throw new InstallNotPossibleException(ex, "eib.ex.gex", execHost, ex.getLocalizedMessage());
        }

        if (executor == null) {
            throw new InstallNotPossibleException("eib.ex.noex", execHost);
        }

        ShellCommand cmd = cmdWithTarget.getCmd();

        if (log.isLoggable(Level.FINE)) {
            log.log(Level.FINER, "eib.exec",
                    new Object[]{cmd.getScript().getRemoteFilename(), execHost});
        } else if (log.isLoggable(Level.FINER)) {
            log.log(Level.FINER, "eib.exec.withCmd",
                    new Object[]{cmd.getScript().getRemoteFilename(), execHost, cmd.getCommandAsString()});
        }

        try {
            return executor.execute(cmd);
        } catch (GrmException ex) {
            /*
             * Installation failed, we cannot say what was changed in resource.
             * Mark the problem as InstallException  so it's not moved to any other resource
             */
            throw new InstallException(ex, "ei.i.ex.exec", cmd.getScript().getRemoteFilename(), execHost, ex.getLocalizedMessage());
        }
    }

}
