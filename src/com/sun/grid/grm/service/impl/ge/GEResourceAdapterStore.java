/*___INFO__MARK_BEGIN__*/
/*************************************************************************
 *
 *  The Contents of this file are made available subject to the terms of
 *  the Sun Industry Standards Source License Version 1.2
 *
 *  Sun Microsystems Inc., March, 2001
 *
 *
 *  Sun Industry Standards Source License Version 1.2
 *  =================================================
 *  The contents of this file are subject to the Sun Industry Standards
 *  Source License Version 1.2 (the "License"); You may not use this file
 *  except in compliance with the License. You may obtain a copy of the
 *  License at http://gridengine.sunsource.net/Gridengine_SISSL_license.html
 *
 *  Software provided under this License is provided on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING,
 *  WITHOUT LIMITATION, WARRANTIES THAT THE SOFTWARE IS FREE OF DEFECTS,
 *  MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, OR NON-INFRINGING.
 *  See the License for the specific provisions governing your rights and
 *  obligations concerning the Software.
 *
 *   The Initial Developer of the Original Code is: Sun Microsystems, Inc.
 *
 *   Copyright: 2009 by Sun Microsystems, Inc.
 *
 *   All Rights Reserved.
 *
 ************************************************************************/
/*___INFO__MARK_END__*/
package com.sun.grid.grm.service.impl.ge;

import com.sun.grid.grm.resource.InvalidResourceException;
import com.sun.grid.grm.resource.Resource;
import com.sun.grid.grm.resource.adapter.impl.AbstractResourceAdapterFileStore;
import com.sun.grid.grm.util.Hostname;
import java.io.File;

/**
 * This ResourceAdapterStore stores Host objects with a HostFileStore in the 
 *  spool directory of the GE service component.
 */
public class GEResourceAdapterStore extends AbstractResourceAdapterFileStore<Hostname,GEResourceAdapter,Host> {

    private final GEServiceAdapterImpl service;
    
    /**
     * Create a new instance of the GEResourceAdapterStore
     * @param service the GE service
     * @param spoolDir the spool directory
     */
    GEResourceAdapterStore(GEServiceAdapterImpl service, File spoolDir) {
        super(new HostFileStore(spoolDir));
        this.service = service;
    }
    
    /**
     * Creates an GEResourceAdapter
     * @param elem the host element
     * @return the GEResourceAdapter
     */
    @Override
    protected GEResourceAdapter createResourceAdapterForElement(Host elem) {
        return new GEResourceAdapter(service, elem);
    }

    /**
     * Get the Host object from the GEResourceAdapter
     * @param adapter the GEResourceAdapter
     * @return the host object
     */
    @Override
    protected Host getElementFromResourceAdapter(GEResourceAdapter adapter) {
        return adapter.getHost();
    }

    /**
     * Creates a GEResourceAdapter for a resource.
     * 
     * 
     * @param resource  the resource 
     * @return the GEResourceAdapter
     * @throws com.sun.grid.grm.resource.InvalidResourceException
     */
    public GEResourceAdapter createResourceAdapter(Resource resource) throws InvalidResourceException {
        Host  host = HostFactory.newInstance(resource);
        return createResourceAdapterForElement(host);
    }

}
